<?php

namespace App\Http\Controllers;

use  App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use App\Product;
use App\Order;
use Datatables;
use Validator;
use URL;
use Redirect;
use Sentinel;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.order.');
    }

    public function getlist($category_id = 0)
    {
        //echo $_SESSION['userrole'];// return;
        //$products = Product::orderBy('id', 'asc')->get();
        $category = DB::table('occ_categories')->where('id', $category_id)->first();
        $orders = Order::where('category_id', $category_id )->orderby('id', 'asc')->get();
        return View('admin.order.list', compact('orders', 'category'));
    }

    public function getOrders($category_id = 0)
    {
        if ($_SESSION['userrole'] == 2) {
            $login_id = Sentinel::getUser()->id;
            $orders = DB::table('occ_products as p')
                ->join('occ_gifts as g', 'g.product_id', '=', 'p.id')
                ->select('g.*')->where('p.admin_id', '=', $login_id)->where('g.category_id', $category_id)->orderby('p.created_at', 'desc')->get();
           $orders = collect($orders);  //without command,erroe;Call to a member function getQuery() on array
          // $orders = $vorders::where('category_id', $category_id)->orderby('id', 'asc')->get();
        } else {
            $orders = Order::where('category_id', $category_id)->orderby('id', 'asc')->get();
        }
        return Datatables::of($orders)
            ->editColumn('created_at', function($data){
                return date('M d, Y', strtotime($data->created_at));
            })
            ->edit_column('product_id', function($data) {
                //$product_id = $data->id;

                $product = DB::table('occ_products')->where('id',$data->product_id)->first();
                $productname = '';
                if (!empty($product)) {
                    $productname = $product->name;
                }
                return '<a href="/admin/order/detail/'.$data->id.'">'.$productname.'</a>';
            })
            ->edit_column('customer_id', function($data) {
                $id = $data->customer_id;
                $user = DB::table('users')->where('id',$id)->first();
                $customername = $user->first_name.' '.$user->last_name;
                return '<a href="/admin/order/detail/'.$data->id.'">'.$customername.'</a>';
            })
            ->edit_column('description', function($data) {
                return '<a href="/admin/order/detail/'.$data->id.'">'.$data->description.'</a>';
            })
            ->add_column('state', function($data) {
                $state = $data->state;
                if ($state == 1) {
                    return '<a href="javascript:cancelModal('.$data->id.',1)"><span id="order_state_'.$data->id.'" >Cancel</span></a>';
                } else {
                    return '<a href="javascript:cancelModal('.$data->id.',0)"><span id="order_state_'.$data->id.'" >Accept</span></a>';
                }
            })
            ->make(true);
    }
    public function detail($id=0)
    {
        $order = DB::table('occ_gifts')->where('id', $id)->first();
        $product=DB::table('occ_products')->where('id',$order->product_id )->first();
        $user = DB::table('users')->where('id', $id)->first();

        return View(('admin.order.detail'), compact('user', 'order', 'product'));
    }
    public function stateupdate(Request $request)
    {
        $id = $request->get('id', '');
        $state = $request->get('state', '0');
        DB::table('occ_gifts')->where('id', $id)->update(['state' =>$state]);
        return $state;

    }





}
