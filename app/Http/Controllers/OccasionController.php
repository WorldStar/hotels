<?php

namespace App\Http\Controllers;

use App\Occasion;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
class OccasionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.occasion.occasion');
    }


    public function getData()
    {
        $tables = Occasion::select(['id', 'name', 'description', 'created_at']);

        return Datatables::of($tables)
            ->make(true);
    }


    public function getlist()
    {
        $tables = Occasion::orderBy('id', 'asc')->get();
        return View('admin/occasion/editable_occasion', compact('tables'));
    }


    public function editableOccasionData()
    {
        $tables = Occasion::get(['id', 'name', 'description', 'created_at']);

        return Datatables::of($tables)
            ->add_column('edit', '<a class="edit" href="javascript:;">Edit</a>')
            ->add_column('delete', '<a class="delete" href="javascript:;">Delete</a>')
            ->make(true);
    }


    public function editableOccasionUpdate(Request $request, $id)
    {
        $table = Occasion::find($id);

        $table->update($request->except('_token'));
        return $table->id;
    }


    public function editableOccasionDestroy($id)
    {
        $row = Occasion::find($id);
        $row->delete();
        return $row->id;
    }


    public function addoccasion(Request $request)
    {
        $tables = array();
        if ($request->ajax()) {

            $tables = Occasion::create($request->except('_token'));
        }
        return $tables;
    }
}

