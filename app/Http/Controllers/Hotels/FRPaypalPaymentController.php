<?php namespace App\Http\Controllers\Hotels;

use Lang;
use Redirect;
use Sentinel;
use View;
use DB;
use Validator;
use Input;
use App\ImageResize;
use Paypalpayment;
use DateTime;
use App\Customer;
use App\Ticket;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FRPaypalPaymentController extends Controller {

    /**
     * object to authenticate the call.
     * @param object $_apiContext
     */
    private $_apiContext;

    /**
     * Set the ClientId and the ClientSecret.
     * @param
     *string $_ClientId
     *string $_ClientSecret
     */
    private $_ClientId = 'AQ6ugSvBEih299MWXWvR1YM0dw6IL46Jfsb-GVESJghpZ5w8Qd9iClBbgL2hXLGXv8ISL0BgMNjjoMTY';
    private $_ClientSecret='ELsHlhUW1o52s0VYcMWF3daJEuRRNG7lcb2vaNB41ajzIgMxWrDnj1gdGxaf5cALW-OBiGBZvueAdkhE';

    /*
     *   These construct set the SDK configuration dynamiclly,
     *   If you want to pick your configuration from the sdk_config.ini file
     *   make sure to update you configuration there then grape the credentials using this code :
     *   $this->_cred= Paypalpayment::OAuthTokenCredential();
    */
    public function __construct()
    {

        // ### Api Context
        // Pass in a `ApiContext` object to authenticate
        // the call. You can also send a unique request id
        // (that ensures idempotency). The SDK generates
        // a request id if you do not pass one explicitly.

        $this->_apiContext = Paypalpayment::ApiContext($this->_ClientId, $this->_ClientSecret);

        // Uncomment this step if you want to use per request
        // dynamic configuration instead of using sdk_config.ini

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => __DIR__.'/../PayPal.log',
            'log.LogLevel' => 'FINE'
        ));


    }
    /*
    * Display form to process payment using credit card
    */
    public function create()
    {
        return View::make('payment.order');
    }

    /*
    * Process payment using credit card
    */
    public function store(Request $request)
    {
        // ### Address
        // Base Address object used as shipping or billing
        // address in a payment. [Optional]
        $cat_id = $request->get('cat_id', '');
        $temp_id = $request->get('temp_id', '');
        $cardtype = $request->get('creditCardType', '');
        $firstName = $request->get('firstName', '');
        $lastName = $request->get('lastName', '');
        $creditCardNumber = $request->get('creditCardNumber', '');
        $expDateMonth = $request->get('expDateMonth', '');
        $expDateYear = $request->get('expDateYear', '');
        $cvv2Number = $request->get('cvv2Number', '');
        $amount1 = $request->get('amount', '');
        if($amount1 == 0){
            $_SESSION['error_message'] = 'invalid amount';
            return Redirect::back();
        }
        $category = DB::table('hotel_categories as c')
            ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
            ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
            ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
            ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
            ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
            ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
            ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
            ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
            ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
            ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
            ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
            ->orderby('c.id', 'asc')->where('c.id', $cat_id)->first();
        $temp_ticket = DB::table('hotel_temp_ticket')->where('id', $temp_id)->first();


        $desc = 'Category name: '.$category->frname.' Rooms: '.$temp_ticket->rooms.' Arrival Date: '.$temp_ticket->arrival_date.' Departure Date: '.$temp_ticket->departure_date.' Total Price(US $): '.$amount1.' Adult: '.$temp_ticket->adult.' Children:'.$temp_ticket->children;


        $addr = Paypalpayment::address();
        $addr->setLine1("");
        $addr->setLine2("");
        $addr->setCity("");
        $addr->setState("");
        $addr->setPostalCode("");
        $addr->setCountryCode("");
        $addr->setPhone($temp_ticket->phone);

        // ### CreditCard
        $card = Paypalpayment::creditCard();
        $card->setType($cardtype)//MasterCard, Discover, Amex
        ->setNumber($creditCardNumber)
            ->setExpireMonth($expDateMonth)
            ->setExpireYear($expDateYear)
            ->setCvv2($cvv2Number)
            ->setFirstName($firstName)
            ->setLastName($lastName);

        // ### FundingInstrument
        // A resource representing a Payer's funding instrument.
        // Use a Payer ID (A unique identifier of the payer generated
        // and provided by the facilitator. This is required when
        // creating or using a tokenized funding instrument)
        // and the `CreditCardDetails`
        $fi = Paypalpayment::fundingInstrument();
        $fi->setCreditCard($card);

        // ### Payer
        // A resource representing a Payer that funds a payment
        // Use the List of `FundingInstrument` and the Payment Method
        // as 'credit_card'
        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments(array($fi));

//            $item1 = Paypalpayment::item();
//            $item1->setName('Ground Coffee 40 oz')
//                ->setDescription('Ground Coffee 40 oz')
//                ->setCurrency('USD')
//                ->setQuantity(1)
//                ->setTax(0.3)
//                ->setPrice(7.50);
//
//            $item2 = Paypalpayment::item();
//            $item2->setName('Granola bars')
//                ->setDescription('Granola Bars with Peanuts')
//                ->setCurrency('USD')
//                ->setQuantity(5)
//                ->setTax(0.2)
//                ->setPrice(2);
//
//
//            $itemList = Paypalpayment::itemList();
//            $itemList->setItems(array($item1, $item2));


        $details = Paypalpayment::details();
        $details->setShipping("0")
            ->setTax("0")
            //total of items prices
            ->setSubtotal($amount1);

        //Payment Amount
        $amount = Paypalpayment::amount();
        $amount->setCurrency("USD")
            // the total is $17.8 = (16 + 0.6) * 1 ( of quantity) + 1.2 ( of Shipping).
            ->setTotal($amount1)
            ->setDetails($details);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. Transaction is created with
        // a `Payee` and `Amount` types

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            //->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent as 'sale'

        $payment = Paypalpayment::payment();

        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));

        try {
            // ### Create Payment
            // Create a payment by posting to the APIService
            // using a valid ApiContext
            // The return object contains the status;
            $payment->create($this->_apiContext);
        } catch (\PPConnectionException $ex) {

        }

        //dd($payment);
        $result = $payment->toJSON();

        $result = json_decode($result, True);
        //echo $result['payer']['payment_method'];

        if (!empty($result['state']) && $result['state'] == 'approved') {
            $pay_id = $result['id'];
            $cart = '';
            $payment_method = $result['payer']['payment_method'];
            $payer_id = '';
            $card_type = $result['payer']['funding_instruments'][0]['credit_card']['type'];
            $amount = $result['transactions'][0]['amount']['total'];
            $transaction_fee = '';
            $invoice_number = $result['transactions'][0]['invoice_number'];
            $transaction_id = $result['transactions'][0]['related_resources'][0]['sale']['id'];
            $paid_time = $result['create_time'];
            $customer = DB::table('hotel_customers')->where('name', $temp_ticket->name)->where('surname', $temp_ticket->surname)->where('phone', $temp_ticket->phone)->where('email', $temp_ticket->email)->first();
            if(empty($customer)){
                $customer = new Customer();
                $customer->name = $temp_ticket->name;
                $customer->surname = $temp_ticket->surname;
                $customer->phone = $temp_ticket->phone;
                $customer->email = $temp_ticket->email;
                $customer->save();
            }
            $cat_reservation = DB::table('hotel_cat_reservations')->where('cat_id', $cat_id)->first();
            if(!empty($cat_reservation)){
                $remain_num = $cat_reservation->remain_num - $temp_ticket->rooms;
                $book_num = $cat_reservation->book_num + $temp_ticket->rooms;
                DB::table('hotel_cat_reservations')->where('cat_id', $cat_id)->update(['book_num'=>$book_num, 'remain_num'=>$remain_num]);
            }
            $ticket_id = time().''.rand(10, 99);
            $ticket = new Ticket();
            $ticket->ticket_id = $ticket_id;
            $ticket->cat_id = $cat_id;
            $ticket->customer_id = $customer->id;
            $ticket->arrival_date = $temp_ticket->arrival_date;
            $ticket->departure_date = $temp_ticket->departure_date;
            $ticket->rooms = $temp_ticket->rooms;
            $ticket->adult = $temp_ticket->adult;
            $ticket->children = $temp_ticket->children;
            $ticket->price = $amount;
            $ticket->cart = $cart;
            $ticket->pay_id = $pay_id;
            $ticket->payment_method = $payment_method;
            $ticket->card_type = $card_type;
            $ticket->payer_id = $payer_id;
            $ticket->transaction_fee = $transaction_fee;
            $ticket->transaction_id = $transaction_id;
            $ticket->invoice_number = $invoice_number;
            $ticket->paid_time = $paid_time;
            $ticket->save();



            $this->sendMail($ticket, $customer);
            $_SESSION['error_message'] = 'Successfully paid. Thank you.';
            Return Redirect::to('/francais');
        } else {
            if(!empty($result['message'])){
                $_SESSION['error_message'] = $result['message'];
            }
            Return Redirect::back();
        }

    }
    public function setRefund($ticketid = ''){
        if(Sentinel::check()){
            if($ticketid != ''){
                $ticket = DB::table('bt_ticket_payments')->where('ticket_id', $ticketid)->first();
                $amount = $ticket->amount;
                $sale_id = $ticket->transaction_id;
                $tid = $ticket->pay_id;
                $refund_amount = round($amount * 0.8);
                //echo $sale_id.":".$refund_amount;return;
                if($refund_amount != 0){

                    $amount = Paypalpayment::amount();
                    $amount->setCurrency("USD")
                        // the total is $17.8 = (16 + 0.6) * 1 ( of quantity) + 1.2 ( of Shipping).
                        ->setTotal($refund_amount);

                    $refund = Paypalpayment::refund();
                    $refund->setAmount($amount);
                    $sale = Paypalpayment::sale();
                    $sale->setId($sale_id);
                    try {
                        // Refund the sale
                        // (See bootstrap.php for more on `ApiContext`)
                        $refundedSale = $sale->refund($refund, $this->_apiContext);
                        //dd($payment);
                        $result = $refundedSale->toJSON();
                        $result = json_decode($result, true);
                        //print_r($result); return;
                        if (!empty($result['state']) && ($result['state'] == 'pendding' || $result['state'] == 'completed')) {
                            $refund_id = $result['id'];
                            $refund_time = $result['create_time'];
                            $refund_status = $result['state'];
                            $amount = $result['amount']['total'];
                            $sale_id = $result['sale_id'];
                            $invoice_number = '';
                            $parent_payment = '';
                            if(!empty($result['parent_payment']))
                                $parent_payment = $result['parent_payment'];
                            if(!empty($result['invoice_number']))
                                $invoice_number = $result['invoice_number'];
                            DB::table('bt_ticket_refunds')->insert(['ticket_id'=>$ticketid, 'refund_id'=>$refund_id, 'amount'=>$amount, 'transaction_id'=>$sale_id, 'invoice_number'=>$invoice_number, 'parent_payment'=>$parent_payment, 'refund_time'=>$refund_time, 'refund_status'=>$refund_status]);
                            DB::table('bt_ticket_book_info')->where('ticket_id', $ticketid)->update(['paid_status'=>2]);
                            DB::table('bt_ticket_seats')->where('ticket_id', $ticketid)->update(['paid_status'=>2]);
                            $_SESSION['error_message'] = 'Successfully refunded.';
                            return Redirect::to('/booking/viewreturnseat/'.$ticketid.'/departure');
                        }else{
                            $msg = $result['message'];
                            $name = $result['name'];
                            if($name == 'TRANSACTION_REFUSED'){
                                $_SESSION['error_message'] = 'Your payment is pending. you need to wait payment.';
                            }else {
                                $_SESSION['error_message'] = $msg . ' Please re-check.';
                            }
                            return Redirect::back();
                        }
                    } catch (Exception $ex) {
                        // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                        //ResultPrinter::printError("Refund Sale", "Sale", $refundedSale->getId(), $refund, $ex);
                        //exit(1);
                        $_SESSION['error_message'] = 'Failed refund. Please try again.';
                        return Redirect::back();
                    }
                }
                $_SESSION['error_message'] = 'There is not amount to refund. Please re-check.';
                return Redirect::back();
            }
            $_SESSION['error_message'] = 'Failed to get ticket. Please check ticket.';
            return Redirect::back();

        }
        $_SESSION['error_message'] = 'You dont have permission. Please sign in.';
        return Redirect::back();

    }
    public function index()
    {
        echo "<pre>";

        $payments = Paypalpayment::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);

        dd($payments);
    }
    public function show($payment_id)
    {
        $payment = Paypalpayment::getById($payment_id,$this->_apiContext);

        dd($payment);
    }
    public function execute(){
        // Get the payment Object by passing paymentId
        // payment id and payer ID was previously stored in database in
        // create() fuction , this function create payment using "paypal" method
        $paymentId = '';//grape it from DB;
        $PayerID = '';//grape it from DB;
        $payment = Paypalpayment::getById($paymentId, $this->_apiContext);

        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = Paypalpayment::PaymentExecution();
        $execution->setPayerId($PayerID);

        //Execute the payment
        $payment->execute($execution,$this->_apiContext);

    }

    //////paypal
    public function getCheckout(Request $request)
    {
        $cat_id = $request->get('cat_id', '0');
        $temp_id = $request->get('temp_id', '0');
        $category = DB::table('hotel_categories as c')
            ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
            ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
            ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
            ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
            ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
            ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
            ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
            ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
            ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
            ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
            ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
            ->orderby('c.id', 'asc')->where('c.id', $cat_id)->first();
        $temp_ticket = DB::table('hotel_temp_ticket')->where('id', $temp_id)->first();
        $mind = new DateTime($temp_ticket->arrival_date);
        $maxd = new DateTime($temp_ticket->departure_date);
        $day1=$mind->diff($maxd);
        $days = $day1->d;
        $total = round($temp_ticket->rooms * $category->price * $days, 2);


            $desc = 'Category name: '.$category->frname.' Rooms: '.$temp_ticket->rooms.' Arrival Date: '.$temp_ticket->arrival_date.' Departure Date: '.$temp_ticket->departure_date.' Total Price(US $): '.$total.' Adult: '.$temp_ticket->adult.' Children:'.$temp_ticket->children;

            $payer = Paypalpayment::Payer();
            $payer->setPaymentMethod('paypal');
            $amount = Paypalpayment:: Amount();
            $amount->setCurrency('USD');
            $amount->setTotal($total); // This is the simple way,
            // you can alternatively describe everything in the order separately;
            // Reference the PayPal PHP REST SDK for details.

            $transaction = Paypalpayment::Transaction();
            $transaction->setAmount($amount);
            $transaction->setDescription($desc);

            $redirectUrls = Paypalpayment:: RedirectUrls();
            $redirectUrls->setReturnUrl("http://".$_SERVER['HTTP_HOST']."/francais/getdone/".$cat_id.'/'.$temp_id);//action('PaypalPaymentController@getDone')
            $redirectUrls->setCancelUrl("http://".$_SERVER['HTTP_HOST']."/francais/getcancel/".$cat_id.'/'.$temp_id);//action('PaypalPaymentController@getCancel')

            $payment = Paypalpayment::Payment();
            $payment->setIntent('sale');
            $payment->setPayer($payer);//
            $payment->setRedirectUrls($redirectUrls);
            $payment->setTransactions(array($transaction));

            $response = $payment->create($this->_apiContext);
            $redirectUrl = $response->links[1]->href;

            return Redirect::to($redirectUrl);
    }

    public function getDone($cat_id = 0, $temp_id = 0, Request $request)
    {
            $id = $request->get('paymentId');
            $token = $request->get('token');
            $payer_id = $request->get('PayerID');
            $payment = Paypalpayment::getById($id, $this->_apiContext);

            $paymentExecution = Paypalpayment::PaymentExecution();

            $paymentExecution->setPayerId($payer_id);
            $executePayment = $payment->execute($paymentExecution, $this->_apiContext);

            // Clear the shopping cart, write to database, send notifications, etc.
            $result = $executePayment->toJSON();

            $result = json_decode($result, True);

            //echo $result['payer']['payment_method'];
            $category = DB::table('hotel_categories as c')
                ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
                ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
                ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
                ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
                ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
                ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
                ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
                ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
                ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
                ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
                ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
                ->orderby('c.id', 'asc')->where('c.id', $cat_id)->first();
            $temp_ticket = DB::table('hotel_temp_ticket')->where('id', $temp_id)->first();
            if ($result['state'] == 'approved') {
                $pay_id = $result['id'];
                $cart = $result['cart'];
                $payment_method = $result['payer']['payment_method'];
                $payer_id = $result['payer']['payer_info']['payer_id'];
                $amount = $result['transactions'][0]['amount']['total'];
                $transaction_fee = $result['transactions'][0]['related_resources'][0]['sale']['transaction_fee']['value'];
                $transaction_id = $result['transactions'][0]['related_resources'][0]['sale']['id'];
                $paid_time = $result['create_time'];
                $invoice_number = '';
                if(!empty($result['transactions'][0]['invoice_number']))
                    $invoice_number = $result['transactions'][0]['invoice_number'];
                $customer = DB::table('hotel_customers')->where('name', $temp_ticket->name)->where('surname', $temp_ticket->surname)->where('phone', $temp_ticket->phone)->where('email', $temp_ticket->email)->first();
                if(empty($customer)){
                    $customer = new Customer();
                    $customer->name = $temp_ticket->name;
                    $customer->surname = $temp_ticket->surname;
                    $customer->phone = $temp_ticket->phone;
                    $customer->email = $temp_ticket->email;
                    $customer->save();
                }
                $cat_reservation = DB::table('hotel_cat_reservations')->where('cat_id', $cat_id)->first();
                if(!empty($cat_reservation)){
                    $remain_num = $cat_reservation->remain_num - $temp_ticket->rooms;
                    $book_num = $cat_reservation->book_num + $temp_ticket->rooms;
                    DB::table('hotel_cat_reservations')->where('cat_id', $cat_id)->update(['book_num'=>$book_num, 'remain_num'=>$remain_num]);
                }
                $ticket_id = time().''.rand(10, 99);
                $ticket = new Ticket();
                $ticket->ticket_id = $ticket_id;
                $ticket->cat_id = $cat_id;
                $ticket->customer_id = $customer->id;
                $ticket->arrival_date = $temp_ticket->arrival_date;
                $ticket->departure_date = $temp_ticket->departure_date;
                $ticket->rooms = $temp_ticket->rooms;
                $ticket->price = $amount;
                $ticket->adult = $temp_ticket->adult;
                $ticket->children = $temp_ticket->children;
                $ticket->cart = $cart;
                $ticket->pay_id = $pay_id;
                $ticket->payment_method = $payment_method;
                $ticket->payer_id = $payer_id;
                $ticket->transaction_fee = $transaction_fee;
                $ticket->transaction_id = $transaction_id;
                $ticket->invoice_number = $invoice_number;
                $ticket->paid_time = $paid_time;
                $ticket->save();


                //send mail
                $this->sendMail($ticket, $customer);
                $_SESSION['error_message'] = 'Successfully paid. Thank you.';
                Return Redirect::to('/francais');
            } else {
                $_SESSION['error_message'] = 'Payment auth failed. Please try again.';
                //print_r($result);
                Return Redirect::to('/francais/reservation/payment/'.$cat_id.'/' . $temp_id);
            }

    }

    public function getCancel($cat_id = 0, $temp_id = 0)
    {
        // Curse and humiliate the user for cancelling this most sacred payment (yours)
        Return Redirect::to('/francais/reservation/payment/'.$cat_id.'/' . $temp_id);
    }
    public function sendMail($ticket, $customer){
        try{
            $category = DB::table('hotel_categories as c')
                ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
                ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
                ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
                ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
                ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
                ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
                ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
                ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
                ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
                ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
                ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
                ->orderby('c.id', 'asc')->where('c.id', $ticket->cat_id)->first();

            $fmail = $customer->email;

            $pay_method = 'paypal';
            if($ticket->card_type != ''){
                $pay_method = $ticket->card_type;
            }
            $contents ='<style type="text/css">'.
                'body { font-family: Verdana;}'.
                'div.invoice { border:1px solid #ccc;  padding:10px;  width:700px; }'.
                'div.company-address {  border:1px solid #ccc;   float:left; width:300pt; }'.
                'div.invoice-details {    border:1px solid #ccc;  float:right;  width:200px; }'.
                'div.customer-address {   border:1px solid #ccc; float:right;  margin-bottom:50px;  margin-top:100px; width:200pt; }'.
                'div.clear-fix {clear:both; float:none;}'.
                'table { width:100%;}'.
                'th {    text-align: left;  border: 1px solid #ddd !important;}'.
                '.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th { padding: 8px;line-height: 1.42857143; vertical-align: top; border: 1px solid #ddd;}'.
                'td {}'.
                '.text-left {text-align:left;}'.
                '.text-center {  text-align:center; }'.
                '.text-right { text-align:right;}'.
                '</style>'.
                '<div class="" style="width:770px;">'.
                '<h1><span class="font-light">'.$ticket->ticket_id.' Ticket Payment</span></h1>'.
                '<p>'.
                '<div class="login-page">'.
                '<div class="form">'.
                '<div class="invoice">'.
                '<div class="invoice-details" style="text-align: left;width:70%; border:0;float: left;">'.
                'Ticket NO: '.$ticket->ticket_id.
                '<br />'.
                'Payment Date: '.$ticket->paid_time.
                '<br>'.
                'Payment Type: '.$pay_method.
                '<br>'.
                'Payment fee: '.$ticket->transaction_fee.
                '<br>'.
                'Email: '.$customer->email.
                '<br />'.
                'Phone: '.$customer->phone.
                '<br />'.
                '</div>'.
                '<div class="clear-fix" style="margin-bottom:10px;"></div>'.
                '<table border="1" class="table" cellspacing="0" style="width:100%">'.
                '<tr>'.
                '<th style="text-align:center">Content</th>'.
                '<th style="text-align:center">Rooms</th>'.
                '<th style="text-align:center">Unit (US $)</th>'.
                '<th style="text-align:center">Total (US $)</th>'.
                '</tr>'.
                '<tr>'.
                '<td class="text-left">'.
                'Category Name: '.$category->name.
                '<br>'.
                'Arrival Date: '.$ticket->arrival_date.
                '<br>'.
                'Departure Date : '.$ticket->departure_date;

            $contents = $contents.'</td>'.
                '<td class="text-center">'.
                ''.$ticket->rooms.'';
            $contents = $contents.'</td>'.
                '<td class="text-right">'.$category->price.
                '</td>'.
                '<td class="text-right">'.
                $ticket-> price.'<br>';

            $contents = $contents.'</td>'.
                '</tr>'.
                '<tr>'.
                '<td colspan="3" class="text-right"><b>TOTAL</b></td>'.
                '<td class="text-right"><b>(US $) '.$ticket-> price.'</b></td>'.
                '</tr>'.
                '</table>'.
                '<div class="row" style="border:0;width:100%;text-align: center">'.
                '<span><br><br><br><b>Ihusigroup Hotels<b></span>'.
                '</div>'.
                '</div>'.
                '</div>'.
                '</div>'.
                '</p>'.
                '</div>';
            $subjects = 'Reservated Ticket';
            $data = array('email' => $fmail, 'content' => $contents, 'subject' => $subjects);
            $ch = array();
            $mh = curl_multi_init();
            $ch[0] = curl_init();
            // set URL and other appropriate options
            curl_setopt($ch[0], CURLOPT_URL, $_SERVER['HTTP_HOST']."/mail/test/testemail.php");
            curl_setopt($ch[0], CURLOPT_HEADER, 0);
            curl_setopt($ch[0], CURLOPT_POST, true);
            curl_setopt($ch[0], CURLOPT_POSTFIELDS, $data);
            curl_multi_add_handle($mh,$ch[0]);

            $active = null;

            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);

            while ($active && $mrc == CURLM_OK)
            {
                // add this line
                while (curl_multi_exec($mh, $active) === CURLM_CALL_MULTI_PERFORM);

                if (curl_multi_select($mh) != -1)
                {
                    do {
                        $mrc = curl_multi_exec($mh, $active);
                        if ($mrc == CURLM_OK)
                        {
                        }
                    } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                }
            }

            //close the handles
                curl_multi_remove_handle($mh, $ch[0]);
            curl_multi_close($mh);

            // close cURL resource, and free up system resources
            //var_dump($re);
            //return;
            //return 1;
        }catch(Exception $e){
            //return back()->with('message','Your mail failed. Please try again')->with('message_type','error');
        }
    }

}
