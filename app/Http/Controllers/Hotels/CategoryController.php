<?php namespace App\Http\Controllers\Hotels;

use Lang;
use Redirect;
use View;
use DB;
use Validator;
use Input;
use App\ImageResize;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Show a list of all the groups.
     *
     * @return View
     */
    public function getList()
    {
        return View('categorylist');
    }
    public function getFRList()
    {
        return View('fr.categorylist');
    }
    public function getCatItem($cat_id){
        return View('catitem', compact('cat_id'));
    }
    public function getFRCatItem($cat_id){
        return View('fr.catitem', compact('cat_id'));
    }


}
