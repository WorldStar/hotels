<?php namespace App\Http\Controllers\Hotels;

use Lang;
use Redirect;
use View;
use DB;
use Validator;
use Input;
use App\ImageResize;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TempTicket;
use DateTime;
class ReservationController extends Controller
{
    /**
     * Show a list of all the groups.
     *
     * @return View
     */
    public function setReservation(Request $request)
    {
        $reservationstep = $request->get('reservationstep', '');
        $html = '';
        if($reservationstep == "one"){
            $dpd1 = $_POST['dpd1'];
            $dpd2 = $_POST['dpd2'];
            $rooms	= $_POST['rooms'];
            $adult	= $_POST['adult'];
            $children = $_POST['children'];
            $dpd1 = date('Y-m-d', strtotime($dpd1));
            $dpd2 = date('Y-m-d', strtotime($dpd2));
            $tempticket = new TempTicket();
            $tempticket->arrival_date = $dpd1;
            $tempticket->departure_date = $dpd2;
            $tempticket->rooms = $rooms;
            $tempticket->adult = $adult;
            $tempticket->children = $children;
            $tempticket->save();

            $html = "<form action=\"#\" method=\"post\" id=\"ajax-reservation-send\">";
            $html .= "<input type=\"hidden\" name=\"_token\" value=\"".csrf_token()."\" />";
            $html .= "<input type=\"hidden\" name=\"temp_id\" value=\"".$tempticket->id."\" />";
            $html .= "<ul class=\"clearfix\">";
            $html .= "<input type=\"hidden\" name=\"dpd1\" value=\"$dpd1\" /><input type=\"hidden\" name=\"dpd2\" value=\"$dpd2\" /><input type=\"hidden\" name=\"rooms\" value=\"$rooms\" /><input type=\"hidden\" name=\"adult\" value=\"$adult\" /><input type=\"hidden\" name=\"children\" value=\"$children\" />";
            $html .= "<li class=\"li-input sec-input\"><label>NAME</label><input type=\"text\" id=\"name\" name=\"name\" /></li>";
            $html .= "<li class=\"li-input sec-input\"><label>SURNAME</label><input type=\"text\" id=\"surname\" name=\"surname\" /></li>";
            $html .= "<li class=\"li-input sec-input\"><label>PHONE</label><input type=\"text\" id=\"phone\" name=\"phone\" /></li>";
            $html .= "<li class=\"li-input sec-input\"><label>E-MAIL</label><input type=\"text\" id=\"mail\" name=\"mail\" /></li>";
            $html .= "<li><div class=\"button-style-1 margint40\"><a id=\"res-send\" href=\"#\"><i class=\"fa fa-search\"></i>SEND</a></div></li>";
            $html .= "</ul>";
            $html .= "</form>";
        }elseif($reservationstep == "two"){
            $dpd1 = $_POST['dpd1'];
            $dpd2 = $_POST['dpd2'];
            $rooms	= $_POST['rooms'];
            $adult	= $_POST['adult'];
            $children = $_POST['children'];
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $phone = $_POST['phone'];
            $mail = $_POST['mail'];
            $temp_id = $_POST['temp_id'];
            $tempticket = TempTicket::find($temp_id);
            $tempticket->name = $name;
            $tempticket->surname = $surname;
            $tempticket->phone = $phone;
            $tempticket->email = $mail;
            $tempticket->save();
            $mainemail = "ihusihotel@yahoo.fr";//Change this mail address.

            $mailinfo  = "MIME-Version: 1.0\r\n";
            $mailinfo .= "Content-type: text/html; charset=utf-8\n";
            $mailinfo .= "From: Ihusi Hotel Reservation \r\n";
            $mailinfo .= "Reply-To: $name <$mail>\r\n";
            $sms  = "Name : ".$name." ".$surname."<br />E-Mail : ".$mail."<br />Phone : ".$phone."<br />Arrival : ".$dpd1."<br />Departure : ".$dpd2."<br />Rooms : ".$rooms."<br />Adult : ".$adult."<br />Children : ".$children;
            $categories = DB::table('hotel_categories as c')
                ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
                ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
                ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
                ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
                ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
                ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
                ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
                ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
                ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
                ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
                ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
                ->orderby('c.id', 'asc')->get();
            $html = '<style type="text/css">'.
                'body { font-family: Verdana;}'.
                'div.invoice { border:1px solid #ccc;  padding:10px;  width:700px; }'.
                'div.company-address {  border:1px solid #ccc;   float:left; width:300pt; }'.
                'div.invoice-details {    border:1px solid #ccc;  float:right;  width:200px; }'.
                'div.customer-address {   border:1px solid #ccc; float:right;  margin-bottom:50px;  margin-top:100px; width:200pt; }'.
                'div.clear-fix {clear:both; float:none;}'.
                'table { width:100%;}'.
                'th {    text-align: left;  border: 1px solid #ddd !important;}'.
                '.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th { padding: 8px;line-height: 1.42857143; vertical-align: top; border: 1px solid #ddd;}'.
                'td {}'.
                '.text-left {text-align:left;}'.
                '.text-center {  text-align:center; }'.
                '.text-right { text-align:right;}'.
                '</style>';
            $html .= '<table class="table" cellspacing="0" style="width:100%">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">CATEGORY NAME</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">PRICE (US $)</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">ROOMS</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">ARRIVAL DATE</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">DEPARTURE DATE</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">TOTAL PRICE (US $)</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">&nbsp;</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            $k = 0;
            foreach($categories as $category){
                $mind = new DateTime($dpd1);
                $maxd = new DateTime($dpd2);
                $day1=$mind->diff($maxd);
                $days = $day1->d;
                $flg = 0;
                for($i = 0; $i < $days; $i++){
                    $dd = date('Y-m-d', strtotime($dpd1.'+'.$i.' days'));
                    $tickets = DB::table('hotel_tickets')->where('cat_id', $category->id)->where('arrival_date', '<=', $dd)->where('departure_date', '>=', $dd)->where('status', 1)->get();
                    $room_num = 0;
                    foreach($tickets as $ticket){
                        $room_num = $room_num + $ticket->rooms;
                    }
                    if($rooms > $category->rooms || ($category->rooms - $room_num) < $rooms){
                        $flg = 1; break;
                    }
                }
                if($flg == 0){
                    $k++;
                    $html .= '<tr>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$category->name.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$category->price.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$rooms.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$dpd1.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$dpd2.'</td>';
                    $total = round($rooms * $category->price * $days, 2);
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$total.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><a href="http://' . $_SERVER['HTTP_HOST'].'/reservation/payment/'.$category->id.'/'.$temp_id.'">Reservation</a></td>';
                    $html .= '</tr>';

                }
            }
            $html .= '</tbody>';
            $html .= '</table><br><br><span style="color:red;font-size:15px;">※&nbsp;&nbsp;If you want to really refund after reservated, you can send the reply mail to support.</span><br><br>Thank you and Regards.<br>Support';
            if($k > 0){
                $sms .= '<br><br>'.$html;
            }

            try{
                $subjects = 'Searched Categories';
                $data = array('email' => $mail, 'content' => $sms, 'subject' => $subjects);
                $ch = array();
                $mh = curl_multi_init();
                $ch[0] = curl_init();
                // set URL and other appropriate options
                curl_setopt($ch[0], CURLOPT_URL, $_SERVER['HTTP_HOST']."/mail/test/testemail.php");
                curl_setopt($ch[0], CURLOPT_HEADER, 0);
                curl_setopt($ch[0], CURLOPT_POST, true);
                curl_setopt($ch[0], CURLOPT_POSTFIELDS, $data);
                curl_multi_add_handle($mh,$ch[0]);

                $active = null;

                do {
                    $mrc = curl_multi_exec($mh, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);

                while ($active && $mrc == CURLM_OK)
                {
                    // add this line
                    while (curl_multi_exec($mh, $active) === CURLM_CALL_MULTI_PERFORM);

                    if (curl_multi_select($mh) != -1)
                    {
                        do {
                            $mrc = curl_multi_exec($mh, $active);
                            if ($mrc == CURLM_OK)
                            {
                            }
                        } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                    }
                }

                //close the handles
                curl_multi_remove_handle($mh, $ch[0]);
                curl_multi_close($mh);
                $html = "<p class=\"reservation-confirm\">Thanks for your request for a reservation. We're checking availability and we'll get back as mail to you as soon as possible.</p>";
            }catch(Exception $e){
                $html = "<p class=\"reservation-confirm\">Error! Please try again.</p>";
            }
            /*
            $mail = mail($mainemail, $name ,stripslashes($sms), $mailinfo);
            if($mail){
                $html = "<p class=\"reservation-confirm\">Thanks for your request for a reservation. We're checking availability and we'll get back as mail to you as soon as possible.</p>";
            }
            else{
                $html = "<p class=\"reservation-confirm\">Error! Please try again.</p>";
            }*/
        }else{
            $html = "Error!";
        }
        // Show the page
        return $html;
    }
    public function setFRReservation(Request $request)
    {
        $reservationstep = $request->get('reservationstep', '');
        $html = '';

        if($reservationstep == "one"){
            $dpd1 = $_POST['dpd1'];
            $dpd2 = $_POST['dpd2'];
            $rooms	= $_POST['rooms'];
            $adult	= $_POST['adult'];
            $children = $_POST['children'];

            $dpd1 = date('Y-m-d', strtotime($dpd1));
            $dpd2 = date('Y-m-d', strtotime($dpd2));
            $tempticket = new TempTicket();
            $tempticket->arrival_date = $dpd1;
            $tempticket->departure_date = $dpd2;
            $tempticket->rooms = $rooms;
            $tempticket->adult = $adult;
            $tempticket->children = $children;
            $tempticket->save();

            $html = "<form action=\"#\" method=\"post\" id=\"ajax-reservation-send\">";
            $html .= "<input type=\"hidden\" name=\"_token\" value=\"".csrf_token()."\" />";
            $html .= "<input type=\"hidden\" name=\"temp_id\" value=\"".$tempticket->id."\" />";
            $html .= "<ul class=\"clearfix\">";
            $html .= "<input type=\"hidden\" name=\"dpd1\" value=\"$dpd1\" /><input type=\"hidden\" name=\"dpd2\" value=\"$dpd2\" /><input type=\"hidden\" name=\"rooms\" value=\"$rooms\" /><input type=\"hidden\" name=\"adult\" value=\"$adult\" /><input type=\"hidden\" name=\"children\" value=\"$children\" />";
            $html .= "<li class=\"li-input sec-input\"><label>NOM</label><input type=\"text\" id=\"name\" name=\"name\" /></li>";
            $html .= "<li class=\"li-input sec-input\"><label>POSTNOM</label><input type=\"text\" id=\"surname\" name=\"surname\" /></li>";
            $html .= "<li class=\"li-input sec-input\"><label>TELEPHONE</label><input type=\"text\" id=\"phone\" name=\"phone\" /></li>";
            $html .= "<li class=\"li-input sec-input\"><label>E-MAIL</label><input type=\"text\" id=\"mail\" name=\"mail\" /></li>";
            $html .= "<li><div class=\"button-style-1 margint40\"><a id=\"res-send\" href=\"#\"><i class=\"fa fa-search\"></i>ENVOYER</a></div></li>";
            $html .= "</ul>";
            $html .= "</form>";
        }elseif($reservationstep == "two"){
            $dpd1 = $_POST['dpd1'];
            $dpd2 = $_POST['dpd2'];
            $rooms	= $_POST['rooms'];
            $adult	= $_POST['adult'];
            $children = $_POST['children'];
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $phone = $_POST['phone'];
            $mail = $_POST['mail'];
            $temp_id = $_POST['temp_id'];

            $tempticket = TempTicket::find($temp_id);
            $tempticket->name = $name;
            $tempticket->surname = $surname;
            $tempticket->phone = $phone;
            $tempticket->email = $mail;
            $tempticket->save();

            $mainemail = "ihusihotel@yahoo.fr";//Change this mail address.

            $mailinfo  = "MIME-Version: 1.0\r\n";
            $mailinfo .= "Content-type: text/html; charset=utf-8\n";
            $mailinfo .= "From: Ihusi Hotel Reservation \r\n";
            $mailinfo .= "Reply-To: $name <$mail>\r\n";
            $sms  = "Name : ".$name." ".$surname."<br />E-Mail : ".$mail."<br />Phone : ".$phone."<br />Arrival : ".$dpd1."<br />Departure : ".$dpd2."<br />Rooms : ".$rooms."<br />Adult : ".$adult."<br />Children : ".$children;
            $categories = DB::table('hotel_categories as c')
                ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
                ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
                ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
                ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
                ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
                ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
                ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
                ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
                ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
                ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
                ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
                ->orderby('c.id', 'asc')->get();
            $html = '<style type="text/css">'.
                'body { font-family: Verdana;}'.
                'div.invoice { border:1px solid #ccc;  padding:10px;  width:700px; }'.
                'div.company-address {  border:1px solid #ccc;   float:left; width:300pt; }'.
                'div.invoice-details {    border:1px solid #ccc;  float:right;  width:200px; }'.
                'div.customer-address {   border:1px solid #ccc; float:right;  margin-bottom:50px;  margin-top:100px; width:200pt; }'.
                'div.clear-fix {clear:both; float:none;}'.
                'table { width:100%;}'.
                'th {    text-align: left;  border: 1px solid #ddd !important;}'.
                '.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th { padding: 8px;line-height: 1.42857143; vertical-align: top; border: 1px solid #ddd;}'.
                'td {}'.
                '.text-left {text-align:left;}'.
                '.text-center {  text-align:center; }'.
                '.text-right { text-align:right;}'.
                '</style>';
            $html .= '<table class="table" cellspacing="0" style="width:100%">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">FRANCAIS NAME</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">PRIX (US $)</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">CHAMBRES</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">ARRIVER</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">DEPART</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">TOTAL PRIX (US $)</th>';
            $html .= '<th style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">&nbsp;</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            $k = 0;
            foreach($categories as $category){
                $mind = new DateTime($dpd1);
                $maxd = new DateTime($dpd2);
                $day1=$mind->diff($maxd);
                $days = $day1->d;
                $flg = 0;
                for($i = 0; $i < $days; $i++){
                    $dd = date('Y-m-d', strtotime($dpd1.'+'.$i.' days'));
                    $tickets = DB::table('hotel_tickets')->where('cat_id', $category->id)->where('arrival_date', '<=', $dd)->where('departure_date', '>=', $dd)->where('status', 1)->get();
                    $room_num = 0;
                    foreach($tickets as $ticket){
                        $room_num = $room_num + $ticket->rooms;
                    }
                    if($rooms > $category->rooms || ($category->rooms - $room_num) < $rooms){
                        $flg = 1; break;
                    }
                }
                if($flg == 0){
                    $k++;
                    $html .= '<tr>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$category->frname.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$category->price.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$rooms.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$dpd1.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$dpd2.'</td>';
                    $total = round($rooms * $category->price * $days, 2);
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">'.$total.'</td>';
                    $html .= '<td style="text-align: center;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><a href="http://' . $_SERVER['HTTP_HOST'].'/francais/reservation/payment/'.$category->id.'/'.$temp_id.'">Reservation</a></td>';
                    $html .= '</tr>';

                }
            }
            $html .= '</tbody>';
            $html .= '</table><br><br><span style="color:red;font-size:15px;">※&nbsp;&nbsp;If you want to really refund after reservated, you can send the reply mail to support.</span><br><br>Thank you and Regards.<br>Support';

            if($k > 0){
                $sms .= '<br><br>'.$html;
            }


            //$mail = mail($mainemail, $name ,stripslashes($sms), $mailinfo);
            try{
                $subjects = 'Searched Categories';
                $data = array('email' => $mail, 'content' => $sms, 'subject' => $subjects);
                $ch = array();
                $mh = curl_multi_init();
                $ch[0] = curl_init();
                // set URL and other appropriate options
                curl_setopt($ch[0], CURLOPT_URL, $_SERVER['HTTP_HOST']."/mail/test/testemail.php");
                curl_setopt($ch[0], CURLOPT_HEADER, 0);
                curl_setopt($ch[0], CURLOPT_POST, true);
                curl_setopt($ch[0], CURLOPT_POSTFIELDS, $data);
                curl_multi_add_handle($mh,$ch[0]);

                $active = null;

                do {
                    $mrc = curl_multi_exec($mh, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);

                while ($active && $mrc == CURLM_OK)
                {
                    // add this line
                    while (curl_multi_exec($mh, $active) === CURLM_CALL_MULTI_PERFORM);

                    if (curl_multi_select($mh) != -1)
                    {
                        do {
                            $mrc = curl_multi_exec($mh, $active);
                            if ($mrc == CURLM_OK)
                            {
                            }
                        } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                    }
                }

                //close the handles
                curl_multi_remove_handle($mh, $ch[0]);
                curl_multi_close($mh);
                $html = "<p class=\"reservation-confirm\">Merci pour votre demande de réservation. Nous vérifions la disponibilité et nous allons revenir à vous dès que possible oh mail.</p>";
            }catch(Exception $e){
                $html = "<p class=\"reservation-confirm\">Erreur! Veuillez réessayer.</p>";
            }
        }else{
            $html = "Erreur!";
        }
        // Show the page
        return $html;
    }
    public function payReservation($cat_id = 0, $temp_id = 0){
        $category = DB::table('hotel_categories as c')
            ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
            ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
            ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
            ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
            ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
            ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
            ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
            ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
            ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
            ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
            ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
            ->orderby('c.id', 'asc')->where('c.id', $cat_id)->first();
        $temp_ticket = DB::table('hotel_temp_ticket')->where('id', $temp_id)->first();
        return View('reservation', compact('cat_id', 'temp_id', 'category', 'temp_ticket'));
    }
    public function payFRReservation($cat_id = 0, $temp_id = 0){
        $category = DB::table('hotel_categories as c')
            ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
            ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
            ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
            ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
            ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
            ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
            ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
            ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
            ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
            ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
            ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
            ->orderby('c.id', 'asc')->where('c.id', $cat_id)->first();
        $temp_ticket = DB::table('hotel_temp_ticket')->where('id', $temp_id)->first();
        return View('fr.reservation', compact('cat_id', 'temp_id', 'category', 'temp_ticket'));
    }

    public function setCreditcard(Request $request){
            $_SESSION['error_message'] = '';
            $cat_id = $request->get('cat_id', '');
            $temp_id = $request->get('temp_id', '');
        $category = DB::table('hotel_categories as c')
            ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
            ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
            ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
            ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
            ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
            ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
            ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
            ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
            ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
            ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
            ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
            ->orderby('c.id', 'asc')->where('c.id', $cat_id)->first();
        $temp_ticket = DB::table('hotel_temp_ticket')->where('id', $temp_id)->first();
        $mind = new DateTime($temp_ticket->arrival_date);
        $maxd = new DateTime($temp_ticket->departure_date);
        $day1=$mind->diff($maxd);
        $days = $day1->d;
        $total = round($temp_ticket->rooms * $category->price * $days, 2);
            return View('setcreditcard', compact('cat_id', 'temp_id', 'category', 'temp_ticket', 'total'));
    }
    public function setFRCreditcard(Request $request){
        $_SESSION['error_message'] = '';
        $cat_id = $request->get('cat_id', '');
        $temp_id = $request->get('temp_id', '');
        $category = DB::table('hotel_categories as c')
            ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
            ->join('hotel_cat_reservations as cr', 'cr.cat_id', '=', 'c.id')
            ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
            ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
            ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
            ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
            ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
            ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
            ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
            ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
            ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])
            ->orderby('c.id', 'asc')->where('c.id', $cat_id)->first();
        $temp_ticket = DB::table('hotel_temp_ticket')->where('id', $temp_id)->first();
        $mind = new DateTime($temp_ticket->arrival_date);
        $maxd = new DateTime($temp_ticket->departure_date);
        $day1=$mind->diff($maxd);
        $days = $day1->d;
        $total = round($temp_ticket->rooms * $category->price * $days, 2);
        return View('fr.setcreditcard', compact('cat_id', 'temp_id', 'category', 'temp_ticket', 'total'));
    }

}
