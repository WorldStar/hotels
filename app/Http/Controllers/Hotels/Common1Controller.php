<?php namespace App\Http\Controllers\Hotels;

use Lang;
use Redirect;
use View;
use DB;
use Validator;
use Input;
use App\ImageResize;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Common1Controller extends Controller
{
    /**
     * Show a list of all the groups.
     *
     * @return View
     */
    public function about()
    {
        return View('about');
    }
    public function getList()
    {
        return View('categorylist');
    }
}
