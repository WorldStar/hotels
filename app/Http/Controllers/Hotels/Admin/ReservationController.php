<?php

namespace App\Http\Controllers\Hotels\Admin;

use App\Occasion;
use App\Http\Requests;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Datatables;
use App\User;
use DB;
use Redirect;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
    }
    public function getlist()
    {
        $reservations = DB::table('hotel_tickets as t')
            ->join('hotel_customers as c', 't.customer_id', '=', 'c.id')
            ->join('hotel_categories as cc', 't.cat_id', '=', 'cc.id')
            ->select('t.*', 'c.name as customername', 'c.surname', 'c.phone', 'c.email', 'cc.name as categoryname')->where('t.status', 1)->where('departure_date', '>', date('Y-m-d'))->orderby('t.departure_date', 'desc')->get();

        return View('admin/hotel/tickets/paid', compact('reservations'));
    }
    public function passinglist()
    {
        $reservations = DB::table('hotel_tickets as t')
            ->join('hotel_customers as c', 't.customer_id', '=', 'c.id')
            ->join('hotel_categories as cc', 't.cat_id', '=', 'cc.id')
            ->select('t.*', 'c.name as customername', 'c.surname', 'c.phone', 'c.email', 'cc.name as categoryname')->where('t.status', 1)->where('departure_date', '=', date('Y-m-d'))->orderby('t.departure_date', 'desc')->get();

        return View('admin/hotel/tickets/passing', compact('reservations'));
    }
    public function passedlist()
    {
        $reservations = DB::table('hotel_tickets as t')
            ->join('hotel_customers as c', 't.customer_id', '=', 'c.id')
            ->join('hotel_categories as cc', 't.cat_id', '=', 'cc.id')
            ->select('t.*', 'c.name as customername', 'c.surname', 'c.phone', 'c.email', 'cc.name as categoryname')->where('t.status', 2)->orderby('t.departure_date', 'desc')->get();

        return View('admin/hotel/tickets/passed', compact('reservations'));
    }
    public function cancellist()
    {
        $reservations = DB::table('hotel_tickets as t')
            ->join('hotel_customers as c', 't.customer_id', '=', 'c.id')
            ->join('hotel_categories as cc', 't.cat_id', '=', 'cc.id')
            ->select('t.*', 'c.name as customername', 'c.surname', 'c.phone', 'c.email', 'cc.name as categoryname')->where('t.status', 0)->orderby('t.departure_date', 'desc')->get();

        return View('admin/hotel/tickets/cancel', compact('reservations'));
    }
    public function passedTicket($ticket_id = 0){
        $ticket = DB::table('hotel_tickets')->where('ticket_id', $ticket_id)->first();
        if(!empty($ticket)){
            $room = $ticket->rooms;
            $cat_id = $ticket->cat_id;
            $cat_reservation = DB::table('hotel_cat_reservations')->where('cat_id', $cat_id)->first();
            if(!empty($cat_reservation)){
                $book_num = $cat_reservation->book_num;
                $remain_num = $cat_reservation->remain_num;
                $book_num = $book_num - $room;
                $remain_num = $remain_num + $room;
                DB::table('hotel_cat_reservations')->where('cat_id', $cat_id)->update(['book_num'=>$book_num, 'remain_num'=>$remain_num]);
                DB::table('hotel_tickets')->where('ticket_id', $ticket_id)->update(['status'=>2, 'closed_date'=>date('Y-m-d H:i:s')]);
            }
        }
        return Redirect::back();
    }
    public function cancelTicket($ticket_id = 0){
        $ticket = DB::table('hotel_tickets')->where('ticket_id', $ticket_id)->first();
        if(!empty($ticket)){
            $room = $ticket->rooms;
            $cat_id = $ticket->cat_id;
            $cat_reservation = DB::table('hotel_cat_reservations')->where('cat_id', $cat_id)->first();
            if(!empty($cat_reservation)){
                $book_num = $cat_reservation->book_num;
                $remain_num = $cat_reservation->remain_num;
                $book_num = $book_num - $room;
                $remain_num = $remain_num + $room;
                DB::table('hotel_cat_reservations')->where('cat_id', $cat_id)->update(['book_num'=>$book_num, 'remain_num'=>$remain_num]);
                DB::table('hotel_tickets')->where('ticket_id', $ticket_id)->update(['status'=>0, 'closed_date'=>date('Y-m-d H:i:s')]);
                //add refund function


            }
        }
        return Redirect::back();
    }





    public function getCustomer($id = 0)
    {
        //$customer = DB::table('users')->where('id', $id)->first();
        $customer = User::find($id);
        return json_encode($customer);
    }
    public function inactiveCustomer($id = 0, $status)
    {
        if($status == 1)
            $status = 0;
        else
            $status = 1;
        DB::table('users')->where('id', $id)->update(['status'=>$status]);
        $data = array("status"=>$status);
        return $data;
    }
}

