<?php

namespace App\Http\Controllers\Hotels\Admin;

use App\Occasion;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use App\Category;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Customer;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.occasion.occasion');
    }
	
    public function getlist()
    {
        $categories = Category::orderBy('id', 'asc')->get();
		return View('admin/hotel/category/category', compact('categories'));
    }
    public function getstatus()
    {
        $categories = DB::table('hotel_cat_reservations as r')
            ->join('hotel_categories as c', 'r.cat_id', '=', 'c.id')
            ->select(['r.cat_id', 'r.book_num', 'r.remain_num', 'c.name', 'c.rooms', 'c.price'])->orderby('r.cat_id', 'asc')->get();

        return View('admin/hotel/category/categorystatus', compact('categories'));
    }
    public function getcustomers()
    {
        $customers = DB::table('hotel_customers')->orderby('id', 'asc')->get();

        return View('admin/hotel/customer/list', compact('customers'));
    }

    public function getitem($cat_id = 0)
    {
        $airs = DB::table('hotel_airs')->orderby('id', 'asc')->get();
        $balconies = DB::table('hotel_balconies')->orderby('id', 'asc')->get();
        $beds = DB::table('hotel_beds')->orderby('id', 'asc')->get();
        $desks = DB::table('hotel_desks')->orderby('id', 'asc')->get();
        $safes = DB::table('hotel_safes')->orderby('id', 'asc')->get();
        $tvs = DB::table('hotel_tvs')->orderby('id', 'asc')->get();
        $wardrobes = DB::table('hotel_wardrobes')->orderby('id', 'asc')->get();
        $category = DB::table('hotel_categories')->where('id', $cat_id)->orderby('id', 'asc')->first();
        $option = DB::table('hotel_options')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
        $photo = DB::table('hotel_photos')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
        return View('admin/hotel/category/view', compact('airs', 'balconies', 'beds', 'desks', 'safes', 'tvs', 'wardrobes', 'category', 'option', 'photo'));
    }
    public function getrefunditem($cat_id = 0, $ticket_id = 0)
    {
        $airs = DB::table('hotel_airs')->orderby('id', 'asc')->get();
        $balconies = DB::table('hotel_balconies')->orderby('id', 'asc')->get();
        $beds = DB::table('hotel_beds')->orderby('id', 'asc')->get();
        $desks = DB::table('hotel_desks')->orderby('id', 'asc')->get();
        $safes = DB::table('hotel_safes')->orderby('id', 'asc')->get();
        $tvs = DB::table('hotel_tvs')->orderby('id', 'asc')->get();
        $wardrobes = DB::table('hotel_wardrobes')->orderby('id', 'asc')->get();
        $category = DB::table('hotel_categories')->where('id', $cat_id)->orderby('id', 'asc')->first();
        $option = DB::table('hotel_options')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
        $photo = DB::table('hotel_photos')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
        $ticket = DB::table('hotel_tickets')->where('ticket_id', $ticket_id)->orderby('id', 'asc')->first();
        return View('admin/hotel/tickets/refundview', compact('ticket', 'airs', 'balconies', 'beds', 'desks', 'safes', 'tvs', 'wardrobes', 'category', 'option', 'photo'));
    }
    public function gettransactionitem($cat_id = 0, $ticket_id = 0)
    {
        $airs = DB::table('hotel_airs')->orderby('id', 'asc')->get();
        $balconies = DB::table('hotel_balconies')->orderby('id', 'asc')->get();
        $beds = DB::table('hotel_beds')->orderby('id', 'asc')->get();
        $desks = DB::table('hotel_desks')->orderby('id', 'asc')->get();
        $safes = DB::table('hotel_safes')->orderby('id', 'asc')->get();
        $tvs = DB::table('hotel_tvs')->orderby('id', 'asc')->get();
        $wardrobes = DB::table('hotel_wardrobes')->orderby('id', 'asc')->get();
        $category = DB::table('hotel_categories')->where('id', $cat_id)->orderby('id', 'asc')->first();
        $option = DB::table('hotel_options')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
        $photo = DB::table('hotel_photos')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
        $ticket = DB::table('hotel_tickets')->where('ticket_id', $ticket_id)->orderby('id', 'asc')->first();
        return View('admin/hotel/tickets/transactionview', compact('ticket', 'airs', 'balconies', 'beds', 'desks', 'safes', 'tvs', 'wardrobes', 'category', 'option', 'photo'));
    }
    public function edit($cat_id = 0)
    {
        $airs = DB::table('hotel_airs')->orderby('id', 'asc')->get();
        $balconies = DB::table('hotel_balconies')->orderby('id', 'asc')->get();
        $beds = DB::table('hotel_beds')->orderby('id', 'asc')->get();
        $desks = DB::table('hotel_desks')->orderby('id', 'asc')->get();
        $safes = DB::table('hotel_safes')->orderby('id', 'asc')->get();
        $tvs = DB::table('hotel_tvs')->orderby('id', 'asc')->get();
        $wardrobes = DB::table('hotel_wardrobes')->orderby('id', 'asc')->get();
        $category = DB::table('hotel_categories')->where('id', $cat_id)->orderby('id', 'asc')->first();
        $option = DB::table('hotel_options')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
        $photo = DB::table('hotel_photos')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
        return View('admin/hotel/category/edit', compact('airs', 'balconies', 'beds', 'desks', 'safes', 'tvs', 'wardrobes', 'category', 'option', 'photo'));
    }

	public function editableDatatableData()
    {
        $tables = Category::get(['id','name', 'title', 'desc', 'rooms', 'price', 'stars', 'created_at']);

        return Datatables::of($tables)
            ->edit_column('desc', function($data) {
                $desc = $data->desc;
                return '<span style="overflow: hidden;  white-space:nowrap;  text-overflow:ellipsis;  width:250px; display:inline-block;">'.$desc.'</span>';
            })
            ->edit_column('created_at', function($data) {
                $dt = date('M d, Y H:i:s', strtotime($data->created_at));
                return $dt;
            })
            ->add_column('edit', '<a class="view" href="javascript:;">View</a> &nbsp;|&nbsp; <a class="edit" href="javascript:;">Edit</a> &nbsp;|&nbsp; <a class="delete" href="javascript:;">Delete</a>')
            ->make(true);
    }
    public function customersData()
    {
        $tables = Customer::get(['id','name', 'surname', 'phone', 'email']);

        return Datatables::of($tables)

            ->edit_column('created_at', function($data) {
                $dt = date('M d, Y H:i:s', strtotime($data->created_at));
                return $dt;
            })
            ->make(true);
    }
    public function getStatusData()
    {
        $categories = DB::table('hotel_cat_reservations as r')
            ->join('hotel_categories as c', 'r.cat_id', '=', 'c.id')
            ->select(['r.cat_id', 'r.book_num', 'r.remain_num', 'c.name', 'c.rooms', 'c.price'])->orderby('r.cat_id', 'asc')->get();
        $categories = array_map(function($object){
            return (array) $object;
        }, $categories);
        return Datatables::of($categories) ->make(true);
    }
	
    public function editableDatatableStore(Request $request)
    {
        if($request->ajax()) {
            Category::create($request->except('_token'));
        }
    }

    public function editableDatatableUpdate(Request $request, $id)
    {
        $table = Category::find($id);
        $table->update($request->except('_token', 'Plants'));
        return $table->id;
    }

    public function deleteCategory($id)
    {
        DB::table('hotel_cat_reservations')->where('cat_id', $id)->delete();
        DB::table('hotel_tickets')->where('cat_id', $id)->delete();
        $row=Category::find($id);
        $row->delete();
        return $row->id;
    }

    public function addCategory(Request $request)
    {
        $airs = DB::table('hotel_airs')->orderby('id', 'asc')->get();
        $balconies = DB::table('hotel_balconies')->orderby('id', 'asc')->get();
        $beds = DB::table('hotel_beds')->orderby('id', 'asc')->get();
        $desks = DB::table('hotel_desks')->orderby('id', 'asc')->get();
        $safes = DB::table('hotel_safes')->orderby('id', 'asc')->get();
        $tvs = DB::table('hotel_tvs')->orderby('id', 'asc')->get();
        $wardrobes = DB::table('hotel_wardrobes')->orderby('id', 'asc')->get();
        return View('admin/hotel/category/add', compact('airs', 'balconies', 'beds', 'desks', 'safes', 'tvs', 'wardrobes'));
    }
    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'title' => 'required',
            'desc' => 'required',
            'frname' => 'required',
            'frtitle' => 'required',
            'frdesc' => 'required',
            'price' => 'required',
            'stars' => 'required',
            'opt_bed' => 'required',
            'photo' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Ooops.. something went wrong
            $error = 'You missing to input some fields.';
            return Redirect::back()->withError($error);
        }
        $name = $request->get('name', '');
        $title = $request->get('title', '');
        $desc = $request->get('desc', '');
        $frname = $request->get('frname', '');
        $frtitle = $request->get('frtitle', '');
        $frdesc = $request->get('frdesc', '');
        $price = $request->get('price', '0');
        $rooms = $request->get('rooms', '0');
        $stars = $request->get('stars', '0');
        $opt_calendar = $request->get('opt_calendar', '0');
        $opt_wifi = $request->get('opt_wifi', '0');
        $opt_service = $request->get('opt_service', '0');
        $opt_minibar = $request->get('opt_minibar', '0');
        $opt_balconies = $request->get('opt_balconies', '0');
        $opt_bed = $request->get('opt_bed', '0');
        $opt_desk = $request->get('opt_desk', '0');
        $opt_safe = $request->get('opt_safe', '0');
        $opt_tv = $request->get('opt_tv', '0');
        $opt_wardrobe = $request->get('opt_wardrobe', '0');
        $opt_air = $request->get('opt_air', '0');
        $opt_table = $request->get('opt_table', '0');
        $opt_hair = $request->get('opt_hair', '0');
        $category = new Category();
        $category->name = $name;
        $category->title = $title;
        $category->desc = $desc;
        $category->frname = $frname;
        $category->frtitle = $frtitle;
        $category->frdesc = $frdesc;
        $category->price = $price;
        $category->rooms = $rooms;
        $category->stars = $stars;
        $category->save();
        $cat_reservation = DB::table('hotel_cat_reservations')->where('cat_id', $category->id)->first();
        if(empty($cat_reservation)){
            DB::table('hotel_cat_reservations')->insert(['cat_id'=>$category->id, 'book_num'=>0, 'remain_num'=>$rooms]);
        }else{
            $remain_num = $rooms - $cat_reservation->book_num;
            DB::table('hotel_cat_reservations')->where('cat_id', $category->id)->update(['remain_num'=>$remain_num]);
        }
        if (isset($_FILES['photo']['tmp_name'])) {
            // Number of uploaded files
            $destinationPath = public_path() . '/uploads/files/';
            // check if there is a file in the array

            $ext = substr(strrchr($_FILES['photo']['name'], "."), 1);
            $photoname = time() . '.' . $ext;
            // copy the file to the specified dir
            if (move_uploaded_file($_FILES['photo']['tmp_name'], $destinationPath . $photoname)) {
                /*** give praise and thanks to the php gods ***/
                DB::table('hotel_photos')->insert(['cat_id' => $category->id, 'photo' => $photoname]);
            }
        }
        DB::table('hotel_options')->insert(['cat_id'=>$category->id, 'opt_calendar'=>$opt_calendar, 'opt_wifi'=>$opt_wifi, 'opt_service'=>$opt_service, 'opt_minibar'=>$opt_minibar, 'opt_balconies'=>$opt_balconies, 'opt_bed'=>$opt_bed, 'opt_desk'=>$opt_desk, 'opt_safe'=>$opt_safe, 'opt_tv'=>$opt_tv, 'opt_wardrobe'=>$opt_wardrobe, 'opt_air'=>$opt_air, 'opt_table'=>$opt_table, 'opt_hair'=>$opt_hair]);
        $success = "Successfully added.";
        return Redirect::back()->withSuccess($success);
    }
    public function updateCategory(Request $request)
    {
        $rules = array(
            'cat_id' => 'required',
            'name' => 'required',
            'title' => 'required',
            'desc' => 'required',
            'frname' => 'required',
            'frtitle' => 'required',
            'frdesc' => 'required',
            'price' => 'required',
            'stars' => 'required',
            'opt_bed' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Ooops.. something went wrong
            $error = 'You missing to input some fields.';
            return Redirect::back()->withError($error);
        }
        $cat_id = $request->get('cat_id', '');
        $name = $request->get('name', '');
        $title = $request->get('title', '');
        $desc = $request->get('desc', '');
        $frname = $request->get('frname', '');
        $frtitle = $request->get('frtitle', '');
        $frdesc = $request->get('frdesc', '');
        $price = $request->get('price', '0');
        $rooms = $request->get('rooms', '0');
        $stars = $request->get('stars', '0');
        $opt_calendar = $request->get('opt_calendar', '0');
        $opt_wifi = $request->get('opt_wifi', '0');
        $opt_service = $request->get('opt_service', '0');
        $opt_minibar = $request->get('opt_minibar', '0');
        $opt_balconies = $request->get('opt_balconies', '0');
        $opt_bed = $request->get('opt_bed', '0');
        $opt_desk = $request->get('opt_desk', '0');
        $opt_safe = $request->get('opt_safe', '0');
        $opt_tv = $request->get('opt_tv', '0');
        $opt_wardrobe = $request->get('opt_wardrobe', '0');
        $opt_air = $request->get('opt_air', '0');
        $opt_table = $request->get('opt_table', '0');
        $opt_hair = $request->get('opt_hair', '0');
        $category = Category::find($cat_id);
        $category->name = $name;
        $category->title = $title;
        $category->desc = $desc;
        $category->frname = $frname;
        $category->frtitle = $frtitle;
        $category->frdesc = $frdesc;
        $category->price = $price;
        $category->stars = $stars;
        $category->save();

        $cat_reservation = DB::table('hotel_cat_reservations')->where('cat_id', $category->id)->first();
        if(empty($cat_reservation)){
            DB::table('hotel_cat_reservations')->insert(['cat_id'=>$category->id, 'book_num'=>0, 'remain_num'=>$rooms]);
        }else{
            $remain_num = $rooms - $cat_reservation->book_num;
            DB::table('hotel_cat_reservations')->where('cat_id', $category->id)->update(['remain_num'=>$remain_num]);
        }
        if (isset($_FILES['photo']['tmp_name'])) {
            // Number of uploaded files
            $destinationPath = public_path() . '/uploads/files/';
            // check if there is a file in the array

            $ext = substr(strrchr($_FILES['photo']['name'], "."), 1);
            $photoname = time() . '.' . $ext;
            // copy the file to the specified dir
            if (move_uploaded_file($_FILES['photo']['tmp_name'], $destinationPath . $photoname)) {
                /*** give praise and thanks to the php gods ***/
                DB::table('hotel_photos')->where('cat_id', $category->id)->update(['photo' => $photoname]);
            }
        }
        DB::table('hotel_options')->where('cat_id', $category->id)->update(['opt_calendar'=>$opt_calendar, 'opt_wifi'=>$opt_wifi, 'opt_service'=>$opt_service, 'opt_minibar'=>$opt_minibar, 'opt_balconies'=>$opt_balconies, 'opt_bed'=>$opt_bed, 'opt_desk'=>$opt_desk, 'opt_safe'=>$opt_safe, 'opt_tv'=>$opt_tv, 'opt_wardrobe'=>$opt_wardrobe, 'opt_air'=>$opt_air, 'opt_table'=>$opt_table, 'opt_hair'=>$opt_hair]);
        $success = "Successfully added.";
        return Redirect::back()->withSuccess($success);
    }
}
