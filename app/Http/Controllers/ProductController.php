<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use App\Product;
use DB;
use Validator;
use Sentinel;
use URL;
use Illuminate\Support\Facades\Redirect;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.occasion.occasion');
    }

    public function getlist($category_id = 0)
    {
        //$products = Product::orderBy('id', 'asc')->get();
        $products = Product::where('category_id', $category_id)->orderby('id', 'asc')->get();
        $category = DB::table('occ_categories')->where('id', $category_id)->first();
        return View('admin.product.list', compact('products', 'category'));
    }

    public function getProducts($category_id = 0)
    {
        //$products = Product::orderBy('id', 'asc')->get();
        if($_SESSION['userrole'] == 1){
            $products = Product::where('category_id', $category_id)->orderby('id', 'asc')->get();
        }else{
            $user_id = Sentinel::getUser()->id;
            $products = Product::where('category_id', $category_id)->where('admin_id', $user_id)->orderby('id', 'asc')->get();
        }

        $i = 0;
        return Datatables::of($products)
            ->editColumn('created_at', function ($data) {
                return date('M d, Y', strtotime($data->created_at));
            })
            ->addColumn('totalprice', function ($data) {
                $price = 0;
                if ($data->price != 0) $price = $data->price;
                $amount = 0;
                if ($data->amount != 0) $amount = $data->amount;
                $total = round($price * $amount, 2);
                return $total;
            })
            ->edit_column('name', function ($data) {

                return '<a href="/admin/product/view/' . $data->id . '">' . $data->name . '</a>';
            })
            ->edit_column('description', function ($data) {

                return '<a href="/admin/product/view/' . $data->id . '">' . $data->description . '</a>';
            })
            ->edit_column('vendor', function ($data) {
                $vendor_id = $data->admin_id;
                $vendor = DB::table('users')->where('id', $vendor_id)->first();
                $vendorname = '';
                if (!empty($vendor)) {
                    $vendorname = $vendor->first_name . ' ' . $vendor->last_name;
                }
                return $vendorname;
            })
            ->add_column('edit', function ($data) {

                return '<a class="edit" href="/admin/product/edit/' . $data->id . '">Edit</a>';
            })
            ->add_column('delete', '<a class="delete" href="javascript:;">Delete</a>')
            ->make(true);
    }


    public function create($cat_id = 0)
    {
        $category = DB::table('occ_categories')->where('id', $cat_id)->first();
        $vendors = DB::table('role_users as ru')
            ->join('users as u', 'u.id', '=', 'ru.user_id')
            ->select('u.*')->where('ru.role_id', 2)->orderby('u.created_at', 'desc')->get();
        $occasions = DB::table('occ_occasions')->orderby('id', 'asc')->get();
        return View('admin.product.add', compact('category', 'vendors', 'occasions'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'cat_id' => 'required',
            'name' => 'required',
            'price' => 'required',
            'amount' => 'required',
            //'vendor' => 'required',
            'occasion' => 'required',
            'photo' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Ooops.. something went wrong
            $error = 'You missing to input some fields.';
            return Redirect::back()->withError($error);
        }
        $cat_id = $request->get('cat_id', '');
        $name = $request->get('name', '');
        $description = $request->get('description', '');
        $price = $request->get('price', 0);
        $amount = $request->get('amount', 0);
        $vendor = $request->get('vendor', 0);
        $occasion = $request->get('occasion', 0);
        $product = new Product();
        $product->category_id = $cat_id;
        $product->name = $name;
        $product->description = $description;
        $product->price = $price;
        $product->amount = $amount;
        $product->p_contact_id = $vendor;
        $product->s_contact_id = $vendor;
        $product->admin_id = $vendor;
        $product->occ_id = $occasion;
        $product->save();

        $filenames = '';
        if (isset($_FILES['photo']['tmp_name'])) {
            // Number of uploaded files
            $num_files = count($_FILES['photo']['tmp_name']);
            $destinationPath = public_path() . '/uploads/files/';
            /** loop through the array of files ***/
            $k = 0;
            for ($i = 0; $i < $num_files; $i++) {
                // check if there is a file in the array
                echo $i;
                $ext = substr(strrchr($_FILES['photo']['name'][$i], "."), 1);
                $photoname = time() . $i . '.' . $ext;
                // copy the file to the specified dir
                if (move_uploaded_file($_FILES['photo']['tmp_name'][$i], $destinationPath . $photoname)) {
                    /*** give praise and thanks to the php gods ***/
                    DB::table('occ_product_photos')->insert(['product_id' => $product->id, 'photo' => $photoname]);
                }
            }
        }
        $success = "Successfully added.";
        return Redirect::back()->withSuccess($success);
    }

    public function edit($id = 0)
    {
        $product = DB::table('occ_products')->where('id', $id)->first();
        $photos = DB::table('occ_product_photos')->where('product_id', $id)->get();
        $category = DB::table('occ_categories')->where('id', $product->category_id)->first();
        $vendors = DB::table('role_users as ru')
            ->join('users as u', 'u.id', '=', 'ru.user_id')
            ->select('u.*')->where('ru.role_id', 2)->orderby('u.created_at', 'desc')->get();
        $occasions = DB::table('occ_occasions')->orderby('id', 'asc')->get();

        return View('admin.product.edit', compact('product', 'photos', 'category', 'vendors', 'occasions'));
    }

    public function photoDelete($photo_id = 0)
    {
        DB::table('occ_product_photos')->where('id', $photo_id)->delete();
        return 1;
    }

    public function update(Request $request)
    {
        $rules = array(
            'cat_id' => 'required',
            'product_id' => 'required',
            'name' => 'required',
            'price' => 'required',
            'amount' => 'required',
          //  'vendor' => 'required',
            'occasion' => 'required',
            'photo' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Ooops.. something went wrong
            $error = 'You missing to input some fields.';
            return Redirect::back()->withError($error);
        }
        $cat_id = $request->get('cat_id', '');
        $product_id = $request->get('product_id', '');
        $name = $request->get('name', '');
        $description = $request->get('description', '');
        $price = $request->get('price', 0);
        $amount = $request->get('amount', 0);
        $vendor = $request->get('vendor', 0);
        $occasion = $request->get('occasion', 0);
        $product = Product::find($product_id);
        $product->category_id = $cat_id;
        $product->name = $name;
        $product->description = $description;
        $product->price = $price;
        $product->amount = $amount;
        $product->p_contact_id = $vendor;
        $product->s_contact_id = $vendor;
        $product->admin_id = $vendor;
        $product->occ_id = $occasion;
        $product->save();
        $filenames = '';
        if (isset($_FILES['photo']['tmp_name'])) {
            // Number of uploaded files
            $num_files = count($_FILES['photo']['tmp_name']);
            $destinationPath = public_path() . '/uploads/files/';
            /** loop through the array of files ***/
            $k = 0;
            for ($i = 0; $i < $num_files; $i++) {
                // check if there is a file in the array
                echo $i;
                $ext = substr(strrchr($_FILES['photo']['name'][$i], "."), 1);
                $photoname = time() . $i . '.' . $ext;
                // copy the file to the specified dir
                if (move_uploaded_file($_FILES['photo']['tmp_name'][$i], $destinationPath . $photoname)) {
                    /*** give praise and thanks to the php gods ***/
                    DB::table('occ_product_photos')->insert(['product_id' => $product->id, 'photo' => $photoname]);
                }
            }
        }
        $success = "Successfully added.";
        return Redirect::back()->withSuccess($success);
    }

    public function view($id = 0)
    {
        $product = DB::table('occ_products')->where('id', $id)->first();
        $photos = DB::table('occ_product_photos')->where('product_id', $id)->get();
        $category = DB::table('occ_categories')->where('id', $product->category_id)->first();
        $vendors = DB::table('role_users as ru')
            ->join('users as u', 'u.id', '=', 'ru.user_id')
            ->select('u.*')->where('ru.role_id', 2)->orderby('u.created_at', 'desc')->get();
        $occasions = DB::table('occ_occasions')->orderby('id', 'asc')->get();

        return View('admin.product.view', compact('product', 'photos', 'category', 'vendors', 'occasions'));
    }

    public function delete($id)
    {
        $row = Product::find($id);
        $row->delete();
        return $row->id;
    }

    public function editableDatatableUpdate(Request $request, $id)
    {
        $table = Product::find($id);
        $table->update($request->except('_token', 'Plants'));
        return $table->id;
    }

    public function editableDatatableData()
    {
        $tables = Product::where('category_id', '=', 2)->orderby('id', 'asc')->get();

        return Datatables::of($tables)
            ->add_column('edit', '<a class="edit" href="javascript:;">Edit</a>')
            ->add_column('delete', '<a class="delete" href="javascript:;">Delete</a>')
            ->make(true);
    }

}
