<?php

namespace App\Http\Controllers;

use App\Occasion;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use App\User;
use DB;
class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
    }
    public function getlist()
    {
        $vendors = DB::table('role_users as ru')
            ->join('users as u','u.id','=','ru.user_id')
            ->select('u.*')->where('ru.role_id', 2)->orderby('u.created_at', 'desc')->get();
        return View('admin/vendor/vendor', compact('vendors'));
    }
    public function getVendor($id = 0)
    {
        //$vendor = DB::table('users')->where('id', $id)->first();
        $vendor = User::find($id);
        return json_encode($vendor);
    }
    public function inactiveVendor($id = 0, $status)
    {
        if($status == 1)
            $status = 0;
        else
            $status = 1;
        DB::table('users')->where('id', $id)->update(['status'=>$status]);
        $data = array("status"=>$status);
        return $data;
    }
}

