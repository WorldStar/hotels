<?php

namespace App\Http\Controllers;

use  App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Datatables;
use Validator;
use URL;
use Redirect;
use App\Ticket;
use Sentinel;
class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.order.');
    }


/* paid ticket */
    public function getpaidlist()
    {
        $paidtickets = Ticket::where('state', 2)->orderby('id', 'asc')->get();
        return View('admin/ticket/paidlist', compact('paidtickets'));
    }
    public function getpaidTicket()
    {
       if ($_SESSION['userrole'] == 2) {
           $user_id = Sentinel::getUser()->id;
           $tickets = DB::table('occ_tickets as t')
               ->join('occ_products as p', 'p.id','=','t.product_id')
               ->select('t.*')->where('p.admin_id','=',$user_id)->where('t.state', 2)->get('t.created_at','desc');
           $tickets = collect($tickets);
       } else {
           $tickets = Ticket::where('state', 2)->orderby('id', 'asc')->get();
       }

        return Datatables::of($tickets)
            ->edit_column('product_id', function($data) {
                $product = DB::table('occ_products')->where('id',$data->product_id)->first();
                $productname = '';
                if (!empty($product)) {
                    $productname = $product->name;
                }
                return '<a href="/admin/ticket/detail/'.$data->id.'">'.$productname.'</a>';
            })
            ->edit_column('customer_id', function($data) {
                $id = $data->customer_id;
                $user = DB::table('users')->where('id',$id)->first();
                $customername = $user->first_name.' '.$user->last_name;
                return '<a href="/admin/ticket/detail/'.$data->id.'">'.$customername.'</a>';
            })
            ->edit_column('ship_id', function($data) {
                $shipper = DB::table('occ_shipper_contacts')->where('id', $data->ship_id)->first();
                $shippername = "";
                if (!empty($shipper)) {
                    $shippername = $shipper->name;
                }
                //'shippername'== !empty( $shipper->name ) ? $shippername->name : 'not_sure';
                return '<a href="/admin/ticket/detail/'.$data->id.'">'.$shippername.'</a>';
            })
            ->editColumn('created_at', function($data){
                return date('M d, Y', strtotime($data->created_at));
            })
            ->add_column('state', function($data) {
                return '<a href="javascript:cancelModal('.$data->id.')">Cancel</a>';
            })
            ->make(true);
    }


    public function update(Request $request)
    {
        $id = $request->get('id', '');
        $state = $request->get('state', '0');
        DB::table('occ_tickets')->where('id', $id)->update(['state' =>$state]);
        return $id;
    }
    public function detail($id=0)
    {
        $ticket = Ticket::find($id);
        $product=DB::table('occ_products')->where('id', $ticket->product_id )->first();
        $user = DB::table('users')->where('id', $ticket->customer_id)->first();
        $shipper = DB::table('occ_shipper_contacts')->where('id', $ticket->ship_id)->first();
        return View(('admin/ticket/detail'), compact('product','ticket',  'user', 'shipper'));
    }


/* cancelled ticket  */
    public function getcancelledlist()
    {
        $cancelledtickets = Ticket::where('state', 0)->orderby('id', 'asc')->get();
        return View('admin/ticket/cancelledlist', compact('cancelledtickets'));
    }
    public function getcancelledTicket()
    {
        $tickets = Ticket::where('state', 0)->orderby('id', 'asc')->get();
        return Datatables::of($tickets)
            ->edit_column('product_id', function($data) {
                $product = DB::table('occ_products')->where('id',$data->product_id)->first();
                $productname = '';
                if (!empty($product)) {
                    $productname = $product->name;
                }
                return '<a href="/admin/ticket/detail/'.$data->id.'">'.$productname.'</a>';
            })
            ->edit_column('customer_id', function($data) {
                $user = DB::table('users')->where('id', 1)->first();
                $customername =  $user->first_name.' '.$user->last_name;
                return '<a href="/admin/ticket/detail/'.$data->id.'">'.$customername.'</a>';
            })
            ->editColumn('created_at', function($data){
                return date('M d, Y', strtotime($data->created_at));
            })
            ->make(true);
    }


/*ship ticket  */
    public function getshiplist()
    {
        $shiptickets = Ticket::where('state', 1)->orderby('id', 'asc')->get();
        return View('admin/ticket/shiplist', compact('shiptickets'));
    }
    public function getshipTicket()
    {
        if ($_SESSION['userrole'] == 2) {
            $login_id = Sentinel::getUser()->id;
            $tickets = DB::table('occ_products as p')
                ->join('occ_tickets as t', 't.product_id', '=', 'p.id')
                ->select('t.*')->where('p.admin_id', '=', $login_id)->where('t.state', 1)->orderby('t.created_at', 'desc')->get();
            $tickets = collect($tickets);
        } else {
            $tickets = Ticket::where('state',1)->orderby('created_at', 'desc')->get();
        }
        return Datatables::of($tickets)
            ->edit_column('product_id', function($data) {
                $product = DB::table('occ_products')->where('id',$data->product_id)->first();
                $productname = '';
                if (!empty($product)) {
                    $productname = $product->name;
                }
                return '<a href="/admin/ticket/detail/'.$data->id.'">'.$productname.'</a>';
            })
            ->edit_column('customer_id', function($data) {
                $id = $data->customer_id;
                $user = DB::table('users')->where('id',$id)->first();
                $customername = $user->first_name.' '.$user->last_name;
                return '<a href="/admin/ticket/detail/'.$data->id.'">'.$customername.'</a>';
            })
            ->edit_column('ship_id', function($data) {
                $shipper = DB::table('occ_shipper_contacts')->where('id', $data->ship_id)->first();
                $shippername = "";
                if (!empty($shipper)) {
                    $shippername = $shipper->name;
                }
                //'shippername'== !empty( $shipper->name ) ? $shippername->name : 'not_sure';
                return '<a href="/admin/ticket/detail/' . $data->id . '">' . $shippername . '</a>';
            })
            ->editColumn('created_at', function($data){
                return date('M d, Y', strtotime($data->created_at));
            })
            ->add_column('cancel', function($data) {
                return '<a href="javascript:cancelModal('.$data->id.')">Cancel</a>';
            })
            ->make(true);
    }
}
