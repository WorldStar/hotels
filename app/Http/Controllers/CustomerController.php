<?php

namespace App\Http\Controllers;

use App\Occasion;
use App\Http\Requests;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Datatables;
use App\User;
use DB;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
    }
    public function getlist()
    {
        if($_SESSION['userrole'] == 1) {
            $customers = DB::table('role_users as ru')
                ->join('users as u', 'u.id', '=', 'ru.user_id')
                ->select('u.*')->where('ru.role_id', 3)->orderby('u.created_at', 'desc')->get();
        } else {
            $customers = DB::table('role_users as ru')
                ->join('users as u', 'u.id', '=', 'ru.user_id')
                ->select('u.*')->where('ru.role_id', 3)->orderby('u.created_at', 'desc')->get();
//            $vendor_id = Sentinel::getUser()->id;
//            $customers = DB::table('occ_products as p')
//                ->join('occ_gifts as g', 'g.product_id','=','p.admin_id')
//                ->join('users as u', 'u.id', '=', 'g.customer_id')
//                ->select('u.*')->where('p.admin_id', $vendor_id)->orderby('u.created_at', 'desc')->get();
//            $orders = DB::table('occ_products as p')
//                ->join('occ_gifts as g', 'g.product_id','=','p.admin_id')
//                ->select('g.*')->where('p.admin_id', $vendor_id)->where('state','<>',0)->orderby('g.id', 'desc')->get();
//            $customers = array();
//            foreach($orders as $order){
//                $customer = DB::table('users')->where('id','=',$order->customer_id)->first();
//                $customers = add($customer);
//            }
//            $customers = array_unique($customers);
//
        }
        return View('admin/customer/customer', compact('customers','orders'));
    }
    public function getCustomer($id = 0)
    {
        //$customer = DB::table('users')->where('id', $id)->first();
        $customer = User::find($id);
        return json_encode($customer);
    }
    public function inactiveCustomer($id = 0, $status)
    {
        if($status == 1)
            $status = 0;
        else
            $status = 1;
        DB::table('users')->where('id', $id)->update(['status'=>$status]);
        $data = array("status"=>$status);
        return $data;
    }
}

