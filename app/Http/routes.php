<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Model binding into route
 */
Route::model('blogcategory', 'App\BlogCategory');
Route::model('blog', 'App\Blog');
Route::model('file', 'App\File');
Route::model('task', 'App\Task');
Route::model('users', 'App\User');

Route::pattern('slug', '[a-z0-9- _]+');

Route::group(array('prefix' => 'admin'), function () {

	# Error pages should be shown without requiring login
	Route::get('404', function () {
		return View('admin/404');
	});
	Route::get('500', function () {
		return View::make('admin/500');
	});

    Route::post('secureImage', array('as' => 'secureImage','uses' => 'JoshController@secureImage'));

	# Lock screen
	Route::get('{id}/lockscreen', array('as' => 'lockscreen', 'uses' =>'UsersController@lockscreen'));

	# All basic routes defined here
	Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
	Route::post('signin', 'AuthController@postSignin');
	Route::post('signup', array('as' => 'signup', 'uses' => 'AuthController@postSignup'));
	Route::post('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@postForgotPassword'));
	Route::get('login2', function () {
		return View::make('admin/login2');
	});

	# Register2
	Route::get('register2', function () {
		return View::make('admin/register2');
	});
	Route::post('register2', array('as' => 'register2', 'uses' => 'AuthController@postRegister2'));

	# Forgot Password Confirmation
	Route::get('forgot-password/{userId}/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
	Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

	# Logout
	Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

	# Account Activation
	Route::get('activate/{userId}/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));
});




Route::group(array('prefix' => 'admin', 'middleware' => 'SentinelAdmin'), function () {
    # Dashboard / Index
	Route::get('/', array('as' => 'dashboard','uses' => 'JoshController@showHome'));
    Route::get('dashboard/ticket', 'JoshController@getrecentlytickets');
    Route::get('occasion', 'OccasionController@index');
    Route::get('occasion/data', array('as' => 'admin.occasion.data', 'uses' => 'OccasionController@data'));


    /* Categories */

    Route::get('categorystatus', 'Hotels\Admin\CategoryController@getstatus');
    Route::get('getcategory', 'Hotels\Admin\CategoryController@getlist');
    Route::get('getcategory/{id}', 'Hotels\Admin\CategoryController@getitem');
    Route::get('getrefund/{cid}/{tid}', 'Hotels\Admin\CategoryController@getrefunditem');
    Route::get('gettransaction/{cid}/{tid}', 'Hotels\Admin\CategoryController@gettransactionitem');
    Route::get('category/edit/{id}', 'Hotels\Admin\CategoryController@edit');
    Route::get('customers', 'Hotels\Admin\CategoryController@getcustomers');
    Route::get('category/data', array('as' => 'admin.category.data', 'uses' => 'Hotels\Admin\CategoryController@editableDatatableData'));
    Route::get('customers/data', array('as' => 'admin.customers.data', 'uses' => 'Hotels\Admin\CategoryController@customersData'));
    Route::get('categorystatus/data', array('as' => 'admin.categorystatus.data', 'uses' => 'Hotels\Admin\CategoryController@getStatusData'));
    Route::get('category/add', array('as' => 'admin.category.add', 'uses' => 'Hotels\Admin\CategoryController@addCategory'));
    Route::post('category/store', array('as' => 'admin.category.store', 'uses' => 'Hotels\Admin\CategoryController@storeCategory'));
    Route::post('category/update', array('as' => 'admin.category.update', 'uses' => 'Hotels\Admin\CategoryController@updateCategory'));
    Route::get('category/delete/{id}', array('as' => 'admin.category.delete', 'uses' => 'Hotels\Admin\CategoryController@deleteCategory'));
    //reservations

    Route::get('reservation', 'Hotels\Admin\ReservationController@getlist');
    Route::get('passingreservation', 'Hotels\Admin\ReservationController@passinglist');
    Route::get('passedreservation', 'Hotels\Admin\ReservationController@passedlist');
    Route::get('cancelreservation', 'Hotels\Admin\ReservationController@cancellist');
    Route::get('reservation/passed/{id}', 'Hotels\Admin\ReservationController@passedTicket');
    Route::get('reservation/cancel/{id}', 'Hotels\PaypalPaymentController@setRefund');







    # editable datatables
    Route::get('editable_occasion', 'OccasionController@editableOccasionIndex');
    Route::get('editable_occasion/data', array('as' => 'admin.editable_occasion.data', 'uses' => 'OccasionController@editableOccasionData'));
    Route::post('editable_occasion/create','OccasionController@editableOccasionStore');
    Route::post('editable_occasion/{id}/update', 'OccasionController@editableOccasionUpdate');
    Route::get('editable_occasion/{id}/delete', array('as' => 'admin.editable_occasion.delete', 'uses' => 'OccasionController@editableOccasionDestroy'));


/* Category  */

    #category

/* Product  */

    Route::get('products/{catid}', 'ProductController@getlist');
    Route::any('products/get/{catid}', 'ProductController@getProducts');
    Route::get('product/data', array('as' => 'admin.product.data', 'uses' => 'ProductController@editableDatatableData'));
    Route::post('product/{id}/update', 'ProductController@editableDatatableUpdate');
    Route::get('product/{id}/delete', array('as' => 'admin.product.delete', 'uses' => 'ProductController@delete'));
    Route::get('product/{cat_id}/add', array('as' => 'admin.product.add', 'uses' => 'ProductController@create'));
    Route::post('product/store', array('as' => 'admin.product.store', 'uses' => 'ProductController@store'));
    Route::any('product/edit/{id}', array('as' => 'admin.product.edit', 'uses' => 'ProductController@edit'));
    Route::get('product/photo/delete/{photo_id}', array('as' => 'admin.product.photo.delete', 'uses' => 'ProductController@photoDelete'));
    Route::post('product/update', array('as' => 'admin.product.store', 'uses' => 'ProductController@update'));
    Route::get('product/view/{id}', array('as' => 'admin.product.view', 'uses' => 'ProductController@view'));


/* Occasion  */

    # editable occasions
    Route::get('occasions', 'OccasionController@getlist');
    Route::get('occasions/data', array('as' => 'admin.occasions.data', 'uses' => 'OccasionController@editableOccasionData'));
    Route::post('occasions/addoccasion', array('as' => 'admin.occasions.add', 'uses' => 'OccasionController@addoccasion'));
    Route::post('occasions/{id}/update', 'OccasionController@editableOccasionUpdate');
    Route::get('occasions/{id}/delete', array('as' => 'admin.occasions.delete', 'uses' => 'OccasionController@editableOccasionDestroy'));

    Route::get('  A/data', array('as' => 'User.    .data', 'uses' => 'OccasionController@getData'));


 /* Vendor */

    Route::get('vendor', 'VendorController@getlist');
    Route::get('vendor/get/{id}', 'VendorController@getVendor');
    Route::get('vendor/inactive/{id}/{status}', 'VendorController@inactiveVendor');


/* Customer */

    Route::get('customer', 'CustomerController@getlist');
    Route::get('customer/get/{id}', 'CustomerController@getCustomer');
    Route::get('customer/inactive/{id}/{status}', 'CustomerController@inactiveCustomer');


/* Order  */

    Route::get('orders/{catid}', 'OrderController@getlist');
    Route::any('orders/get/{catid}', 'OrderController@getOrders');
    Route::get('order/{id}/delete', array('as' => 'admin.order.delete', 'uses' => 'OrderController@delete'));
    Route::get('order/detail/{id}', array('as' => 'admin.order.detail', 'uses' => 'OrderController@detail'));
    Route::get('order/stateupdate', array('as' => 'admin.order.stateupdate', 'uses' => 'OrderController@stateUpdate'));
    Route::get('order/cancel/{id}/{status}', 'OrderController@cancelOrder');


/* ticket */

    Route::get('ticket/paid', 'TicketController@getpaidlist');
    Route::any('ticket/getpaid', 'TicketController@getpaidTicket');
    Route::get('ticket/update', array('as' => 'admin.ticket.update', 'uses' => 'TicketController@update'));
    Route::get('ticket/detail/{id}', array('as' => 'admin.ticket.detail', 'uses' => 'TicketController@detail'));
    Route::get('order/cancel/{id}/{status}', 'OrderController@cancelOrder');
    Route::get('ticket/cancelled', 'TicketController@getcancelledlist');
    Route::any('ticket/getcancelled', 'TicketController@getcancelledTicket');
    Route::get('ticket/ship', 'TicketController@getshiplist');
    Route::any('ticket/getship', 'TicketController@getshipTicket');


    # User Management
    Route::group(array('prefix' => 'users'), function () {
        Route::get('/', array('as' => 'users', 'uses' => 'UsersController@index'));
        Route::get('data',['as' => 'users.data', 'uses' =>'UsersController@data']);
        Route::get('create', 'UsersController@create');
        Route::post('create', 'UsersController@store');
        Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'UsersController@destroy'));
        Route::get('{userId}/confirm-delete', array('as' => 'confirm-delete/user', 'uses' => 'UsersController@getModalDelete'));
        Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'UsersController@getRestore'));
        Route::get('{userId}', array('as' => 'users.show', 'uses' => 'UsersController@show'));
        Route::post('{userId}/passwordreset', array('as' => 'passwordreset', 'uses' => 'UsersController@passwordreset'));
    });

    Route::resource('users', 'UsersController');

	Route::get('deleted_users',array('as' => 'deleted_users','before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'));

	# Group Management
    Route::group(array('prefix' => 'groups'), function () {
        Route::get('/', array('as' => 'groups', 'uses' => 'GroupsController@index'));
        Route::get('create', array('as' => 'create/group', 'uses' => 'GroupsController@create'));
        Route::post('create', 'GroupsController@store');
        Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'GroupsController@edit'));
        Route::post('{groupId}/edit', 'GroupsController@update');
        Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'GroupsController@destroy'));
        Route::get('{groupId}/confirm-delete', array('as' => 'confirm-delete/group', 'uses' => 'GroupsController@getModalDelete'));
        Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'GroupsController@getRestore'));
    });

    /*routes for blog*/
	Route::group(array('prefix' => 'blog'), function () {
        Route::get('/', array('as' => 'blogs', 'uses' => 'BlogController@index'));
        Route::get('create', array('as' => 'create/blog', 'uses' => 'BlogController@create'));
        Route::post('create', 'BlogController@store');
        Route::get('{blog}/edit', array('as' => 'update/blog', 'uses' => 'BlogController@edit'));
        Route::post('{blog}/edit', 'BlogController@update');
        Route::get('{blog}/delete', array('as' => 'delete/blog', 'uses' => 'BlogController@destroy'));
		Route::get('{blog}/confirm-delete', array('as' => 'confirm-delete/blog', 'uses' => 'BlogController@getModalDelete'));
		Route::get('{blog}/restore', array('as' => 'restore/blog', 'uses' => 'BlogController@getRestore'));
        Route::get('{blog}/show', array('as' => 'blog/show', 'uses' => 'BlogController@show'));
        Route::post('{blog}/storecomment', array('as' => 'restore/blog', 'uses' => 'BlogController@storecomment'));
	});

    /*routes for blog category*/
	Route::group(array('prefix' => 'blogcategory'), function () {
        Route::get('/', array('as' => 'blogcategories', 'uses' => 'BlogCategoryController@index'));
        Route::get('create', array('as' => 'create/blogcategory', 'uses' => 'BlogCategoryController@create'));
        Route::post('create', 'BlogCategoryController@store');
        Route::get('{blogcategory}/edit', array('as' => 'update/blogcategory', 'uses' => 'BlogCategoryController@edit'));
        Route::post('{blogcategory}/edit', 'BlogCategoryController@update');
        Route::get('{blogcategory}/delete', array('as' => 'delete/blogcategory', 'uses' => 'BlogCategoryController@destroy'));
		Route::get('{blogcategory}/confirm-delete', array('as' => 'confirm-delete/blogcategory', 'uses' => 'BlogCategoryController@getModalDelete'));
		Route::get('{blogcategory}/restore', array('as' => 'restore/blogcategory', 'uses' => 'BlogCategoryController@getRestore'));
	});

	/*routes for file*/
	Route::group(array('prefix' => 'file'), function () {
        Route::post('create', 'FileController@store');
		Route::post('createmulti', 'FileController@postFilesCreate');
		Route::delete('delete', 'FileController@delete');
	});

	Route::get('crop_demo', function () {
        return redirect('admin/imagecropping');
    });
    Route::post('crop_demo','JoshController@crop_demo');


    /* laravel example routes */
	# datatables
	Route::get('datatables', 'DataTablesController@index');
	Route::get('datatables/data', array('as' => 'admin.datatables.data', 'uses' => 'DataTablesController@data'));

   # editable datatables
    Route::get('editable_datatables', 'DataTablesController@editableDatatableIndex');
    Route::get('editable_datatables/data', array('as' => 'admin.editable_datatables.data', 'uses' => 'DataTablesController@editableDatatableData'));
    Route::post('editable_datatables/create','DataTablesController@editableDatatableStore');
    Route::post('editable_datatables/{id}/update', 'DataTablesController@editableDatatableUpdate');
    Route::get('editable_datatables/{id}/delete', array('as' => 'admin.editable_datatables.delete', 'uses' => 'DataTablesController@editableDatatableDestroy'));


	//tasks section
    Route::post('task/create', 'TaskController@store');
    Route::get('task/data', 'TaskController@data');
    Route::post('task/{task}/edit', 'TaskController@update');
    Route::post('task/{task}/delete', 'TaskController@delete');


	# Remaining pages will be called from below controller method
	# in real world scenario, you may be required to define all routes manually

	Route::get('{name?}', 'JoshController@showView');

});

//frontend

Route::get('/', function () {
    return view('index');
});
Route::get('/francais', function () {
    return view('fr.index');
});
Route::post('/reservation', array('as' => 'reservation', 'uses' =>'Hotels\ReservationController@setReservation'));
Route::get('/reservation/payment/{cat_id}/{temp_id}', array('as' => 'reservation', 'uses' =>'Hotels\ReservationController@payReservation'));
Route::get('/categorylist', array('as' => 'category.list', 'uses' =>'Hotels\CategoryController@getList'));
Route::get('/catitem/{cat_id}', array('as' => 'category.list', 'uses' =>'Hotels\CategoryController@getCatItem'));
Route::any('/pay-paypal', array('as' => 'pay-paypal','uses' => 'Hotels\PaypalPaymentController@getCheckout'));
Route::any('/getdone/{cat_id}/{temp_id}', array('as' => 'paydone','uses' => 'Hotels\PaypalPaymentController@getDone'));
Route::any('/getcancel/{cat_id}/{temp_id}', array('as' => 'paydone','uses' => 'Hotels\PaypalPaymentController@getCancel'));
Route::any('/set-creditcard', array('as' => 'set-creditcard','uses' => 'Hotels\ReservationController@setCreditcard'));
Route::any('/pay-creditcard', array('as' => 'pay-creditcard','uses' => 'Hotels\PaypalPaymentController@store'));

Route::get('/about', function () {
    return view('about');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/event', function () {
    return view('event');
});
Route::get('/gallery', function () {
    return view('gallery');
});
Route::get('/reservation_form_dark', function () {
    return view('reservation_form_dark');
});
Route::get('/blog', function () {
    return view('blog');
});
Route::any('/blogdetail', function () {
    return view('blogdetail');
});
Route::get('/catitem', function () {
    return view('catitem');
});
Route::get('/reservation_form_light', function () {
    return view('reservation_form_light');
});
Route::get('/roomsingle', function () {
    return view('roomsingle');
});
Route::get('/roomsingle', function () {
    return view('roomsingle');
});
Route::get('/categorygrid', function () {
    return view('roomsingle');
});
Route::get('/mailchimp', function () {
    return view('roomsingle');
});






Route::post('/francais/reservation', array('as' => 'reservation', 'uses' =>'Hotels\ReservationController@setFRReservation'));
Route::get('/francais/reservation/payment/{cat_id}/{temp_id}', array('as' => 'frreservation', 'uses' =>'Hotels\ReservationController@payFRReservation'));
Route::get('/francais/categorylist', array('as' => 'category.list', 'uses' =>'Hotels\CategoryController@getFRList'));
Route::get('/francais/catitem/{cat_id}', array('as' => 'category.list', 'uses' =>'Hotels\CategoryController@getFRCatItem'));
Route::any('/francais/pay-paypal', array('as' => 'fr-pay-paypal','uses' => 'Hotels\FRPaypalPaymentController@getCheckout'));
Route::any('/francais/getdone/{cat_id}/{temp_id}', array('as' => 'fr-paydone','uses' => 'Hotels\FRPaypalPaymentController@getDone'));
Route::any('/francais/getcancel/{cat_id}/{temp_id}', array('as' => 'fr-paydone','uses' => 'Hotels\FRPaypalPaymentController@getCancel'));
Route::any('/francais/set-creditcard', array('as' => 'fr-set-creditcard','uses' => 'Hotels\ReservationController@setCreditcard'));
Route::any('/francais/pay-creditcard', array('as' => 'fr-pay-creditcard','uses' => 'Hotels\FRPaypalPaymentController@store'));
Route::get('/francais/about', function () {
    return view('fr.about');
});
Route::get('/francais/contact', function () {
    return view('fr.contact');
});
Route::get('/francais/event', function () {
    return view('fr.event');
});
Route::get('/francais/gallery', function () {
    return view('fr.gallery');
});
Route::get('/francais/reservation_form_dark', function () {
    return view('fr.reservation_form_dark');
});
Route::get('/francais/blog', function () {
    return view('fr.blog');
});
Route::any('/francais/blogdetail', function () {
    return view('fr.blogdetail');
});
Route::get('/francais/catitem', function () {
    return view('fr.catitem');
});
Route::get('/francais/reservation_form_light', function () {
    return view('fr.reservation_form_light');
});
Route::get('/francais/roomsingle', function () {
    return view('fr.roomsingle');
});
Route::get('/francais/roomsingle', function () {
    return view('fr.roomsingle');
});
Route::get('/francais/categorygrid', function () {
    return view('fr.roomsingle');
});
Route::get('/francais/mailchimp', function () {
    return view('fr.roomsingle');
});






#FrontEndController
Route::get('login', array('as' => 'login','uses' => 'FrontEndController@getLogin'));
Route::post('login','FrontEndController@postLogin');
Route::get('register', array('as' => 'register','uses' => 'FrontEndController@getRegister'));
Route::post('register','FrontEndController@postRegister');
Route::get('activate/{userId}/{activationCode}',array('as' =>'activate','uses'=>'FrontEndController@getActivate'));
Route::get('forgot-password',array('as' => 'forgot-password','uses' => 'FrontEndController@getForgotPassword'));
Route::post('forgot-password','FrontEndController@postForgotPassword');

# Forgot Password Confirmation
Route::get('forgot-password/{userId}/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'FrontEndController@getForgotPasswordConfirm'));
Route::post('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@postForgotPasswordConfirm');

# My account display and update details
Route::group(array('middleware' => 'SentinelUser'), function () {
	Route::get('my-account', array('as' => 'my-account', 'uses' => 'FrontEndController@myAccount'));
    Route::put('my-account', 'FrontEndController@update');
});
Route::get('logout', array('as' => 'logout','uses' => 'FrontEndController@getLogout'));

# contact form
Route::post('contact',array('as' => 'contact','uses' => 'FrontEndController@postContact'));

#frontend views
Route::get('/index', array('as' => 'home', function () {  // change /index to /
    return View::make('index');
}));

Route::get('blog', array('as' => 'blog', 'uses' => 'BlogController@getIndexFrontend'));
Route::get('blog/{slug}/tag', 'BlogController@getBlogTagFrontend');
Route::get('blogitem/{slug?}', 'BlogController@getBlogFrontend');
Route::post('blogitem/{blog}/comment', 'BlogController@storeCommentFrontend');

Route::get('{name?}', 'JoshController@showFrontEndView');
# End of frontend views

