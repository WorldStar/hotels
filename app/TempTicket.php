<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempTicket extends Model
{
    protected $table = 'hotel_temp_ticket';

    protected $guarded  = ['id'];

}
