<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'hotel_categories';

    protected $guarded  = ['id'];

}
