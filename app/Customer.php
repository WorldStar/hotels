<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'hotel_customers';

    protected $guarded  = ['id'];

}
