<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occasion extends Model
{
    protected $table = 'occ_occasions';

    protected $guarded  = ['id'];

}
