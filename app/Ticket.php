<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'hotel_tickets';

    protected $guarded  = ['id'];

}
