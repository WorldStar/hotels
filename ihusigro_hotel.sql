-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 07, 2016 at 12:00 PM
-- Server version: 5.6.34
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ihusigro_hotel`
--
CREATE DATABASE IF NOT EXISTS `ihusi_hotels` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ihusi_hotels`;

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS `activations`;
CREATE TABLE IF NOT EXISTS `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'zuSsthG6AI9sojZbG4vdIYsm5sJ1HV7i', 1, '2016-11-07 16:13:06', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(2, 2, 'YpKW5WZPy1DLMCo41yPNrYHzNsLisNGN', 1, '2016-11-08 15:03:30', '2016-11-08 15:03:30', '2016-11-08 15:03:30');

-- --------------------------------------------------------

--
-- Table structure for table `ams_plans`
--

DROP TABLE IF EXISTS `ams_plans`;
CREATE TABLE IF NOT EXISTS `ams_plans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `road_id` int(10) unsigned NOT NULL,
  `es_rock` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rock_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `es_gravel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gravel_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `road_id` (`road_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ams_plans`
--

INSERT INTO `ams_plans` (`id`, `road_id`, `es_rock`, `rock_desc`, `es_gravel`, `gravel_desc`, `created_at`, `updated_at`) VALUES
(1, 5, 'est rock', 'rock desc', 'est gravel', 'Large gravel deposits are a common geological feature, being formed as a result of the weathering and erosion of rocks. The action of rivers and waves tends to pile up gravel in large accumulations.', '2016-11-24 10:41:09', '2016-11-24 03:53:17'),
(4, 10, 'test rock 2', 'rock 2 description', 'test gravel 2', 'gravel 2 description', '2016-11-24 03:45:49', '2016-11-24 03:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `ams_records`
--

DROP TABLE IF EXISTS `ams_records`;
CREATE TABLE IF NOT EXISTS `ams_records` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `road_street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ams_records`
--

INSERT INTO `ams_records` (`id`, `road_street`, `alt_name`, `priority`, `created_at`, `updated_at`) VALUES
(5, 'A119-3', 'Davey test', '3', '2016-11-23 13:15:29', '2016-11-24 03:57:23'),
(10, 'A11-14', 'AGNEW', '3', '2016-11-24 03:45:49', '2016-11-24 03:45:49');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_category_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

DROP TABLE IF EXISTS `blog_categories`;
CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments`
--

DROP TABLE IF EXISTS `blog_comments`;
CREATE TABLE IF NOT EXISTS `blog_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `datatables`
--

DROP TABLE IF EXISTS `datatables`;
CREATE TABLE IF NOT EXISTS `datatables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `datatables`
--

INSERT INTO `datatables` (`id`, `username`, `fullname`, `email`, `points`, `notes`, `created_at`, `updated_at`) VALUES
(1, 'Lucio', 'Reilly', 'justine.rogahn@yahoo.com', '60', 'Sit enim itaque numquam corporis est. Dolores numquam rem maiores a. Fugiat blanditiis eos non.', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(2, 'Terrence', 'Doyle', 'ywuckert@heaney.com', '583', 'Molestiae enim rem magnam nulla. Quo voluptas atque vel. Voluptates dolores mollitia laboriosam.', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(3, 'Raegan', 'O''Connell', 'amir.hauck@heidenreich.com', '68', 'Et ut quis nihil dolorem. Quia qui quia sunt maxime. Et aliquid et qui molestias et quis.', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(4, 'Mia', 'Hackett', 'herman.torphy@gmail.com', '163', 'Consequatur blanditiis eius enim ut. Et saepe sit expedita iste maxime.', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(5, 'Luisa', 'Wunsch', 'drutherford@volkman.biz', '867', 'Vel aut voluptates est fuga ea quas. Vitae ut sint est nesciunt.', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(6, 'Willard', 'McCullough', 'lluettgen@gmail.com', '15', 'Eveniet iste voluptatem consequatur. Excepturi quisquam quasi possimus alias.', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(7, 'Ursula', 'Torphy', 'edna15@yahoo.com', '659', 'Autem et cupiditate maxime magni illum. Nihil qui debitis reiciendis nostrum magni est culpa.', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(8, 'Jessica', 'D''Amore', 'wmayer@gmail.com', '596', 'Rerum dolores eligendi voluptatem veniam. Quis tenetur fuga quos a iste possimus repellendus.', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(9, 'Felipa', 'Wolf', 'nels.denesik@gmail.com', '410', 'Voluptas maxime sit et aut. Aut qui enim quo nemo sint. Porro est harum veritatis provident sint.', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(10, 'Else', 'Sporer', 'jensen.renner@morissette.com', '820', 'Facere excepturi eius et. Modi dolore eligendi aut nihil et est. Ullam aut consequuntur amet.', '2016-11-07 16:13:06', '2016-11-07 16:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_airs`
--

DROP TABLE IF EXISTS `hotel_airs`;
CREATE TABLE IF NOT EXISTS `hotel_airs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `hotel_airs`
--

INSERT INTO `hotel_airs` (`id`, `name`, `frname`, `created_at`, `updated_at`) VALUES
(1, 'A AIR CONDITIONNER', 'CLIMATISEUR', '2016-12-01 18:22:06', '0000-00-00 00:00:00'),
(2, '2 AIR CONDITIONNERES', '2 CLIMATISEUR', '2016-12-01 18:22:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_balconies`
--

DROP TABLE IF EXISTS `hotel_balconies`;
CREATE TABLE IF NOT EXISTS `hotel_balconies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `hotel_balconies`
--

INSERT INTO `hotel_balconies` (`id`, `name`, `frname`, `created_at`, `updated_at`) VALUES
(1, '1 BALCONIES', '1 BALCON', '2016-12-01 18:16:31', '0000-00-00 00:00:00'),
(2, '2 BALCONIES', '2 BALCON', '2016-12-01 18:16:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_beds`
--

DROP TABLE IF EXISTS `hotel_beds`;
CREATE TABLE IF NOT EXISTS `hotel_beds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `hotel_beds`
--

INSERT INTO `hotel_beds` (`id`, `name`, `frname`, `created_at`, `updated_at`) VALUES
(1, 'SINGLE BED', '1 LIT', '2016-12-01 18:11:56', '0000-00-00 00:00:00'),
(2, '1 DOUBLE BED', '1 DOUBLE LITS', '2016-12-01 18:12:14', '0000-00-00 00:00:00'),
(3, '2 DOUBLE BED', '2 DOUBLE LITS', '2016-12-01 18:12:30', '0000-00-00 00:00:00'),
(4, '2 BEDS', '2 LITS', '2016-12-01 18:13:36', '0000-00-00 00:00:00'),
(5, '3 BEDS', '3 LITS', '2016-12-01 18:13:51', '0000-00-00 00:00:00'),
(6, '4 BEDS', '4 LITS', '2016-12-01 18:13:56', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_categories`
--

DROP TABLE IF EXISTS `hotel_categories`;
CREATE TABLE IF NOT EXISTS `hotel_categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` text COLLATE utf8_unicode_ci NOT NULL,
  `frdesc` text COLLATE utf8_unicode_ci NOT NULL,
  `rooms` int(10) NOT NULL DEFAULT '0',
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stars` int(10) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `hotel_categories`
--

INSERT INTO `hotel_categories` (`id`, `name`, `frname`, `title`, `frtitle`, `desc`, `frdesc`, `rooms`, `price`, `stars`, `created_at`, `updated_at`) VALUES
(12, 'CATEGORY A', 'CATEGORIE A', 'CATEGORY A ROOM DESCRIPTION', 'CATEGORIE A DESCRIPTION', 'This category is the least expensive, has an area of ... m2, very comfortable Greco-Roman style. All rooms are equipped with air conditioning, telephone, Internet, minibar, safe, DSTV. The room service and dry cleaning are available to clients 24/24.', 'Cette catégorie étant la moins cher, présente une superficie de … m2, un style gréco-romain très confortables. Toutes nos chambres sont équipées d''air climatisé, d''un téléphone, Internet, minibar, coffre-fort, DSTV. Les services de chambre et de lavage à sec sont à la disposition des clients 24h/24.', 12, '81', 4, '2016-12-03 03:08:54', '2016-12-05 15:13:54'),
(13, 'CATEGORY B', 'CATEGORIE B', 'CATEGORY B ROOM DESCRIPTION', 'CATEGORIE B DESCRIPTION', 'This room is a bit more spacious and features a modern style and a terrace.', 'Cette chambre, étant un peu plus spacieuse, offre un style moderne ainsi qu’une terrasse.', 10, '93', 4, '2016-12-03 03:11:23', '2016-12-05 15:18:43'),
(14, 'CATEGORY C', 'CATEGORY C', 'CATEGORY C ROOM DESCRIPTION', 'CATEGORY C ROOM DESCRIPTION', 'This category offers a room for 3 people. When booking this room, or at the latest the day before your arrival, you must specify if you want a double bed plus a Single bed or if you opt for 3 separate beds. In case nothing would have been said, the room will be prepared with two beds, a double bed and a simple bed, and the hotel will not have the possibility to change it.', 'Cette catégorie offre une chambre pour 3 personnes maximum. Lors de la réservation de cette chambre, ou au plus tard la veille de votre arrivée, vous devez préciser si vous souhaitez un lit Double plus un lit Simple ou si vous optez pour 3 lits séparés. Dans le cas où rien n''aurait été précisé, la chambre sera préparée avec 2 lits, un lit Double et un lit Simple, et l''hôtel n''aura plus la possibilité de la modifier.', 10, '104', 4, '2016-12-03 03:12:53', '2016-12-05 15:18:15');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_cat_reservations`
--

DROP TABLE IF EXISTS `hotel_cat_reservations`;
CREATE TABLE IF NOT EXISTS `hotel_cat_reservations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) unsigned NOT NULL,
  `book_num` int(10) NOT NULL DEFAULT '0',
  `remain_num` int(10) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `hotel_cat_reservations`
--

INSERT INTO `hotel_cat_reservations` (`id`, `cat_id`, `book_num`, `remain_num`, `created_at`, `updated_at`) VALUES
(3, 10, 0, 12, '2016-12-03 11:05:46', '0000-00-00 00:00:00'),
(4, 11, 0, 12, '2016-12-03 11:06:50', '0000-00-00 00:00:00'),
(5, 12, 0, 12, '2016-12-03 11:08:54', '0000-00-00 00:00:00'),
(6, 13, 0, 10, '2016-12-03 11:11:23', '0000-00-00 00:00:00'),
(7, 14, 0, 10, '2016-12-03 11:12:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_customers`
--

DROP TABLE IF EXISTS `hotel_customers`;
CREATE TABLE IF NOT EXISTS `hotel_customers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ticket_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `hotel_customers`
--

INSERT INTO `hotel_customers` (`id`, `ticket_id`, `name`, `surname`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(8, '', 'future', 'syg', '12345678', 'future.syg1118@gmail.com', '2016-12-07 11:58:01', '2016-12-07 11:58:01');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_desks`
--

DROP TABLE IF EXISTS `hotel_desks`;
CREATE TABLE IF NOT EXISTS `hotel_desks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hotel_desks`
--

INSERT INTO `hotel_desks` (`id`, `name`, `frname`, `created_at`, `updated_at`) VALUES
(1, 'A DESK', 'UN BUREAU', '2016-12-01 18:17:54', '0000-00-00 00:00:00'),
(2, '2 DESKS', '2 UN BUREAU', '2016-12-01 18:18:04', '0000-00-00 00:00:00'),
(3, '3 DESKS', '3 UN BUREAU', '2016-12-01 18:18:13', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_options`
--

DROP TABLE IF EXISTS `hotel_options`;
CREATE TABLE IF NOT EXISTS `hotel_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `opt_calendar` int(3) NOT NULL DEFAULT '0',
  `opt_wifi` int(3) NOT NULL DEFAULT '0',
  `opt_balconies` int(10) NOT NULL DEFAULT '0',
  `opt_service` int(3) NOT NULL DEFAULT '0',
  `opt_bed` int(10) NOT NULL DEFAULT '0',
  `opt_desk` int(10) NOT NULL DEFAULT '0',
  `opt_minibar` int(3) NOT NULL DEFAULT '0',
  `opt_safe` int(10) NOT NULL DEFAULT '0',
  `opt_table` int(3) NOT NULL DEFAULT '0',
  `opt_tv` int(10) NOT NULL DEFAULT '0',
  `opt_wardrobe` int(10) NOT NULL DEFAULT '0',
  `opt_air` int(3) NOT NULL DEFAULT '0',
  `opt_hair` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `airid` (`opt_air`),
  KEY `balconiesid` (`opt_balconies`),
  KEY `bedid` (`opt_bed`),
  KEY `deskid` (`opt_desk`),
  KEY `safeid` (`opt_safe`),
  KEY `tvid` (`opt_tv`),
  KEY `wardrobeid` (`opt_wardrobe`),
  KEY `catid` (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hotel_options`
--

INSERT INTO `hotel_options` (`id`, `cat_id`, `opt_calendar`, `opt_wifi`, `opt_balconies`, `opt_service`, `opt_bed`, `opt_desk`, `opt_minibar`, `opt_safe`, `opt_table`, `opt_tv`, `opt_wardrobe`, `opt_air`, `opt_hair`, `created_at`, `updated_at`) VALUES
(9, 12, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, '2016-12-03 11:08:54', '0000-00-00 00:00:00'),
(10, 13, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 1, 1, 0, '2016-12-03 11:11:23', '0000-00-00 00:00:00'),
(11, 14, 1, 1, 1, 1, 5, 1, 1, 1, 0, 3, 1, 1, 0, '2016-12-03 11:12:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_photos`
--

DROP TABLE IF EXISTS `hotel_photos`;
CREATE TABLE IF NOT EXISTS `hotel_photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`cat_id`),
  KEY `cat id` (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `hotel_photos`
--

INSERT INTO `hotel_photos` (`id`, `cat_id`, `photo`, `created_at`, `updated_at`) VALUES
(8, 12, '1480734534.jpg', '2016-12-03 11:08:54', '0000-00-00 00:00:00'),
(9, 13, '1480734683.jpg', '2016-12-03 11:11:23', '0000-00-00 00:00:00'),
(10, 14, '1480734773.jpg', '2016-12-03 11:12:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_safes`
--

DROP TABLE IF EXISTS `hotel_safes`;
CREATE TABLE IF NOT EXISTS `hotel_safes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `hotel_safes`
--

INSERT INTO `hotel_safes` (`id`, `name`, `frname`, `created_at`, `updated_at`) VALUES
(1, 'A SAFE', 'UN COFFRE FORT', '2016-12-01 18:19:15', '0000-00-00 00:00:00'),
(2, '2 SAFES', '2 UN COFFRE FORT', '2016-12-01 18:19:28', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_temp_ticket`
--

DROP TABLE IF EXISTS `hotel_temp_ticket`;
CREATE TABLE IF NOT EXISTS `hotel_temp_ticket` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ticket_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(10) NOT NULL DEFAULT '0',
  `cat_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rooms` int(10) NOT NULL DEFAULT '0',
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `arrival_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000-00-00',
  `departure_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000-00-00',
  `adult` int(10) NOT NULL DEFAULT '0',
  `children` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '1',
  `closed_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`ticket_id`),
  KEY `customerid` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `hotel_temp_ticket`
--

INSERT INTO `hotel_temp_ticket` (`id`, `ticket_id`, `customer_id`, `cat_id`, `rooms`, `price`, `arrival_date`, `departure_date`, `adult`, `children`, `name`, `surname`, `phone`, `email`, `status`, `closed_date`, `created_at`, `updated_at`) VALUES
(25, '', 0, 0, 1, '0', '2016-12-07', '2016-12-08', 1, 0, 'future', 'syg', '12345678', 'future.syg1118@gmail.com', 1, '0000-00-00 00:00:00', '2016-12-07 11:56:34', '2016-12-07 11:56:49');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_tickets`
--

DROP TABLE IF EXISTS `hotel_tickets`;
CREATE TABLE IF NOT EXISTS `hotel_tickets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ticket_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(10) NOT NULL DEFAULT '0',
  `cat_id` int(10) unsigned NOT NULL DEFAULT '0',
  `refund_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rooms` int(10) NOT NULL DEFAULT '0',
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `refund_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `arrival_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000-00-00',
  `departure_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000-00-00',
  `adult` int(10) NOT NULL DEFAULT '0',
  `children` int(10) NOT NULL DEFAULT '0',
  `cart` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pay_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payer_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_fee` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `refund_transaction_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `refund_invoice_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `refund_parent_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paid_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '1',
  `closed_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`ticket_id`),
  KEY `customerid` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `hotel_tickets`
--

INSERT INTO `hotel_tickets` (`id`, `ticket_id`, `customer_id`, `cat_id`, `refund_id`, `rooms`, `price`, `refund_price`, `arrival_date`, `departure_date`, `adult`, `children`, `cart`, `pay_id`, `payment_method`, `payer_id`, `transaction_fee`, `transaction_id`, `refund_transaction_id`, `invoice_number`, `refund_invoice_number`, `refund_parent_payment`, `paid_time`, `card_type`, `status`, `closed_date`, `created_at`, `updated_at`) VALUES
(12, '148111188148', 8, 14, '39S86267H1469770W', 1, '104.00', '104.00', '2016-12-07', '2016-12-08', 1, 0, '', 'PAY-2DS51941PL2504633LBD7SRI', 'credit_card', '', '', '99P04510KJ648892X', '99P04510KJ648892X', '5847f93922cb9', '', 'PAY-2DS51941PL2504633LBD7SRI', '2016-12-07T11:57:57Z', 'visa', 0, '2016-12-07 11:58:23', '2016-12-07 11:58:01', '2016-12-07 11:58:01');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_tvs`
--

DROP TABLE IF EXISTS `hotel_tvs`;
CREATE TABLE IF NOT EXISTS `hotel_tvs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `hotel_tvs`
--

INSERT INTO `hotel_tvs` (`id`, `name`, `frname`, `created_at`, `updated_at`) VALUES
(1, 'A TV', 'A TV', '2016-12-01 18:19:46', '0000-00-00 00:00:00'),
(2, 'A TV + 50 FRENCH/ENGLISH CHANELS', 'A TV + 50 CHANELS FRANCAIS/ENGLISH ', '2016-12-01 18:20:09', '0000-00-00 00:00:00'),
(3, 'A TV + 50 FRENCH/INTERNATIONAL CHANELS', 'A TV + 50 CHAINES FRANCAIS/INTERNATIONALES', '2016-12-01 18:20:14', '0000-00-00 00:00:00'),
(4, '2 TVS', '2 TVS', '2016-12-01 18:20:30', '0000-00-00 00:00:00'),
(5, '2 TV + 50 FRANCH/ENGLISH CHANELS', '2 TV + 50 CHANELS FRANCAIS/ENGLISH ', '2016-12-01 18:22:50', '0000-00-00 00:00:00'),
(6, '2 TV + 50 FRENCH/INTERNATIONAL CHANELS', '2 TV + 50 CHAINES FRANCAIS/INTERNATIONALES', '2016-12-01 18:22:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_wardrobes`
--

DROP TABLE IF EXISTS `hotel_wardrobes`;
CREATE TABLE IF NOT EXISTS `hotel_wardrobes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `hotel_wardrobes`
--

INSERT INTO `hotel_wardrobes` (`id`, `name`, `frname`, `created_at`, `updated_at`) VALUES
(1, 'A WARDROBE', 'A UNE GARDE ROBE', '2016-12-01 18:21:22', '0000-00-00 00:00:00'),
(2, '2 WARDROBES', '2 UNE GARDE ROBE', '2016-12-01 18:21:32', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_07_02_230147_migration_cartalyst_sentinel', 1),
('2014_10_04_174350_soft_delete_users', 1),
('2014_12_10_011106_add_fields_to_user_table', 1),
('2015_08_09_200015_create_blog_module_table', 1),
('2015_08_11_064636_add_slug_to_blogs_table', 1),
('2015_08_19_073929_create_taggable_table', 1),
('2015_11_10_140011_create_files_table', 1),
('2016_01_02_062647_create_tasks_table', 1),
('2016_04_26_054601_create_datatables_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `occ_categories`
--

DROP TABLE IF EXISTS `occ_categories`;
CREATE TABLE IF NOT EXISTS `occ_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `occ_categories`
--

INSERT INTO `occ_categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'test111', 'test111', '2016-11-23 03:38:28', '2016-11-23 03:38:34');

-- --------------------------------------------------------

--
-- Table structure for table `occ_gifts`
--

DROP TABLE IF EXISTS `occ_gifts`;
CREATE TABLE IF NOT EXISTS `occ_gifts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `totalprice` float NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `occ_occasions`
--

DROP TABLE IF EXISTS `occ_occasions`;
CREATE TABLE IF NOT EXISTS `occ_occasions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `occ_products`
--

DROP TABLE IF EXISTS `occ_products`;
CREATE TABLE IF NOT EXISTS `occ_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `photos` text COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `falldown` float NOT NULL DEFAULT '0',
  `p_contact_id` int(11) NOT NULL DEFAULT '0',
  `s_contact_id` int(11) NOT NULL DEFAULT '0',
  `occ_id` int(11) NOT NULL DEFAULT '0',
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `occ_shipper_contacts`
--

DROP TABLE IF EXISTS `occ_shipper_contacts`;
CREATE TABLE IF NOT EXISTS `occ_shipper_contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wechat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `occ_shippments`
--

DROP TABLE IF EXISTS `occ_shippments`;
CREATE TABLE IF NOT EXISTS `occ_shippments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalprice` float NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `occ_tickets`
--

DROP TABLE IF EXISTS `occ_tickets`;
CREATE TABLE IF NOT EXISTS `occ_tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `ship_id` int(10) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `totalprice` float NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `state` int(10) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

DROP TABLE IF EXISTS `persistences`;
CREATE TABLE IF NOT EXISTS `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'jzUT5Km1HVo86XTLejZvCfHCKBBImRIn', '2016-11-07 16:19:31', '2016-11-07 16:19:31'),
(5, 1, 'oVWNGuJuWYxYwPfEi2FAhWf1RSMZXTrg', '2016-11-08 15:27:38', '2016-11-08 15:27:38'),
(6, 1, 'pKcp2h4dp90m00vQJeo9nlGm9Pw04Jvt', '2016-11-09 00:49:15', '2016-11-09 00:49:15'),
(8, 1, 'zjHtyL2tC0x9DaRgyuD4Yn93HGkawSok', '2016-11-09 08:39:08', '2016-11-09 08:39:08'),
(9, 1, '1xWN0QsgGyZmbGelq9PMGi2ptAjTfnca', '2016-11-10 17:40:40', '2016-11-10 17:40:40'),
(10, 1, 'RJexQK8nC47ZqrHXhkWl3ikYskTtrZr0', '2016-11-23 03:16:47', '2016-11-23 03:16:47'),
(11, 1, 'aeOEGCK4sP5ncbiHPCluCDMQV6ppjHRw', '2016-11-23 05:29:44', '2016-11-23 05:29:44'),
(12, 1, 'yjncxGtUy3jxVJj2EkHDHgdlaeN1NSGi', '2016-11-23 05:30:46', '2016-11-23 05:30:46'),
(13, 1, 'zUNqRwYs7yyPUgOTELPbvrEN93xlEeu9', '2016-11-23 05:33:00', '2016-11-23 05:33:00'),
(14, 1, 'yOcEdXmuVLgPRKy94rCZPFwwascTiQpc', '2016-11-23 05:33:31', '2016-11-23 05:33:31'),
(15, 1, 'umvwcm3v1bWHa5Y8hq1DrS7cTDwUFVay', '2016-11-23 13:04:48', '2016-11-23 13:04:48'),
(16, 1, 'o8uybM5Dh3e8ZNegcTPtGOm9A5bYUdN2', '2016-11-23 13:10:37', '2016-11-23 13:10:37'),
(17, 1, 'MQSP3ZoC82JECmJZyiOTSypieW6yggCZ', '2016-11-24 01:56:38', '2016-11-24 01:56:38'),
(18, 1, 'QNZdFILV2dbz3dMrAVR8c8MI270tTgg4', '2016-11-24 03:36:02', '2016-11-24 03:36:02'),
(19, 1, 'ClBnFxBGxBfKSm4kfLOxk65TjA3XV5AK', '2016-11-24 03:45:21', '2016-11-24 03:45:21'),
(20, 1, 'pCI8SB4KrfmltaROROuszb2tsgrNCA1q', '2016-11-24 03:51:17', '2016-11-24 03:51:17'),
(21, 1, 'XQxYyLdhhwBY7rXDe0i9JGZoDOQi03iJ', '2016-11-24 03:55:49', '2016-11-24 03:55:49'),
(23, 1, 'MnhJdJceSFlfYLBNEks9JtFErTKY4FyT', '2016-12-01 09:34:29', '2016-12-01 09:34:29'),
(24, 1, 'fbWdt1IoriRIm5ZXRB90UyNBBpbeuyVn', '2016-12-01 09:39:21', '2016-12-01 09:39:21'),
(25, 1, 'bnGqGLswxpwXEGsf7iyeOfoTEBE1x4rV', '2016-12-01 09:45:13', '2016-12-01 09:45:13'),
(27, 1, 'YIUh8cBDBLpldUZ2VFxZh41hUYzztOzt', '2016-12-01 13:23:07', '2016-12-01 13:23:07'),
(28, 1, 'j1dv3oD7wV7eth7vxfMlS15aLlxVs84h', '2016-12-01 15:51:57', '2016-12-01 15:51:57'),
(29, 1, 'mG8L9atgHGGKJi0u3gUwEXUTYawxdzNe', '2016-12-01 16:13:04', '2016-12-01 16:13:04'),
(30, 1, '43Uh0triT1Ho5lXfwR9KygZAinRn8GRg', '2016-12-01 16:13:43', '2016-12-01 16:13:43'),
(31, 1, 'dR1UNbGTZGcg2PUUsWyWG1BeIgv8mAUe', '2016-12-01 16:21:45', '2016-12-01 16:21:45'),
(32, 1, 'deiwf2kywIqdO7tzY7yl3w292xhSX6jy', '2016-12-01 16:26:43', '2016-12-01 16:26:43'),
(33, 1, '3r1PeEVRmbFM7nDzTbxM4kUSGlfZ10G7', '2016-12-01 18:27:32', '2016-12-01 18:27:32'),
(34, 1, 'iAfcO7qCH0qKDDpqaagbFL9jwhCjXZzf', '2016-12-01 18:27:41', '2016-12-01 18:27:41'),
(35, 1, 'zFiVIZNQdavNtDW1YcaSe8O5Hp50wuhN', '2016-12-02 01:00:21', '2016-12-02 01:00:21'),
(36, 1, 'XpRAPEiWdGpv1VMKbhKgCUUYAuez3A4p', '2016-12-02 01:14:26', '2016-12-02 01:14:26'),
(37, 1, '9Y48jRt4ojjNlG0q9ojytbcCeyxyzNCT', '2016-12-02 14:45:59', '2016-12-02 14:45:59'),
(38, 1, 'spkkvTV8DSyTevXIwLLrKOkwpZFXKCH7', '2016-12-02 14:56:27', '2016-12-02 14:56:27'),
(39, 1, 'eei7jFFdGYaNANYTRH9C9ySM6KlcZOJ4', '2016-12-02 15:48:29', '2016-12-02 15:48:29'),
(40, 1, 'YbtQWJq0j8BEBxenV7Sczk5rPgcWmkOr', '2016-12-02 15:49:31', '2016-12-02 15:49:31'),
(41, 1, 'HHtcq1KL3WFPjgMZHDgzswN5SZoaB5Uz', '2016-12-02 16:04:50', '2016-12-02 16:04:50'),
(42, 1, 'Ig7LQg38UziSgWdmPjKdq2fiDzZpLDpG', '2016-12-02 16:08:56', '2016-12-02 16:08:56'),
(43, 1, 'Ex8Zv9HGJrzWFwbQkn0vJeqzrZtr4OcX', '2016-12-02 18:03:36', '2016-12-02 18:03:36'),
(44, 1, 'owifBcPJEegaK8jNXvjeXfSsewnXMBos', '2016-12-02 18:03:46', '2016-12-02 18:03:46'),
(45, 1, 'F4xQLhsnDhHOf2cFoXmURn1gpUnObEBu', '2016-12-03 02:09:02', '2016-12-03 02:09:02'),
(46, 1, 'AVyhVSwXHJ6zHWPRJqu5mzXz1inxb1xO', '2016-12-05 15:11:47', '2016-12-05 15:11:47'),
(47, 1, 'Xpk13XYNTARUZ4GdbJ14A9CzN4sLf6cI', '2016-12-07 03:58:01', '2016-12-07 03:58:01'),
(48, 1, 'KepnDhzbOhlut23VXwiGSaoJPgWG9CvY', '2016-12-07 09:39:47', '2016-12-07 09:39:47'),
(49, 1, 'P9caSg0gJLeLzSGaTGZMnNXe7FtglkyU', '2016-12-07 11:42:18', '2016-12-07 11:42:18');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
CREATE TABLE IF NOT EXISTS `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '{"admin":1}', '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(2, 'subadmin', 'Sub Admin', '{"admin":2}', '2016-11-07 16:13:06', '2016-11-07 16:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
CREATE TABLE IF NOT EXISTS `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2016-11-07 16:13:06', '2016-11-07 16:13:06'),
(2, 2, '2016-11-08 15:03:30', '2016-11-08 15:03:30');

-- --------------------------------------------------------

--
-- Table structure for table `taggable_taggables`
--

DROP TABLE IF EXISTS `taggable_taggables`;
CREATE TABLE IF NOT EXISTS `taggable_taggables` (
  `tag_id` int(11) NOT NULL,
  `taggable_id` int(10) unsigned NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `taggable_taggables_taggable_id_index` (`taggable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taggable_tags`
--

DROP TABLE IF EXISTS `taggable_tags`;
CREATE TABLE IF NOT EXISTS `taggable_tags` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `normalized` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `finished` tinyint(4) NOT NULL DEFAULT '0',
  `task_description` text COLLATE utf8_unicode_ci NOT NULL,
  `task_deadline` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

DROP TABLE IF EXISTS `throttle`;
CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2016-11-08 15:02:53', '2016-11-08 15:02:53'),
(2, NULL, 'ip', '192.168.1.252', '2016-11-08 15:02:53', '2016-11-08 15:02:53'),
(3, NULL, 'global', NULL, '2016-11-08 15:03:03', '2016-11-08 15:03:03'),
(4, NULL, 'ip', '192.168.1.252', '2016-11-08 15:03:03', '2016-11-08 15:03:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fb_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bio` text COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wechat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fb_id`, `email`, `password`, `first_name`, `last_name`, `bio`, `gender`, `dob`, `pic`, `country`, `state`, `city`, `address`, `postal`, `company`, `age`, `email2`, `contactno`, `whatsapp`, `wechat`, `permissions`, `last_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', 'admin@admin.com', '$2y$10$.w6w1vEZ.eap5VjXKwv19uOsDj1kEXL.J2G01mJWc8o5P2BR1PWFW', 'Super', 'Admin', '', '', '2016-11-08', 'NBEVVRZxx5.jpg', '', '', '', '', '', '', '', '', '', '', '', NULL, '2016-12-07 11:42:18', '2016-11-07 16:13:06', '2016-12-07 11:42:18', NULL),
(2, '', 'future.syg1118@gmail.com', '$2y$10$PWzxk1NKW5LmxQg77aiyU./Zhro7whzV3nOSZo4mJkudxAL269h.C', 'blue', 'star', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '2016-11-08 15:03:30', '2016-11-08 15:03:30', '2016-11-08 15:03:30', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ams_plans`
--
ALTER TABLE `ams_plans`
  ADD CONSTRAINT `road_id` FOREIGN KEY (`road_id`) REFERENCES `ams_records` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hotel_options`
--
ALTER TABLE `hotel_options`
  ADD CONSTRAINT `catid` FOREIGN KEY (`cat_id`) REFERENCES `hotel_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hotel_photos`
--
ALTER TABLE `hotel_photos`
  ADD CONSTRAINT `cat id` FOREIGN KEY (`cat_id`) REFERENCES `hotel_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hotel_tickets`
--
ALTER TABLE `hotel_tickets`
  ADD CONSTRAINT `customerid` FOREIGN KEY (`customer_id`) REFERENCES `hotel_customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
