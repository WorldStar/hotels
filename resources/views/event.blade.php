<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Meetings & Events | Ihusi Hotel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="img/favicon.ico" />
    <!-- CSS FILES -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/prettyPhoto.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/2035.responsive.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.1.0.min.js"></script>
    <!-- Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="boxed">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<div id="wrapper" class="container">
    <div class="header"><!-- Header Section -->
        <div class="pre-header"><!-- Pre-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left pre-address-b"><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, GOMA - Congo (DRC)</p></div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <ul class="pre-link-box">
                                <li><a href="/about">About</a></li>
                                <li><a href="/contact">Contact</a></li>
                            </ul>
                        </div>
                        <div class="pull-right">
                            <div class="language-box">
                                <ul>
                                    <li><a href="/"><img alt="language" src="temp/english.png"><span class="language-text">ENGLISH</span></a></li>
                                    <li><a href="/francais"><img alt="language" src="temp/france.png"><span class="language-text">FRANÇAIS</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header"><!-- Main-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left">
                        <div class="logo">
                            <a href="/"><img alt="Logo" src="img/logo.png" width="100" class="img-responsive" /></a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <nav class="nav">
                                <ul id="navigate" class="sf-menu navigate">
                                    <li><a href="/">HOMEPAGE</a></li>
                                    <!--<li class="parent-menu"><a href="#">FEATURES</a>
                                        <ul>
                                            <li><a href="#">2 Homepages</a></li>
                                            <li><a href="#">Ajax/PHP Booking Form</a></li>
                                            <li><a href="#">Ultra Responsive</a></li>
                                            <li><a href="under-construction.html">Countdown Page</a></li>
                                            <li><a href="#">2 Category Pages</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <!--<li class="parent-menu"><a href="#">PAGES</a>
                                        <ul>
                                            <li><a href="about.html">About</a></li>
                                            <li><a href="category-grid.html">Category Grid</a></li>
                                            <li><a href="category-list.html">Category List</a></li>
                                            <li><a href="room-single.html">Room Details</a></li>
                                            <li><a href="reservation-form-dark.html">Dark Reservation Form</a></li>
                                            <li><a href="reservation-form-light.html">Light Reservation Form</a></li>
                                            <li><a href="gallery.html">Gallery</a></li>
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog-details.html">Blog Single</a></li>
                                            <li><a href="left-sidebar-page.html">Left Sidebar Page</a></li>
                                            <li><a href="right-sidebar-page.html">Right Sidebar Page</a></li>
                                            <li><a href="under-construction.html">Under Construction</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <li><a href="/categorylist">ROOMS</a></li>
                                    <li class="active"><a href="/event">MEETINGS & EVENTS</a></li>
                                    <li><a href="/gallery">GALLERY</a></li>
                                    <li><a href="/contact">CONTACT</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right">
                            <div class="button-style-1 margint45">
                                <a href="/reservation_form_dark"><i class="fa fa-calendar"></i>BOOK NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb breadcrumb-1 pos-center">
        <h1>MEETINGS & EVENTS</h1>
    </div>
    <div class="content"><!-- Content Section -->
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-sm-8 blog-post-contents">
                    <div class="blog-post"><!-- Blog Post -->
                        <h3><a href="/blogdetail">MEETINGS</a></h3>
                        <!--<div class="post-materials  clearfix">
                            <ul>
                                <li><h6><a href="#"><i class="fa fa-calendar"></i> 24 MAY 2013</a></h6></li>
                                <li><h6><a href="#"><i class="fa fa-user"></i>ADMIN</a></h6></li>
                                <li><h6><a href="#"><i class="fa fa-comments"></i>13 COMMENTS</a></h6></li>
                                <li><h6><a href="#"><i class="fa fa-tags"></i>DESIGN, WORDPRESS, COMPANY</a></h6></li>
                            </ul>
                        </div>-->
                        <div class="blog-image marginb30 margint30">
                            <img alt="Blog Image 2" class="img-responsive" src="temp/cr.jpg" >
                        </div>
                        <div class="post-content margint10">
                            <p>The Ihusi Hotel offers 2 conference rooms and a dedicated banquets coordinator. The rooms are equipped with the state-of-the-art audio-visual equipment and good internet.</p>

                            <h4>Standard facilities:</h4>
                            <br>➢	Conference Room
                            <br>➢	Writing pads and pens
                            <br>➢	One bottle of mineral water (1 bottles of 350ml per delegate)
                            <br>➢	Complimentary WIFI Internet Connection
                            <br><br>
                            <h4>On request and plus charges</h4>
                            <br>➢	Mid morning tea, coffee or herbal infusions with pastries
                            <br>➢	Chefs tailored lunch menu with one soft drink at the main restaurant
                            <br>➢	After noon tea / coffee break with pastries

                        </div>
                    </div>
                    <div class="blog-post">
                        <h3><a href="/blogdetail">WEDDING EVENTS</a></h3>
                        <div class="blog-image marginb30 margint30">
                            <div class="flexslider">
                                <ul class="slides">
                                    <li><img alt="Blog Image 3" class="img-responsive" src="temp/wd.jpg"  /> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-content margint10">
                            <p>The hotel provides a big wedding and event room that can contain up to 400 chairs.  We offer the room only, all the complementary services are to be covered by the customer. Catering service is optional.</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 col-sm-4 margint60"><!-- Sidebar -->
                    <div class="luxen-widget news-widget">
                        <div class="title">
                            <h5>HOTEL INFORMATION</h5>
                        </div>
                        <p>Ihusi Hotel is located 6 km from Goma International Airport and 2 km away from the main NGOs district.</p>
                    </div>
                    <div class="luxen-widget news-widget">
                        <div class="title">
                            <h5>CONTACT</h5>
                        </div>
                        <ul class="footer-links">
                            <li><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, Congo (DRC) </p></li>
                            <li><p><i class="fa fa-phone"></i> +243(0) 81 31 29 560 </p></li>
                            <li><p><i class="fa fa-envelope"></i> ihusihotel@ihusigroups.com</p></li>
                        </ul>
                    </div>
                    <div class="luxen-widget news-widget">
                        <div class="title">
                            <h5>SOCIAL MEDIA</h5>
                        </div>
                        <ul class="social-links">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-vine"></i></a></li>
                            <li><a href="#"><i class="fa fa-foursquare"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer margint40"><!-- Footer Section -->
        <div class="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-sm-2 footer-logo">
                        <img alt="Logo" src="img/logo.png" class="img-responsive" >
                    </div>
                    <div class="col-lg-10 col-sm-10">
                        <div class="col-lg-3 col-sm-3">
                            <h6>DISCOVER IEXPRESS</h6>
                            <ul class="footer-links">
                                <li>Book and pay for your boats tickets online by using our newly designed Ihusi Express Web App. Click <a href="http://iexpress.ihusigroups.com" target="_blank">HERE</a> to discover the app.<br><br><img src="temp/logo.png" width="150"></li>
                                <li>Available for<br><img src="temp/android.png"></li>
                                <li><p><img src="temp/ios.png"></p></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>THINGS TO DO (OUTSIDE SERVICE)</h6>
                            <ul class="footer-links">
                                <li>Visit the Virunga National Park and The Nyiragongo Mount<br><img src="temp/1.png" width="150"></a><br>Virunga National Park Tourism Office
                                    <br>Boulevard Kanya Mulanga
                                    <br>Goma, DRC

                                    <br><br><i class="fa fa-envelope"></i><a href="mailto:visit@virunga.org">visit@virunga.org</a>
                                    <br><i class="fa fa-phone"></i>+243 99 1715401</li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>PAGES SITE</h6>
                            <ul class="footer-links">
                                <li><a href="/contact">Contact</a></li>
                                <li><a href="/gallery">Gallery</a></li>
                                <li><a href="/categorylist">Rooms</a></li>
                                <br><br>
                                <li>
                                    <div class="newsletter-form margint40">
                                        <div class="newsletter-wrapper">
                                            <div class="pull-left">
                                                <h2><i class="fa fa-shopping-cart"></i><b>Ihusi Shop</b></h2>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>Buy souvenirs at the Ihusi Shop during your stay. We are top on fashion! <br>Please visit us.
                                <li><i class="fa fa-envelope"></i><a href="mailto:ishop@ihusigroups.com">ishop@ihusigroups.com</a>
                                    <br><i class="fa fa-phone"></i>+243 99 0000000
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>CONTACT</h6>
                            <ul class="footer-links">
                                <li><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, Congo (DRC)</p></li>
                                <li><p><i class="fa fa-phone"></i> +243(0) 81 31 29 560 </p></li>
                                <li><p><i class="fa fa-envelope"></i> <a href="mailto:info@2035themes.com">ihusihotel@ihusigroups.com</a></p></li>
                                <li><br><br><b>TRIP ADVISOR AWARDED</b></li>
                                <li><img src="temp/ta.jpg" width="80">&nbsp;&nbsp;<img src="temp/tatc.jpg" width="80"></li>
                                <li><br></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pre-footer">
            <div class="container">
                <div class="row">
                    <div class="pull-left"><p>© IHUSI HOTEL 2016</p></div>
                    <div class="pull-right">
                        <ul>
                            <li><p>DESIGNED BY aXiom INVENT</p></li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- JS FILES -->
<script src="js/vendor/jquery-1.11.1.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/retina-1.1.0.min.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/superfish.pack.1.4.1.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/fitvids.js"></script>
<script src="js/main.js"></script>
<!--
<script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src='//www.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
-->
</body>
</html>