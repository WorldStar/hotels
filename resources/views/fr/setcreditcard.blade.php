<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Détails de la chambre | Ihusi Hotel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="/fr/img/favicon.ico" />
    <!-- CSS FILES -->
    <link rel="stylesheet" href="/fr/css/bootstrap.min.css">
    <link rel="stylesheet" href="/fr/css/flexslider.css">
    <link rel="stylesheet" href="/fr/css/prettyPhoto.css">
    <link rel="stylesheet" href="/fr/css/datepicker.css">
    <link rel="stylesheet" href="/fr/css/selectordie.css">
    <link rel="stylesheet" href="/fr/css/main.css">
    <link rel="stylesheet" href="/fr/css/2035.responsive.css">

    <script src="/fr/js/vendor/modernizr-2.8.3-respond-1.1.0.min.js"></script>
    <!-- Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/fr/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="boxed">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<div id="wrapper" class="container">
    <div class="header"><!-- Header Section -->
        <div class="pre-header"><!-- Pre-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left pre-address-b"><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, GOMA - Congo (DRC)</p></div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <ul class="pre-link-box">
                                <li><a href="/fr/about.html">A propos de nous</a></li>
                                <li><a href="/fr/contact.html">Contact</a></li>
                            </ul>
                        </div>
                        <div class="pull-right">
                            <div class="language-box">
                                <ul>
                                    <li><a href="/francais"><img alt="language" src="/fr/temp/france.png"><span class="language-text">FRANÇAIS</span></a></li>
                                    <li><a href="/"><img alt="language" src="/fr/temp/english.png"><span class="language-text">ENGLISH</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header"><!-- Main-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left">
                        <div class="logo">
                            <a href="/francais"><img alt="Logo" src="/fr/img/logo.png" width="100" class="img-responsive" /></a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <nav class="nav">
                                <ul id="navigate" class="sf-menu navigate">
                                    <li><a href="/francais">ACCUEIL</a></li>
                                    <!--<li class="parent-menu"><a href="#">FEATURES</a>
                                        <ul>
                                            <li><a href="#">2 Homepages</a></li>
                                            <li><a href="#">Ajax/PHP Booking Form</a></li>
                                            <li><a href="#">Ultra Responsive</a></li>
                                            <li><a href="under-construction.html">Countdown Page</a></li>
                                            <li><a href="#">2 Category Pages</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <!--<li class="parent-menu"><a href="#">PAGES</a>
                                        <ul>
                                            <li><a href="about.html">About</a></li>
                                            <li><a href="category-grid.html">Category Grid</a></li>
                                            <li><a href="category-list.html">Category List</a></li>
                                            <li><a href="room-single.html">Room Details</a></li>
                                            <li><a href="reservation-form-dark.html">Dark Reservation Form</a></li>
                                            <li><a href="reservation-form-light.html">Light Reservation Form</a></li>
                                            <li><a href="gallery.html">Gallery</a></li>
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog-details.html">Blog Single</a></li>
                                            <li><a href="left-sidebar-page.html">Left Sidebar Page</a></li>
                                            <li><a href="right-sidebar-page.html">Right Sidebar Page</a></li>
                                            <li><a href="under-construction.html">Under Construction</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <li class="active"><a href="/francais/categorylist">CHAMBRES</a></li>
                                    <li><a href="/fr/event.html">REUNION & EVENEMENTS</a></li>
                                    <li><a href="/fr/gallery.html">GALLERIE PHOTO</a></li>
                                    <li><a href="/fr/contact.html">CONTACT</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right">
                            <div class="button-style-1 margint45">
                                <a href="/francais/reservation_form_dark"><i class="fa fa-calendar"></i>RESERVATION</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb breadcrumb-1 pos-center">
        <h1>DETAILS DE LA CHAMBRE </h1>
    </div>

    <?php
    if(isset($_SESSION['error_message']) && $_SESSION['error_message'] != ''){
        $error_message = $_SESSION['error_message'];
        $_SESSION['error_message'] = '';
        echo '<script>alert("'.$error_message.'");</script>';
    }
    ?>
    <form class="login-form" id="paymentform" method="post" action="">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" id="temp_id" name="temp_id" value="{{ $temp_id }}">
        <input type="hidden" id="cat_id" name="cat_id" value="{{ $cat_id }}">

    <div class="content"><!-- Content Section -->
        <div class="container">
            <div class="row">

                <table class="table table-striped table-bordered table-hover" style="width:80%;margin-left:10%;margin-top:20px;">
                    <tbody>
                    <tr>
                        <td id="td-label">First Name : </td>
                        <td><input type="text" name="firstName" id="firstName" data-tooltip="{ 'offset': 30 }" title="first name" placeholder="first name" required="true"></td>

                    </tr>
                    <tr>
                        <td id="td-label">Last Name : </td>
                        <td><input type="text" name="lastName" id="lastName" data-tooltip="{ 'offset': 30 }" title="last name" placeholder="last name" required="true"></td>

                    </tr>
                    <tr>
                        <td id="td-label">Card Type : </td>
                        <td>
                            <select name="creditCardType" id="creditCardType" data-tooltip="{ 'offset': 30 }" title="card type">
                                <option value="visa" selected="selected">Visa</option>
                                <option value="masterCard">MasterCard</option>
                                <option value="discover">Discover</option>
                                <option value="amex">American Express</option>
                            </select>
                        </td>

                    </tr>
                    <tr>

                        <td id="td-label">Card Number : </td>
                        <td><input type="text" name="creditCardNumber" id="creditCardNumber" size="40" data-tooltip="{ 'offset': 30 }" title="card number" placeholder="card number" required="true"></td>

                    </tr>

                    <tr>

                        <td id="td-label">Expired Date : </td>
                        <td><select name="expDateMonth" id="expDateMonth" data-tooltip="{ 'offset': 30 }" title="expired month">
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                            <?php
                            $year = date('Y');
                            ?>

                            <select name="expDateYear" id="expDateYear" data-tooltip="{ 'offset': 30 }" title="expired year">
                                <?php for($i = $year; $i < ($year+10); $i++){?>
                                <option value="{{ $i }}">{{ $i }}</option>
                                <?php } ?>
                            </select>
                        </td>

                    </tr>

                    <tr>

                        <td id="td-label">CVV : </td>
                        <td><div id="date-div"><input type="text" name="cvv2Number" id="cvv2Number" data-tooltip="{ 'offset': 30 }" title="cvv" placeholder="cvv" required="true"></div></td>

                    </tr>
                    <tr>
                        <input type="hidden" name="amount" id="amount" value="{{ $total }}">
                        <td id="td-label">Amount ( US $ ) : </td>
                        <td><input type="text" name="amount1" id="amount1" placeholder="amount" value="{{ $total }}" disabled="true"></td>

                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="row" style="text-align: center">
                <img src="/img/paypal.png" style="width:210px;margin-top:15px;margin-right:20px;cursor: pointer" onclick="setPaypal()">

            </div>
        </div>
    </div>
    </form>
    <div class="footer margint40"><!-- Footer Section -->
        <div class="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-sm-2 footer-logo">
                        <img alt="Logo" src="/fr/img/logo.png" class="img-responsive" >
                    </div>
                    <div class="col-lg-10 col-sm-10">
                        <div class="col-lg-3 col-sm-3">
                            <h6>DECOUVREZ IEXPRESS</h6>
                            <ul class="footer-links">
                                <li>Réservez et payez vos billets de bateaux en ligne en utilisant notre application web et mobile IExpress nouvellement conçu. Cliquez<a href="http://iexpress.ihusigroups.com" target="_blank"> ICI</a> pour découvrir l'application.<br><br><img src="/fr/temp/logo.png" width="150"></li>
                                <li>Disponible pour<br><img src="/fr/temp/android.png"></li>
                                <li><p><img src="/fr/temp/ios.png"></p></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>ACTIVITES À FAIRE PENDANT VOTRE SEJOUR (SERVICE EXTÉRIEUR)</h6>
                            <ul class="footer-links">
                                <li>Visitez le Parc national des Virunga et le Nyiragongo Montagne
                                    <br><img src="/fr/temp/1.png" width="150"></a><br>Bureau de Tourisme Parc national de Virunga
                                    <br>Boulevard Kanya Mulanga
                                    <br>Goma, DRC

                                    <br><br><i class="fa fa-envelope"></i><a href="mailto:visit@virunga.org">visit@virunga.org</a>
                                    <br><i class="fa fa-phone"></i>+243 99 1715401</li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>PAGES DU SITE</h6>
                            <ul class="footer-links">
                                <li><a href="/fr/contact.html">Contact</a></li>
                                <li><a href="/fr/gallery.html">Gallerie</a></li>
                                <li><a href="/feancais/categorylist">Chambres</a></li>
                                <br><br>
                                <li>
                                    <div class="newsletter-form margint40">
                                        <div class="newsletter-wrapper">
                                            <div class="pull-left">
                                                <h2><i class="fa fa-shopping-cart"></i><b>Ihusi Shop</b></h2>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>Acheter des souvenirs à la boutique Ihusi pendant votre séjour.<br> Veuillez nous rendre visite.
                                <li><i class="fa fa-envelope"></i><a href="mailto:ishop@ihusigroups.com">ishop@ihusigroups.com</a>
                                    <br><i class="fa fa-phone"></i>+243 99 0000000
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>CONTACT</h6>
                            <ul class="footer-links">
                                <li><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, Congo (DRC)</p></li>
                                <li><p><i class="fa fa-phone"></i> +243(0) 81 31 29 560 </p></li>
                                <li><p><i class="fa fa-envelope"></i> <a href="mailto:info@2035themes.com">ihusihotel@ihusigroups.com</a></p></li>
                                <li><br><br><b>TRIP ADVISOR AWARDED</b></li>
                                <li><img src="/fr/temp/ta.jpg" width="80">&nbsp;&nbsp;<img src="/fr/temp/tatc.jpg" width="80"></li>
                                <li><br></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pre-footer">
            <div class="container">
                <div class="row">
                    <div class="pull-left"><p>© IHUSI HOTEL 2016</p></div>
                    <div class="pull-right">
                        <ul>
                            <li><p>DEVELOPPER PAR aXiom INVENT</p></li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- JS FILES -->


<script src="/fr/js/vendor/jquery-1.11.1.min.js"></script>
<script src="/fr/js/vendor/bootstrap.min.js"></script>
<script src="/fr/js/retina-1.1.0.min.js"></script>
<script src="/fr/js/jquery.flexslider-min.js"></script>
<script src="/fr/js/superfish.pack.1.4.1.js"></script>
<script src="/fr/js/jquery.slicknav.min.js"></script>
<script src="/fr/js/jquery.prettyPhoto.js"></script>
<script src="/fr/js/bootstrap-datepicker.js"></script>
<script src="/fr/js/selectordie.min.js"></script>
<script src="/fr/js/jquery.parallax-1.1.3.js"></script>
<script src="/fr/js/jquery.simpleWeather.min.js"></script>
<script src="/fr/js/main.js"></script>
<!--
<script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src='//www.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
-->
<script>
    function setPaypal(){
        document.getElementById('paymentform').action = '{{ route('fr-pay-creditcard') }}';
        document.getElementById('paymentform').submit();
    }
</script>
</body>
</html>