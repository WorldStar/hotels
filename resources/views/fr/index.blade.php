<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Bienvenu à Ihusi Hotel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="/fr/img/favicon.ico" />
    <!-- CSS FILES -->
    <link rel="stylesheet" href="/fr/css/bootstrap.min.css">
    <link rel="stylesheet" href="/fr/css/flexslider.css">
    <link rel="stylesheet" href="/fr/css/prettyPhoto.css">
    <link rel="stylesheet" href="/fr/css/datepicker.css">
    <link rel="stylesheet" href="/fr/css/selectordie.css">
    <link rel="stylesheet" href="/fr/css/main.css">
    <link rel="stylesheet" href="/fr/css/2035.responsive.css">

    <script src="/fr/js/vendor/modernizr-2.8.3-respond-1.1.0.min.js"></script>
    <!-- Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/fr/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="boxed">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<?php
if(isset($_SESSION['error_message']) && $_SESSION['error_message'] != ''){
    $error_message = $_SESSION['error_message'];
    $_SESSION['error_message'] = '';
    echo '<script>alert("'.$error_message.'");</script>';
}
?>
<div id="wrapper" class="container">
    <div class="header"><!-- Header Section -->
        <div class="pre-header"><!-- Pre-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left pre-address-b"><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, GOMA - Congo (DRC)</p></div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <ul class="pre-link-box">
                                <li><a href="/fr/about.html">A propos de nous</a></li>
                                <li><a href="/fr/contact.html">Contact</a></li>
                            </ul>
                        </div>
                        <div class="pull-right">
                            <div class="language-box">
                                <ul>
                                    <li><a href="/francais"><img alt="language" src="/fr/temp/france.png"><span class="language-text">FRANÇAIS</span></a></li>
                                    <li><a href="/"><img alt="language" src="/fr/temp/english.png"><span class="language-text">ENGLISH</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header"><!-- Main-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left">
                        <div class="logo">
                            <a href="/francais"><img alt="Logo" src="img/logo.png" width="100" class="img-responsive" /></a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <nav class="nav">
                                <ul id="navigate" class="sf-menu navigate">
                                    <li class="active"><a href="/francais">ACCUEIL</a></li>
                                    <!--<li class="parent-menu"><a href="#">FEATURES</a>
                                        <ul>
                                            <li><a href="#">2 Homepages</a></li>
                                            <li><a href="#">Ajax/PHP Booking Form</a></li>
                                            <li><a href="#">Ultra Responsive</a></li>
                                            <li><a href="under-construction.html">Countdown Page</a></li>
                                            <li><a href="#">2 Category Pages</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <!--<li class="parent-menu"><a href="#">PAGES</a>
                                        <ul>
                                            <li><a href="about.html">About</a></li>
                                            <li><a href="category-grid.html">Category Grid</a></li>
                                            <li><a href="category-list.html">Category List</a></li>
                                            <li><a href="room-single.html">Room Details</a></li>
                                            <li><a href="reservation-form-dark.html">Dark Reservation Form</a></li>
                                            <li><a href="reservation-form-light.html">Light Reservation Form</a></li>
                                            <li><a href="gallery.html">Gallery</a></li>
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog-details.html">Blog Single</a></li>
                                            <li><a href="left-sidebar-page.html">Left Sidebar Page</a></li>
                                            <li><a href="right-sidebar-page.html">Right Sidebar Page</a></li>
                                            <li><a href="under-construction.html">Under Construction</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <li><a href="/francais/categorylist">CHAMBRES</a></li>
                                    <li><a href="/fr/event.html">REUNION & EVENEMENTS</a></li>
                                    <li><a href="/fr/gallery.html">GALLERIE PHOTO</a></li>
                                    <li><a href="/fr/contact.html">CONTACT</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right">
                            <div class="button-style-1 margint45">
                                <a href="/francais/reservation_form_dark"><i class="fa fa-calendar"></i>RESERVATION</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slider slider-home"><!-- Slider Section -->
        <div class="flexslider slider-loading falsenav">
            <ul class="slides">
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">BIENVENU A L'HOTEL IHUSI  - LAC KIVU...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">HOTEL 4 ETOILES SITUE DANS LE NORD - KIVU, GOMA - RDC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="/fr/temp/sli-1.jpg" />
                </li>
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">BIENVENU A L'HOTEL IHUSI  - LAC KIVU...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">HOTEL 4 ETOILES SITUE DANS LE NORD - KIVU, GOMA - RDC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="/fr/temp/sli-2.jpg" />
                </li>
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">BIENVENU A L'HOTEL IHUSI  - LAC KIVU...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">HOTEL 4 ETOILES SITUE DANS LE NORD - KIVU, GOMA - RDC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="/fr/temp/maps.jpg" />
                </li>
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">BIENVENU A L'HOTEL IHUSI  - LAC KIVU...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">HOTEL 4 ETOILES SITUE DANS LE NORD - KIVU, GOMA - RDC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="/fr/temp/4.jpg" />
                </li>
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">BIENVENU A L'HOTEL IHUSI  - LAC KIVU...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">HOTEL 4 ETOILES SITUE DANS LE NORD - KIVU, GOMA - RDC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="/fr/temp/7.jpg" />
                </li>
            </ul>
        </div>
        <div class="book-slider">
            <div class="container">
                <div class="row pos-center">
                    <div class="reserve-form-area">
                        <form action="#" method="post" id="ajax-reservation-form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <ul class="clearfix">
                                <li class="li-input">
                                    <label>ARRIVER</label>
                                    <input type="text" id="dpd1" name="dpd1" class="date-selector" placeholder="&#xf073;" />
                                </li>
                                <li class="li-input">
                                    <label>DEPART</label>
                                    <input type="text" id="dpd2" name="dpd2" class="date-selector" placeholder="&#xf073;" />
                                </li>
                                <li class="li-select">
                                    <label>CHAMBRES</label>
                                    <select name="rooms" class="pretty-select">
                                        <option selected="selected" value="1" >1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </li>
                                <li class="li-select">
                                    <label>ADULTE</label>
                                    <select name="adult" class="pretty-select">
                                        <option selected="selected" value="1" >1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </li>
                                <li class="li-select">
                                    <label>ENFANT(S)</label>
                                    <select name="children" class="pretty-select">
                                        <option selected="selected" value="0" >0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </li>
                                <li>
                                    <div class="button-style-1 margint40">
                                        <a id="res-submit" href="#"><i class="fa fa-search"></i>CHERCHER</a>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-book-slider">
            <div class="container">
                <div class="row pos-center">
                    <ul>
                        <li><i class="fa fa-shopping-cart"></i> VISITEZ IHUSI SHOP</li>
                        <!--<li><i class="fa fa-globe"></i> LANGUAGE COMPATIBLE</li>-->
                        <li><i class="fa fa-coffee"></i> CAFE & PETIT DEJEUNER GRATUIT</li>
                        <li><i class="fa fa-windows"></i> WI-FI GRATUIT</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="content"><!-- Content Section -->
        <div class="about clearfix"><!-- About Section -->
            <div class="container">
                <div class="row">
                    <div class="about-title pos-center">
                        <h2>BIENVENU A L'HOTEL IHUSI</h2>
                        <div class="title-shape"><img alt="Shape" src="img/shape.png"></div>
                        <p>Ihusi Hotel est situé à quelques mètres de la frontière avec le Rwanda et se trouve à 6km de l'aéroport International de Goma.</p>
                    </div>
                    <div class="otel-info margint60">
                        <div class="col-lg-4 col-sm-12">
                            <div class="title-style-1 marginb40">
                                <h5>GALLERIE</h5>
                                <hr>
                            </div>
                            <div class="flexslider">
                                <ul class="slides">
                                    <li><img alt="Slider 1" class="img-responsive" src="/fr/temp/imgresp/1.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="/fr/temp/imgresp/2.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="/fr/temp/imgresp/3.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="/fr/temp/imgresp/4.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="/fr/temp/imgresp/5.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="/fr/temp/imgresp/6.jpg" /></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="title-style-1 marginb40">
                                <h5>A PROPOS DE NOUS</h5>
                                <hr>
                            </div>
                            <p align="justify">Ihusi Hotel est un hôtel 4 étoiles situé à Goma, capitale touristique de la République Démocratique du Congo, dans la province du Nord-Kivu.</p>

                            <p align="justify">Situé au bord du lac Kivu, Ihusi Hôtel offre une vue imprenable sur les chaînes de montagnes du célèbre parc du Virunga, second patrimoine mondial de l’UNESCO. </p>

                            <p align="justify">Reputé pour son accueil chaleureux, l’hôtel vous charmera entre autres par sa gastronomie maintes fois acclamée. </p>

                            <p align="justify">L’hôtel offre 72 chambres élégantes, de différentes catégories pour tous les styles de séjour. Avec des terrasses privées avec vu sur le lac ou sur le jardin, meublées dans un style moderne, et pourvues de DSTV et  d’une connexion internet.
                                &nbsp;&nbsp;<a class="active-color" href="about.html">[LIRE LA SUITE]</a></p>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="title-style-1 marginb40">
                                <h5><i class="fa fa-money"></i>BANKING</h5>
                                <hr>
                            </div>
                            <div class="home-news">
                                <div class="news-box clearfix">

                                    <div class="news-content pull-left">
                                        <h6><a href="#">RETIRER DE L'ARGENT EN TOUTE SECURITE AVEC TMB</a></h6>
                                        <p class="margint10">Nous avons un distributeur automatique de billets à l'intérieur qui permet à nos clients de retirer de l'argent en toute sécurité. Le distributeur automatique de billets est fourni par une banque locale.</p>
                                    </div>
                                </div>
                                <div class="news-box clearfix">

                                    <div class="news-content pull-left">
                                        <img src="/fr/temp/tmb.jpg" width="350"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="explore-rooms margint30 clearfix"><!-- Explore Rooms Section -->
            <div class="container">
                <div class="row">
                    <div class="title-style-2 marginb40 pos-center">
                        <h3>EXPLORER NOS CHAMBRES</h3>
                        <hr>
                    </div>
                    <?php
                    $categories = DB::table('hotel_categories as c')
                            ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
                            ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
                            ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
                            ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
                            ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
                            ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
                            ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
                            ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
                            ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
                            ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.name as balname', 'a.name as airname', 'be.name as bedname', 'd.name as deskname', 'p.photo', 's.name as safename', 't.name as tvname', 'w.name as wname'])->orderby('c.id', 'asc')->limit(3)->get();
                    foreach($categories as $category){
                        ?>
                    <div class="col-lg-4 col-sm-6">
                        <div class="home-room-box">
                            <div class="room-image">
                                <img alt="Room Images" class="img-responsive" src="/uploads/files/{{ $category->photo }}">
                                <div class="home-room-details">
                                    <h5><a href="/francais/catitem/{{ $category->id }}">{{ $category->frname }}</a></h5>
                                    <div class="pull-left">
                                        <ul>
                                            @if($category->opt_calendar == 1)
                                                <li><i class="fa fa-calendar"></i></li>
                                            @endif
                                            @if($category->opt_wifi == 1)
                                                <li><i class="fa fa-flask"></i></li>
                                            @endif
                                            @if($category->opt_balconies != 0)
                                                <li><i class="fa fa-umbrella"></i></li>
                                            @endif
                                            @if($category->opt_tv != 1)
                                                <li><i class="fa fa-laptop"></i></li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="pull-right room-rating">
                                        <ul>
                                            <?php if($category->stars == 1){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php }else if($category->stars == 2){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php }else if($category->stars == 3){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php }else if($category->stars == 4){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php }else if($category->stars == 5){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <?php }else{ ?>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="room-details" style="height: 140px;max-height: 140px;">
                                <p>{{ $category->frdesc }}</p>
                            </div>
                            <div class="room-bottom">
                                <div class="pull-left"><h4>{{ $category->price }}$<span class="room-bottom-time">/ Jour</span></h4></div>
                                <div class="pull-right">
                                    <div class="button-style-1">
                                        <a href="/francais/catitem/{{ $category->id }}">LIRE LA SUITE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div id="parallax123" class="parallax parallax-one clearfix margint60"><!-- Parallax Section -->
            <div class="support-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-4">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <div class="flipper">
                                    <div class="support-box pos-center front">
                                        <div class="support-box-title"><i class="fa fa-phone"></i></div>
                                        <h4>APPELEZ NOUS</h4>
                                        <p class="margint20">Le centre d'appel de l'Hotel Ihusi est ouvert 24/24.<br>Appelez nous</p>
                                    </div>
                                    <div class="support-box pos-center back">
                                        <div class="support-box-title"><i class="fa fa-phone"></i></div>
                                        <h4>NUMEROS DE TELEPHONE</h4>
                                        <p class="margint20">+243(0) 81 31 29 560 (Réception)
                                            <br /> +243(0) 81 35 32 300 (GERANCE)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <div class="flipper">
                                    <div class="support-box pos-center front">
                                        <div class="support-box-title"><i class="fa fa-envelope"></i></div>
                                        <h4>ECRIVEZ NOUS</h4>
                                        <p class="margint20">Pour toutes questions concernant l'hotel et ses services, envoyez nous un email.</p>
                                    </div>
                                    <div class="support-box pos-center back">
                                        <div class="support-box-title"><i class="fa fa-envelope"></i></div>
                                        <h4>ADRESSE E-MAIL</h4>
                                        <p class="margint20">Envoyez nous un email à cette adresse:<br />ihusihotel@yahoo.fr</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <div class="flipper">
                                    <div class="support-box pos-center front">
                                        <div class="support-box-title"><i class="fa fa-home"></i></div>
                                        <h4>RENDEZ-NOUS VISITE</h4>
                                        <p class="margint20">Les portes de l'hotel sont ouvert 24/24.</p>
                                    </div>
                                    <div class="support-box pos-center back">
                                        <div class="support-box-title"><i class="fa fa-home"></i></div>
                                        <h4>ADRESSE PHYSIQUE</h4>
                                        <p class="margint20">Ihusi Hotel, 140 Boulevard Kanyamuhanga, <br>Goma North Kivu<br>Congo (DRC)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Newsletter Section -->
        <!--<div class="newsletter-section">
            <div class="container">
                <div class="row">
                    <div class="newsletter-top pos-center margint30">
                        <img alt="Shape Image" src="img/shape.png" >
                    </div>
                    <div class="newsletter-form margint40 pos-center">
                        <div class="newsletter-wrapper">
                            <div class="pull-left">
                                <h2>Sign up newsletter</h2>
                            </div>
                            <div class="pull-left">
                                <form action="#" method="post" id="ajax-contact-form">
                                    <input type="text" placeholder="Enter a e-mail address">
                                    <input type="submit" value="SUBSCRIBE" >
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="footer margint40"><!-- Footer Section -->
            <div class="main-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-sm-2 footer-logo">
                            <img alt="Logo" src="img/logo.png" class="img-responsive" >
                        </div>
                        <div class="col-lg-10 col-sm-10">
                            <div class="col-lg-3 col-sm-3">
                                <h6>DECOUVREZ IEXPRESS</h6>
                                <ul class="footer-links">
                                    <li>Réservez et payez vos billets de bateaux en ligne en utilisant notre application web et mobile IExpress nouvellement conçu. Cliquez<a href="http://iexpress.ihusigroups.com" target="_blank"> ICI</a> pour découvrir l'application.<br><br><img src="/fr/temp/logo.png" width="150"></li>
                                    <li>Disponible pour<br><img src="/fr/temp/android.png"></li>
                                    <li><p><img src="/fr/temp/ios.png"></p></li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <h6>ACTIVITES À FAIRE PENDANT VOTRE SEJOUR (SERVICE EXTÉRIEUR)</h6>
                                <ul class="footer-links">
                                    <li>Visitez le Parc national des Virunga et le Nyiragongo Montagne
                                        <br><img src="/fr/temp/1.png" width="150"></a><br>Bureau de Tourisme Parc national de Virunga
                                        <br>Boulevard Kanya Mulanga
                                        <br>Goma, DRC

                                        <br><br><i class="fa fa-envelope"></i><a href="mailto:visit@virunga.org">visit@virunga.org</a>
                                        <br><i class="fa fa-phone"></i>+243 99 1715401</li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <h6>PAGES DU SITE</h6>
                                <ul class="footer-links">
                                    <li><a href="contact.html">Contact</a></li>
                                    <li><a href="gallery.html">Gallerie</a></li>
                                    <li><a href="/francais/categorylist">Chambres</a></li>
                                    <br><br>
                                    <li>
                                        <div class="newsletter-form margint40">
                                            <div class="newsletter-wrapper">
                                                <div class="pull-left">
                                                    <h2><i class="fa fa-shopping-cart"></i><b>Ihusi Shop</b></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>Acheter des souvenirs à la boutique Ihusi pendant votre séjour.<br> Veuillez nous rendre visite.
                                    <li><i class="fa fa-envelope"></i><a href="mailto:ishop@ihusigroups.com">ishop@ihusigroups.com</a>
                                        <br><i class="fa fa-phone"></i>+243 99 0000000
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <h6>CONTACT</h6>
                                <ul class="footer-links">
                                    <li><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, Congo (DRC)</p></li>
                                    <li><p><i class="fa fa-phone"></i> +243(0) 81 31 29 560 </p></li>
                                    <li><p><i class="fa fa-envelope"></i> <a href="mailto:info@2035themes.com">ihusihotel@ihusigroups.com</a></p></li>
                                    <li><br><br><b>TRIP ADVISOR AWARDED</b></li>
                                    <li><img src="/fr/temp/ta.jpg" width="80">&nbsp;&nbsp;<img src="/fr/temp/tatc.jpg" width="80"></li>
                                    <li><br></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pre-footer">
                <div class="container">
                    <div class="row">
                        <div class="pull-left"><p>© IHUSI HOTEL 2016</p></div>
                        <div class="pull-right">
                            <ul>
                                <li><p>DEVELOPPER PAR aXiom INVENT</p></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- JS FILES -->

<script src="/fr/js/vendor/jquery-1.11.1.min.js"></script>
<script src="/fr/js/vendor/bootstrap.min.js"></script>
<script src="/fr/js/retina-1.1.0.min.js"></script>
<script src="/fr/js/jquery.flexslider-min.js"></script>
<script src="/fr/js/superfish.pack.1.4.1.js"></script>
<script src="/fr/js/jquery.slicknav.min.js"></script>
<script src="/fr/js/jquery.prettyPhoto.js"></script>
<script src="/fr/js/bootstrap-datepicker.js"></script>
<script src="/fr/js/selectordie.min.js"></script>
<script src="/fr/js/jquery.parallax-1.1.3.js"></script>
<script src="/fr/js/jquery.simpleWeather.min.js"></script>
<script src="/fr/js/main.js"></script>
<!--
<script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src='//www.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
-->
</body>
</html>