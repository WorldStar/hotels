<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Categorie des Chambres | Ihusi Hotel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="/fr/img/favicon.ico" />
    <!-- CSS FILES -->
    <link rel="stylesheet" href="/fr/css/bootstrap.min.css">
    <link rel="stylesheet" href="/fr/css/flexslider.css">
    <link rel="stylesheet" href="/fr/css/prettyPhoto.css">
    <link rel="stylesheet" href="/fr/css/datepicker.css">
    <link rel="stylesheet" href="/fr/css/selectordie.css">
    <link rel="stylesheet" href="/fr/css/main.css">
    <link rel="stylesheet" href="/fr/css/2035.responsive.css">

    <script src="/fr/js/vendor/modernizr-2.8.3-respond-1.1.0.min.js"></script>
    <!-- Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/fr/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="boxed">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<div id="wrapper" class="container">
    <div class="header"><!-- Header Section -->
        <div class="pre-header"><!-- Pre-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left pre-address-b"><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, GOMA - Congo (DRC)</p></div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <ul class="pre-link-box">
                                <li><a href="/fr/about.html">A propos de nous</a></li>
                                <li><a href="/fr/contact.html">Contact</a></li>
                            </ul>
                        </div>
                        <div class="pull-right">
                            <div class="language-box">
                                <ul>
                                    <li><a href="/francais"><img alt="language" src="/fr/temp/france.png"><span class="language-text">FRANÇAIS</span></a></li>
                                    <li><a href="/"><img alt="language" src="/fr/temp/english.png"><span class="language-text">ENGLISH</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header"><!-- Main-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left">
                        <div class="logo">
                            <a href="/francais"><img alt="Logo" src="img/logo.png" width="100" class="img-responsive" /></a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <nav class="nav">
                                <ul id="navigate" class="sf-menu navigate">
                                    <li><a href="/francais">ACCUEIL</a></li>
                                    <!--<li class="parent-menu"><a href="#">FEATURES</a>
                                        <ul>
                                            <li><a href="#">2 Homepages</a></li>
                                            <li><a href="#">Ajax/PHP Booking Form</a></li>
                                            <li><a href="#">Ultra Responsive</a></li>
                                            <li><a href="under-construction.html">Countdown Page</a></li>
                                            <li><a href="#">2 Category Pages</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <!--<li class="parent-menu"><a href="#">PAGES</a>
                                        <ul>
                                            <li><a href="about.html">About</a></li>
                                            <li><a href="category-grid.html">Category Grid</a></li>
                                            <li><a href="category-list.html">Category List</a></li>
                                            <li><a href="room-single.html">Room Details</a></li>
                                            <li><a href="reservation-form-dark.html">Dark Reservation Form</a></li>
                                            <li><a href="reservation-form-light.html">Light Reservation Form</a></li>
                                            <li><a href="gallery.html">Gallery</a></li>
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog-details.html">Blog Single</a></li>
                                            <li><a href="left-sidebar-page.html">Left Sidebar Page</a></li>
                                            <li><a href="right-sidebar-page.html">Right Sidebar Page</a></li>
                                            <li><a href="under-construction.html">Under Construction</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <li class="active"><a href="/francais/categorylist">CHAMBRES</a></li>
                                    <li><a href="/fr/event.html">REUNION & EVENEMENTS</a></li>
                                    <li><a href="/fr/gallery.html">GALLERIE PHOTO</a></li>
                                    <li><a href="/fr/contact.html">CONTACT</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right">
                            <div class="button-style-1 margint45">
                                <a href="/francais/reservation_form_dark"><i class="fa fa-calendar"></i>RESERVATION</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb breadcrumb-1 pos-center">
        <h1>CATEGORIE DE CHAMBRES</h1>
    </div>
    <div class="content"><!-- Content Section -->
        <div class="container margint60">
            <div class="row">
                <div class="col-lg-12 marginb20"><!-- Room Sort -->
                    <div class="sortby clearfix">
                        <div class="pull-left">
                            <select>
                                <option selected="selected">ASCENDANT</option>
                                <option>DESCENDANT</option>
                            </select>
                        </div>
                        <div class="pull-right sort-icon">
                            <!--<a class="fright" href="category-grid.html"><img src="/fr/temp/grid-icon.png" alt=""></a>
                            <a class="fright" href="category-list.html"><img src="/fr/temp/list-icon.png" alt=""></a>-->
                        </div>
                    </div>
                </div>
                <div class="col-lg-9"><!-- Explore Rooms -->

                    <table>
                        <tr class="products-title">
                            <td class="table-products-image pos-center"><h6>IMAGE</h6></td>
                            <td class="table-products-name pos-center"><h6>CATEGORIE</h6></td>
                            <td class="table-products-price pos-center"><h6>PRIX</h6></td>
                            <td class="table-products-total pos-center"><h6>ACHETER</h6></td>
                        </tr>
                        <?php
                        $categories = DB::table('hotel_categories as c')
                                ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
                                ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
                                ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
                                ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
                                ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
                                ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
                                ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
                                ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
                                ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
                                ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.frname as balname', 'a.frname as airname', 'be.frname as bedname', 'd.frname as deskname', 'p.photo', 's.frname as safename', 't.frname as tvname', 'w.frname as wname'])->orderby('c.id', 'asc')->get();
                        foreach($categories as $category){
                            ?>
                        <tr class="table-products-list pos-center">
                            <td class="products-image-table"><img alt="Products Image 1" src="/uploads/files/{{ $category->photo }}" class="img-responsive"></td>
                            <td class="title-table">
                                <div class="room-details-list clearfix">
                                    <div class="pull-left">
                                        <h5>{{ $category->frname }}</h5>
                                    </div>
                                    <div class="pull-left room-rating">
                                        <ul>
                                            <?php if($category->stars == 1){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php }else if($category->stars == 2){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php }else if($category->stars == 3){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php }else if($category->stars == 4){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php }else if($category->stars == 5){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <?php }else{ ?>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="list-room-icons clearfix">
                                    <ul>
                                        @if($category->opt_calendar == 1)
                                            <li><i class="fa fa-calendar"></i></li>
                                        @endif
                                        @if($category->opt_wifi == 1)
                                            <li><i class="fa fa-flask"></i></li>
                                        @endif
                                        @if($category->opt_balconies != 0)
                                            <li><i class="fa fa-umbrella"></i></li>
                                        @endif
                                        @if($category->opt_tv != 1)
                                            <li><i class="fa fa-laptop"></i></li>
                                        @endif
                                    </ul>
                                </div>
                                <p align="justify">{{ $category->frdesc }}</p>
                            </td>
                            <td><h3>{{ $category->price }}$</h3></td>
                            <td><div class="button-style-1"><a href="/francais/catitem/{{ $category->id }}"><i class="fa fa-calendar"></i><span class="mobile-visibility">DETAILS</span></a></div></td>
                        </tr>
                        <?php }?>

                    </table>
                </div>
                <div class="col-lg-3"><!-- Sidebar -->
                    <div class="quick-reservation-container">
                        <div class="quick-reservation clearfix">
                            <div class="title-quick pos-center margint30">
                                <h5>RESERVATION RAPIDE</h5>
                                <div class="line"></div>
                            </div>
                            <div class="reserve-form-area">
                                <form action="#" method="post" id="ajax-reservation-form">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <label>ARRIVE</label>
                                    <input type="text" id="dpd1" name="dpd1" class="date-selector" placeholder="&#xf073;" />
                                    <label>DEPART</label>
                                    <input type="text" id="dpd2" name="dpd2" class="date-selector" placeholder="&#xf073;" />
                                    <div class="pull-left children clearfix">
                                        <label>CHAMBRES</label>
                                        <select name="rooms" class="pretty-select">
                                            <option selected="selected" value="1" >1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                    <div class="pull-left type clearfix">
                                        <label>ADULTE(S)</label>
                                        <select name="adult" class="pretty-select">
                                            <option selected="selected" value="1" >1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                    <div class="pull-left rooms clearfix">
                                        <label>ENFANT(S)</label>
                                        <select name="children" class="pretty-select">
                                            <option selected="selected" value="0" >0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                    <div class="pull-left search-button clearfix">
                                        <div class="button-style-1">
                                            <a id="res-submit" href="#">CHERCHER</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="luxen-widget news-widget">
                        <div class="title-quick marginb20">
                            <h5>INFORMATION UTILES</h5>
                        </div>
                        <p align="justify">Le Parc national de Virunga est l'une des zones les plus biologiquement diversifiées et protégées de la planète. La moitié de la biodiversité en Afrique sub-saharienne se trouve dans Virunga. Le parc est également une merveille géologique et contient deux des volcans les plus actifs du monde. Pour une grande partie de sa longue histoire, bien que, le parc national de Virunga a été gravement menacée par les conflits armés. Merci au dévouement des gardes forestiers et les gardes du parc, Virunga a été en mesure de survivre. Certains hommes politiques, l'Union européenne, les écologistes, les philanthropes et les donateurs privés ont également joué un rôle vital dans la survie de Virunga.</p><a class="active-color" href="https://visitvirunga.org/about-virunga/" target="_blank">[EN SAVOIR PLUS]</a>
                        <center><img src="/fr/temp/1.png" width="260"></center>
                    </div>
                    <div class="luxen-widget news-widget">
                        <div class="title">
                            <h5>CONTACT</h5>
                        </div>
                        <ul class="footer-links">
                            <li>Visitez le Parc national des Virunga et le Nyiragongo Montagne

                                <br>Boulevard Kanya Mulanga
                                <br>Goma, DRC
                                <br><br><i class="fa fa-envelope"></i><a href="mailto:visit@virunga.org">visit@virunga.org</a>
                                <br><i class="fa fa-phone"></i>+243 99 1715401</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer margint40"><!-- Footer Section -->
        <div class="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-sm-2 footer-logo">
                        <img alt="Logo" src="img/logo.png" class="img-responsive" >
                    </div>
                    <div class="col-lg-10 col-sm-10">
                        <div class="col-lg-3 col-sm-3">
                            <h6>DECOUVREZ IEXPRESS</h6>
                            <ul class="footer-links">
                                <li>Réservez et payez vos billets de bateaux en ligne en utilisant notre application web et mobile IExpress nouvellement conçu. Cliquez<a href="http://iexpress.ihusigroups.com" target="_blank"> ICI</a> pour découvrir l'application.<br><br><img src="/fr/temp/logo.png" width="150"></li>
                                <li>Disponible pour<br><img src="/fr/temp/android.png"></li>
                                <li><p><img src="/fr/temp/ios.png"></p></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>ACTIVITES À FAIRE PENDANT VOTRE SEJOUR (SERVICE EXTÉRIEUR)</h6>
                            <ul class="footer-links">
                                <li>Visitez le Parc national des Virunga et le Nyiragongo Montagne
                                    <br><img src="/fr/temp/1.png" width="150"></a><br>Bureau de Tourisme Parc national de Virunga
                                    <br>Boulevard Kanya Mulanga
                                    <br>Goma, DRC

                                    <br><br><i class="fa fa-envelope"></i><a href="mailto:visit@virunga.org">visit@virunga.org</a>
                                    <br><i class="fa fa-phone"></i>+243 99 1715401</li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>PAGES DU SITE</h6>
                            <ul class="footer-links">
                                <li><a href="/fr/contact.html">Contact</a></li>
                                <li><a href="/fr/gallery.html">Gallerie</a></li>
                                <li><a href="/francais/categorylist">Chambres</a></li>
                                <br><br>
                                <li>
                                    <div class="newsletter-form margint40">
                                        <div class="newsletter-wrapper">
                                            <div class="pull-left">
                                                <h2><i class="fa fa-shopping-cart"></i><b>Ihusi Shop</b></h2>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>Acheter des souvenirs à la boutique Ihusi pendant votre séjour.<br> Veuillez nous rendre visite.
                                <li><i class="fa fa-envelope"></i><a href="mailto:ishop@ihusigroups.com">ishop@ihusigroups.com</a>
                                    <br><i class="fa fa-phone"></i>+243 99 0000000
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>CONTACT</h6>
                            <ul class="footer-links">
                                <li><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, Congo (DRC)</p></li>
                                <li><p><i class="fa fa-phone"></i> +243(0) 81 31 29 560 </p></li>
                                <li><p><i class="fa fa-envelope"></i> <a href="mailto:info@2035themes.com">ihusihotel@ihusigroups.com</a></p></li>
                                <li><br><br><b>TRIP ADVISOR AWARDED</b></li>
                                <li><img src="/fr/temp/ta.jpg" width="80">&nbsp;&nbsp;<img src="/fr/temp/tatc.jpg" width="80"></li>
                                <li><br></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pre-footer">
            <div class="container">
                <div class="row">
                    <div class="pull-left"><p>© IHUSI HOTEL 2016</p></div>
                    <div class="pull-right">
                        <ul>
                            <li><p>DEVELOPPER PAR aXiom INVENT</p></li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- JS FILES -->
<script src="/fr/js/vendor/jquery-1.11.1.min.js"></script>
<script src="/fr/js/vendor/bootstrap.min.js"></script>
<script src="/fr/js/retina-1.1.0.min.js"></script>
<script src="/fr/js/jquery.flexslider-min.js"></script>
<script src="/fr/js/superfish.pack.1.4.1.js"></script>
<script src="/fr/js/jquery.slicknav.min.js"></script>
<script src="/fr/js/jquery.prettyPhoto.js"></script>
<script src="/fr/js/bootstrap-datepicker.js"></script>
<script src="/fr/js/selectordie.min.js"></script>
<script src="/fr/js/jquery.parallax-1.1.3.js"></script>
<script src="/fr/js/main.js"></script>
<!--
<script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src='//www.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
-->
</body>
</html>