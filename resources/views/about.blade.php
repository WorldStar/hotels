<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>About | Ihusi Hotel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="img/favicon.ico" />
    <!-- CSS FILES -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/prettyPhoto.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/2035.responsive.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.1.0.min.js"></script>
    <!-- Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="boxed">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<div id="wrapper" class="container">
    <div class="header"><!-- Header Section -->
        <div class="pre-header"><!-- Pre-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left pre-address-b"><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, GOMA - Congo (DRC)</p></div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <ul class="pre-link-box">
                                <li><a href="/about">About</a></li>
                                <li><a href="/contact">Contact</a></li>
                            </ul>
                        </div>
                        <div class="pull-right">
                            <div class="language-box">
                                <ul>
                                    <li><a href="/"><img alt="language" src="temp/english.png"><span class="language-text">ENGLISH</span></a></li>
                                    <li><a href="/francais"><img alt="language" src="temp/france.png"><span class="language-text">FRANÇAIS</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header"><!-- Main-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left">
                        <div class="logo">
                            <a href="/"><img alt="Logo" src="img/logo.png" width="100" class="img-responsive" /></a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <nav class="nav">
                                <ul id="navigate" class="sf-menu navigate">
                                    <li class="active"><a href="/">HOMEPAGE</a></li>
                                    <!--<li class="parent-menu"><a href="#">FEATURES</a>
                                        <ul>
                                            <li><a href="#">2 Homepages</a></li>
                                            <li><a href="#">Ajax/PHP Booking Form</a></li>
                                            <li><a href="#">Ultra Responsive</a></li>
                                            <li><a href="under-construction.html">Countdown Page</a></li>
                                            <li><a href="#">2 Category Pages</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <!--<li class="parent-menu"><a href="#">PAGES</a>
                                        <ul>
                                            <li><a href="about.html">About</a></li>
                                            <li><a href="category-grid.html">Category Grid</a></li>
                                            <li><a href="category-list.html">Category List</a></li>
                                            <li><a href="room-single.html">Room Details</a></li>
                                            <li><a href="reservation-form-dark.html">Dark Reservation Form</a></li>
                                            <li><a href="reservation-form-light.html">Light Reservation Form</a></li>
                                            <li><a href="gallery.html">Gallery</a></li>
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog-details.html">Blog Single</a></li>
                                            <li><a href="left-sidebar-page.html">Left Sidebar Page</a></li>
                                            <li><a href="right-sidebar-page.html">Right Sidebar Page</a></li>
                                            <li><a href="under-construction.html">Under Construction</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <li><a href="/categorylist">ROOMS</a></li>
                                    <li><a href="/event">MEETINGS & EVENTS</a></li>
                                    <li><a href="/gallery">GALLERY</a></li>
                                    <li><a href="/contact">CONTACT</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right">
                            <div class="button-style-1 margint45">
                                <a href="/reservation_form_dark"><i class="fa fa-calendar"></i>BOOK NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb breadcrumb-1 pos-center">
        <h1>ABOUT THE HOTEL</h1>
    </div>
    <div class="content"><!-- Content Section -->
        <div class="container">
            <div class="row">
                <div class="about-slider margint40"><!-- About Slider -->
                    <div class="col-lg-12">
                        <div class="flexslider">
                            <ul class="slides">
                                <li><img alt="Slider 1" class="img-responsive" src="temp/about/1.jpg" /></li>
                                <li><img alt="Slider 1" class="img-responsive" src="temp/about/2.jpg" /></li>
                                <li><img alt="Slider 3" class="img-responsive" src="temp/about/3.jpg" /></li>
                                <li><img alt="Slider 3" class="img-responsive" src="temp/slide/1.jpg" /></li>
                                <li><img alt="Slider 3" class="img-responsive" src="temp/slide/2.jpg" /></li>
                                <li><img alt="Slider 3" class="img-responsive" src="temp/slide/3.jpg" /></li>
                                <li><img alt="Slider 3" class="img-responsive" src="temp/slide/4.jpg" /></li>
                                <li><img alt="Slider 3" class="img-responsive" src="temp/slide/5.jpg" /></li>
                                <li><img alt="Slider 3" class="img-responsive" src="temp/slide/6.jpg" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="about-info clearfix"><!-- About Info -->
                    <div class="col-lg-8">
                        <h3>ABOUT US</h3>
                        <p class="margint30">Ihusi Hotel is a four-star hotel located in Goma, the touristic capital of the Democratic republic of Congo, in the province of North-Kivu. Located at the edge of Kivu Lake, Ihusi Hotel offers a breath-taking view on the mountain ranges of the famous park of Virunga the second world heritage of UNESCO.</p>

                        <p>Ihusi Hotel is located 6 km from Goma International Airport and 2 km away from the main NGOs district.</p>

                        <p>With its 14-year existence, the four-star property is designed for the higher-level corporate guests or tourists. The hotel operates under the slogan “feel at home”, so coined because in providing comfortable environment and warm welcome, Ihusi Hotel is simply the in the region. The hotel also features 72 rooms and a full spa for your pleasure.
                        </p>
                    </div>
                    <div class="col-lg-4">
                        <h3>OVERVIEW</h3>
                        <div id="accordion" class="margint30">
                            <div class="panel panel-luxen active-panel">
                                <div class="panel-style active">
                                    <h4><span class="plus-box"><i class="fa fa-angle-up"></i></span> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">ACCOMODATION</a></h4>
                                </div>
                                <div id="collapse1" class="collapse collapse-luxen in">
                                    <div class="padt20">
                                        <p>The hotel features 72 elegant rooms of various categories for all the types of stays. Most of our rooms are provided with private terraces, which give views over the lake, or the gardens, furnished in a modern style, and equipped with DSTV and an Internet connection. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-luxen">
                                <div class="panel-style">
                                    <h4><span class="plus-box"><i class="fa fa-angle-down"></i></span> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse2">PACKAGES & SPECIALS</a></h4>
                                </div>
                                <div id="collapse2" class="collapse collapse-luxen">
                                    <div class="padt20">
                                        <p>All rooms’ prices are breakfast inclusive and corporates are exempted with taxes. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-luxen">
                                <div class="panel-style">
                                    <h4><span class="plus-box"><i class="fa fa-angle-down"></i></span> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse3">DINNING</a></h4>
                                </div>
                                <div id="collapse3" class="collapse collapse-luxen">
                                    <div class="padt20">
                                        <p>Spend your dining experiences at the best restaurant, combining a blend of the world's gourmet flavors.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-services margint60 clearfix"><!-- About Services -->
                    <div class="col-lg-4 col-sm-6">
                        <img alt="About Services" class="img-responsive" src="temp/spa.jpg" >
                        <h5 class="margint20">SPA CENTER</h5>
                        <p class="margint20" align="justify">The health club has ultimate pampering session with rejuvenating massages and a variety of soothing spa treatment rooms which offer holistic well-being services including massages, facials, body brushing, aromatheraphy.</p>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <img alt="About Services" class="img-responsive" src="temp/fitness.png" >
                        <h5 class="margint20">FITNESS CENTER</h5>
                        <p class="margint20">During your stay at Ihusi Hotel, there is no need to let your fitness regime slip - unless you want of course. Ihusi Hotel has an outdoor pool, 11*9 meters, all guests and fitness members have free access to the swimming pool and the fitness area. </p>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <img alt="About Services" class="img-responsive" src="temp/food.jpg" >
                        <h5 class="margint20">DELICIOUS FOOD</h5>
                        <p class="margint20">The kitchen team at Ihusi Hotel have numerous dining offers available for you throughout the year. The freshest seasonal produce is used to create these offers.</p>

                        <p>For more information on any of our offers, please contact +243(0) 81 31 29 560</p>
                    </div>
                </div>
                <div class="about-destination margint40 marginb40 clearfix"><!-- About Destination -->
                    <div class="title pos-center marginb40">
                        <h2>Our Breakfast Service</h2>
                        <div class="title-shape"><img alt="Shape" src="img/shape.png"></div>
                    </div>
                    <div class="col-lg-8 col-sm-12 about-title pos-center">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1">
                                <img src="temp/maps.jpg" alt="" class="img-responsive" />
                            </div>
                            <div class="tab-pane fade" id="tab2">
                                <img src="temp/maps.jpg" alt="" class="img-responsive" />
                            </div>
                            <div class="tab-pane fade" id="tab3">
                                <img src="temp/maps.jpg" alt="" class="img-responsive" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12 tabbed-area tab-style">
                        <div class="about-destination-box active-tab">
                            <a href="#tab1"><h6>Good breakfast buffet</h6></a>
                            <!--<a href="#tab1">--><p class="margint10">Whether morning or not, simply enjoy coming enjoy our continental breakfast in our restaurant or on the balcony overlooking the lake. Any hold in a friendly atmosphere.</p>

                            <p>Rather than be sweet or savory, no need to choose, we offer both!</p>

                            <p>Hotel Ihusi teams are at your service to prepare a gourmet breakfast with hot and cold varied dishes that will satisfy all tastes.
                            </p>
                            <p>Before starting voting day refueling forces with the formula "buffet" offering a variety of sweet products, salty and hot and cold drinks.
                                <br>The rich continental breakfast is served daily from 7:00 to 10:30.
                                <br>• Hot drinks: Tea, coffee, milk, hot chocolate.
                                <br>• Cold Beverages: Fruit juices and mineral waters</p>

                            <p>Assorted breads and pastries, cereals, jam, organic honey, spreads, butter, fresh fruit, cheeses, meats, vegetables, dried fruit, scrambled eggs or fresh map, sausages, bacon.</p>

                            <p>Buffet breakfast extra: $ 15 per person</p>

                            <p>For the lazy, you choose to serve breakfast directly in your room.</p>

                        </div>
                        <!--<div class="about-destination-box">
                            <a href="#tab2"><h6>BUS DESTINATION</h6></a>
                            <a href="#tab2"><p class="margint10">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere erat .Do gravida at eget metus.</p></a>
                        </div>
                        <div class="about-destination-box">
                            <a href="#tab3"><h6>OWN CAR</h6></a>
                            <a href="#tab3"><p class="margint10">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere era at eget metus.</p></a>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer margint40"><!-- Footer Section -->
        <div class="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-sm-2 footer-logo">
                        <img alt="Logo" src="img/logo.png" class="img-responsive" >
                    </div>
                    <div class="col-lg-10 col-sm-10">
                        <div class="col-lg-3 col-sm-3">
                            <h6>DISCOVER IEXPRESS</h6>
                            <ul class="footer-links">
                                <li>Book and pay for your boats tickets online by using our newly designed Ihusi Express Web App. Click <a href="http://iexpress.ihusigroups.com" target="_blank">HERE</a> to discover the app.<br><br><img src="temp/logo.png" width="150"></li>
                                <li>Available for<br><img src="temp/android.png"></li>
                                <li><p><img src="temp/ios.png"></p></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>THINGS TO DO (OUTSIDE SERVICE)</h6>
                            <ul class="footer-links">
                                <li>Visit the Virunga National Park and The Nyiragongo Mount<br><img src="temp/1.png" width="150"></a><br>Virunga National Park Tourism Office
                                    <br>Boulevard Kanya Mulanga
                                    <br>Goma, DRC

                                    <br><br><i class="fa fa-envelope"></i><a href="mailto:visit@virunga.org">visit@virunga.org</a>
                                    <br><i class="fa fa-phone"></i>+243 99 1715401</li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>PAGES SITE</h6>
                            <ul class="footer-links">
                                <li><a href="/contact">Contact</a></li>
                                <li><a href="/gallery">Gallery</a></li>
                                <li><a href="/categorylist">Rooms</a></li>
                                <br><br>
                                <li>
                                    <div class="newsletter-form margint40">
                                        <div class="newsletter-wrapper">
                                            <div class="pull-left">
                                                <h2><i class="fa fa-shopping-cart"></i><b>Ihusi Shop</b></h2>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>Buy souvenirs at the Ihusi Shop during your stay. We are top on fashion! <br>Please visit us.
                                <li><i class="fa fa-envelope"></i><a href="mailto:ishop@ihusigroups.com">ishop@ihusigroups.com</a>
                                    <br><i class="fa fa-phone"></i>+243 99 0000000
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <h6>CONTACT</h6>
                            <ul class="footer-links">
                                <li><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, Congo (DRC)</p></li>
                                <li><p><i class="fa fa-phone"></i> +243(0) 81 31 29 560 </p></li>
                                <li><p><i class="fa fa-envelope"></i> <a href="mailto:info@2035themes.com">ihusihotel@ihusigroups.com</a></p></li>
                                <li><br><br><b>TRIP ADVISOR AWARDED</b></li>
                                <li><img src="temp/ta.jpg" width="80">&nbsp;&nbsp;<img src="temp/tatc.jpg" width="80"></li>
                                <li><br></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pre-footer">
            <div class="container">
                <div class="row">
                    <div class="pull-left"><p>© IHUSI HOTEL 2016</p></div>
                    <div class="pull-right">
                        <ul>
                            <li><p>DESIGNED BY aXiom INVENT</p></li>
                            <li><a><img alt="axiom" src="temp/orkut.png" ></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- JS FILES -->
<script src="js/vendor/jquery-1.11.1.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/retina-1.1.0.min.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/superfish.pack.1.4.1.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/main.js"></script>
<!--
<script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src='//www.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
-->
</body>
</html>