@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Tickets
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Tickets</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">tickets</a>
            </li>
            <li class="active" >paid tickets</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                                    <span style="font-size: 110%">
                                        <i class="livicon" data-name="address-book" data-c="#26d32d" data-hc="#26d32d" data-size="12"
                                           data-loop="true"></i>
                                        Paid Tickets
                                    </span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div id="sample_editable_1_wrapper" class="">
                            <input type="hidden" id="cat_id" value="">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="paidtable" role="grid">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc"  tabindex="0" aria-controls="paidtable" rowspan="1" te
                                        colspan="1"  aria-label="
                                                   ID
                                            : activate to sort column ascending" style="width: 30px;">ID
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Product Name
                                            : activate to sort column ascending" style="width: 100px;"> Product
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                                        colspan="1" aria-label="
                                                Customer Name
                                            : activate to sort column ascending" style="width: 100px;">Customer
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                                        colspan="1" aria-label="
                                                Shipper
                                            : activate to sort column ascending" style="width: 100px;">Shipper
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Description
                                            : activate to sort column ascending" style="width: 300px;">Description
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                                        colspan="1" aria-label="
                                                Total Price
                                            : activate to sort column ascending" style="width: 80px;">Total Price
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Amount
                                            : activate to sort column ascending" style="width: 30px;">Amount
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Created Date
                                            : activate to sort column ascending" style="width:100px;">Created Date
                                    </th>
                                    <?php if($_SESSION['userrole'] == 3) {?>
                                    <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                                        colspan="1" aria-label="
                                                 Cancel
                                            : activate to sort column ascending" style="width: 70px;">Change to
                                    </th>
                                    <?php }?>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->

    <!-- cancel modal -->
    <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 80%": >
                <div class="modal-body" style="font-size: 150%; background-color: #f64747">
                    Notice!!!
                </div>
                <input type="hidden" id="orderid" value="0">
                <input type="hidden" id="state" value="0">
                <div class="modal-body" style="font-size: 130%" id="modalcontent">
                    Do you really want to cancel the Payment?
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="modal_order_id" value=""/>
                    <input type="hidden" id="modal_cancel_accept" value=""/>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="updatestate()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 80%">
                <div class="modal-body" style="font-size: 150%;  background-color: #2ca02c">
                    Notice!!!
                </div>
                <div class="modal-body" style="font-size: 130%" id="messagecontent">
                    Successfully cancelled the Paid Ticket..
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>

    <script>
        // display table by using Datatable method
        var table = null;
        $(function () {
            var nEditing = null;
            var table = $('#paidtable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("/admin/ticket/getpaid") !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'product_id', name: 'product_id'},
                    {data: 'customer_id', name: 'customer_id'},
                    {data: 'ship_id', name: 'ship_id'},
                    {data: 'description', name: 'description'},
                    {data: 'totalprice', name: 'totalprice'},
                    {data: 'amount', name: 'amount'},
                    {data: 'created_at', name: 'created_at'},
                   <?php if ($_SESSION['userrole'] == 3) {?>
                    {data: 'state', name: 'state', orderable: false, searchable: false}
                   <?php }?>
                ]
            });

            table.on('draw', function () {
                console.log('asaa');
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });
        })

        function cancelModal(id){
            $("#modal_order_id").val(id);
           // $("#modal_cancel_accept").val(num)
                $('#modalcontent').html("Do you really want to cancel the ticket?");
                $('#cancelModal').modal('show');
            }
        function updatestate() {
            //var num = $("#modal_cancel_accept").val();
            var id = $("#modal_order_id").val();
            var state;
            state = 0;
            var data = {
                id:id,
                state: state
            };
            $.ajax({
                type: "get",
                url: '/admin/ticket/update',
                data: data,
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (result) {
                }
            });
        }
    </script>
@stop
