@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
    Details
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Details</h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">tickets</a>
            </li>
            <li>
                <a>details</a>
            </li>
        </ol>
    </section>

    {{-- order information--}}
    <div class="panel-body" style="width:70%;margin-left:15%">
        <form role="form"  enctype="multipart/form-data">
            <div>
                <div class="panel-body" style="width:90%;margin-left:5%">
                    <div class="form-group has-success">
                        <label class="control-label" style="color: #f64747; font-size:25px;">Ticket Information</label>
                    </div>
                </div>
                <div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="name">Product</label>
                        <?php
                        $product = DB::table('occ_products')->where('id', $ticket->product_id)->first();
                        $productname = '';
                        if (!empty($product)) {
                             $productname = $product->name;
                            }
                        ?>
                        <input type="text" class="form-control" id="name" name="name" disabled value="{{ $productname }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="name">Description:</label>
                        <input type="text" class="form-control" id="description" name="description" disabled value="{{ $ticket->description }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="gender">Amount:</label>
                        <input type="text" class="form-control" id="amount" name="amount" disabled value="{{ $ticket->amount }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="birthday">Total Price:</label>
                        <input type="text" class="form-control" id="totalprice" name="totalprice" disabled value="{{ $ticket->totalprice }}">
                    </div>
                </div>
            </div>
            {{--customer information--}}
            <div>
                <div class="panel-body" style="width:90%;margin-left:5%">
                    <div class="form-group has-success">
                        <label class="control-label" style="color: #f64747; font-size:25px;">Customer Information</label>
                    </div>
                </div>
                <div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <?php
                        $user = DB::table('users')->where('id', 2)->first();
                        $firstname = '';
                        'firstname' == !empty($user->first_name)?  $firstname =  ($user->first_name): 'not_sure';
                        $lastname = '';
                        'lastname' == !empty($user->last_name)?  $lastname =  ($user->last_name): 'not_sure';
                        ?>
                        <label class="control-label" for="name">First Name</label>
                        <input type="text" class="form-control" id="name" name="name" disabled value="{{ $firstname }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="lastname">Last Name</label>
                        <input type="text" class="form-control" id="lastname" name="lastname" disabled value="{{  $lastname }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="email">User Email</label>
                        <input type="text" class="form-control" id="email" name="email" disabled value="{{ $user->email }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $user->address }}">
                    </div>
                </div>
            </div>

            {{--shipper information--}}
            <div>
                <div class="panel-body" style="width:90%;margin-left:5%">
                    <div class="form-group has-success">
                        <label class="control-label" style="color: #f64747; font-size:25px;">Shipper Information</label>
                    </div>
                </div>
                <div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <?php
                        $shipper = DB::table('occ_shipper_contacts')->where('id', $ticket->ship_id)->first();
                        ?>
                        <label class="control-label" for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" disabled value="{{ $shipper->name}}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="country">Age</label>
                        <input type="text" class="form-control" id="age" name="age" disabled value="{{ $shipper->age}}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="birthday">Address</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $shipper->address}}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="country">Company</label>
                        <input type="text" class="form-control" id="company" name="company" disabled value="{{ $shipper->company}}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="city">Email</label>
                        <input type="text" class="form-control" id="email" name="email" disabled value="{{ $shipper->email }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="city">Phone Number</label>
                        <input type="text" class="form-control" id="contactno" name="contactno" disabled value="{{ $shipper->contactno }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="address">Created Date</label>
                        <input type="text" class="form-control" id="created_at" name="created_at" disabled value="{{ $shipper->created_at }}">
                    </div>
                </div>
            </div>

            <div class="col-md-12 mar-10"  style="width:50%;margin-left:25%">
                <div class="col-md-12 mar-10"  style="width:50%;margin-left:25%">
                    <input type="reset" value="Close" class="btn btn-success btn-block btn-md btn-responsive" onclick="onBack({{$ticket->id}})">
                </div>
            </div>
        </form>
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>

    <script>
       console.log($_SESSION['userrole']);
        function onBack(id){
           history.go(-1);

        }
    </script>

@stop
