@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Tickets
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Tickets</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">tickets</a>
            </li>
            <li class="active" >cancelled tickets</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                                    <span style="font-size: 110%">
                                      <i class="livicon" data-name="address-book" data-c="#26d32d" data-hc="#26d32d" data-size="12"></i>
                                        Cancelled Tickets
                                    </span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div id="sample_editable_1_wrapper" class="">
                            <input type="hidden" id="cat_id" value="">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="cancelledtable" role="grid">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="cancelledtable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   ID
                                            : activate to sort column ascending" style="width: 30px;">ID
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="cancelledtable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Product Name
                                            : activate to sort column ascending" style="width: 100px;"> Product Name
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="cancelledtable" rowspan="1"
                                        colspan="1" aria-label="
                                                Customer Name
                                            : activate to sort column ascending" style="width: 100px;">Customer Name
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="cancelledtable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Description
                                            : activate to sort column ascending" style="width: 300px;">Description
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="cancelledtable" rowspan="1"
                                        colspan="1" aria-label="
                                                Total Price
                                            : activate to sort column ascending" style="width: 50px;">Total Price
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="cancelledtable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Amount
                                            : activate to sort column ascending" style="width: 30px;">Amount
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="cancelledtable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Created Date
                                            : activate to sort column ascending" style="width:100px;">Created Date
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>

    <script>
        // display table by using Datatable method
        var table = null;
        $(function () {
            var nEditing = null;
            var table = $('#cancelledtable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("/admin/ticket/getcancelled") !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'product_id', name: 'product_id'},
                    {data: 'customer_id', name: 'customer_id'},
                    {data: 'description', name: 'description'},
                    {data: 'totalprice', name: 'totalprice'},
                    {data: 'amount', name: 'amount'},
                    {data: 'created_at', name: 'created_at'},
                ]
            });

            table.on('draw', function () {
                console.log('asaa');
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });
        })
    </script>
@stop
