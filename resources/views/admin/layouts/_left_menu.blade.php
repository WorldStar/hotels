<ul id="menu" class="page-sidebar-menu" style="margin-top:10px;">
       <li {!! (Request::is('admin') ? 'class="active"' : '') !!}>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                   data-loop="true"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>

        <li {!! (Request::is('admin/getcatgegory') || Request::is('admin/getcatgegory') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('/admin/getcategory') }}">
                <i class="livicon" data-name="list-ul" data-c="#418BCA" data-hc="#418BCA" data-size="18"
                   data-loop="true"></i>
                <span class="title">Categories</span>
            </a>
        </li>
        <li {!! (Request::is('admin/categorystatus') || Request::is('admin/categorystatus') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('/admin/categorystatus') }}">
                <i class="livicon" data-name="medal" data-c="#418BCA" data-hc="#418BCA" data-size="18"
                   data-loop="true"></i>
                <span class="title">Categories Status</span>
            </a>
        </li>

        <li {!! (Request::is('admin/reservation') || Request::is('admin/reservation') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="address-book" data-c="#6CC66C" data-hc="#6CC66C" data-size="18" data-loop="true"></i>
                <span class="title">Reservations</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/reservation') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/reservation') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Current Reservations
                    </a>
                </li>
                <li {!! (Request::is('admin/passingreservation') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/passingreservation') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Closing Reservations
                    </a>
                </li>
                <li {!! (Request::is('admin/passedreservation') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/passedreservation') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Closed Reservations
                    </a>
                </li>
                <li {!! (Request::is('admin/cancelreservation') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/cancelreservation') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Cancel Reservations
                    </a>
                </li>
            </ul>
        </li>
    <li {!! (Request::is('admin/customers') || Request::is('admin/customers') ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('/admin/customers') }}">
            <i class="livicon" data-name="user" data-c="#418BCA" data-hc="#418BCA" data-size="18"
               data-loop="true"></i>
            <span class="title">Customers</span>
        </a>
    </li>
        <!--<li {!! (Request::is('admin/charts') || Request::is('admin/piecharts') || Request::is('admin/charts_animation') || Request::is('admin/jscharts') || Request::is('admin/sparklinecharts') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="barchart" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                   data-loop="true"></i>
                <span class="title">Reports</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/charts') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/charts') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Flot Charts
                    </a>
                </li>
                <li {!! (Request::is('admin/piecharts') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/piecharts') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Pie Charts
                    </a>
                </li>
                <li {!! (Request::is('admin/charts_animation') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/charts_animation') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Animated Charts
                    </a>
                </li>
                <li {!! (Request::is('admin/jscharts') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/jscharts') }}">
                        <i class="fa fa-angle-double-right"></i>
                        JS Charts
                    </a>
                </li>
                <li {!! (Request::is('admin/sparklinecharts') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/sparklinecharts') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Sparkline Charts
                    </a>
                </li>
            </ul>
        </li>-->
        <li {!! (Request::is('admin/login') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/login') }}">
                <i class="livicon" data-name="sign-out" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C"
                   data-loop="true"></i>
                <span class="title">Log out</span>
            </a>
        </li>
        <!--
        <li {!! (Request::is('admin/datatables') || Request::is('admin/editable_datatables') || Request::is('admin/dropzone') || Request::is('admin/multiple_upload')? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/datatables') }}">
                    <i class="livicon" data-name="car" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                       data-loop="true"></i>
                    <span class="title">Shippers</span>
                </a>
            </li>
        -->
</ul>