@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Current Reservations
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <!--section starts-->
                <h1>Current Reservations</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('dashboard') }}">
                            <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                            Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="#">Reservations</a>
                    </li>
                    <li class="active">Current Reservations</li>
                </ol>
            </section>
            <!--section ends-->
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary filterable">
                            <div class="panel-heading clearfix  ">
                                <div class="panel-title pull-left">
                                    <div class="caption" style="font-size: 110%">
                                           <i class="livicon" data-name="notebook" data-c="#6CC66C" data-hc="#6CC66C" data-size="12"
                                              data-loop="true"></i>
                                        Current Reservations
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body table-responsive">
                                <table class="table table-striped table-bordered" id="table1">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Reservation ID</th>
                                            <th>Transaction ID</th>
                                            <th>Category Name</th>
                                            <th>Customer Name</th>
                                            <th>Number of Room</th>
                                            <th>Price (US $)</th>
                                            <th>Arrival</th>
                                            <th>Departure</th>
                                            <th>Adult</th>
                                            <th>Children</th>
                                            <th>Created Date</th>
                                            <th>&nbsp;</th>
                                            <!--<th>&nbsp;</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        //print_r($customers);
                                        $i = 0;
                                        foreach($reservations as $reservation){
                                        $i++;
                                        $added_date = date('M d, Y', strtotime($reservation->created_at));

                                    ?>
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $reservation->ticket_id }}</td>
                                        <td><a href="{{ url('/admin/gettransaction/'.$reservation->cat_id.'/'.$reservation->ticket_id) }}" style="text-decoration: none">{{ $reservation->transaction_id }}</a></td>
                                        <td><a href="{{ url('/admin/gettransaction/'.$reservation->cat_id.'/'.$reservation->ticket_id) }}" style="text-decoration: none">{{ $reservation->categoryname }}</a></td>
                                        <td><span style="cursor: pointer;color:#337ab7" onclick="viewCustomer('{{ $reservation->customername }}','{{ $reservation->surname }}','{{ $reservation->phone }}','{{ $reservation->email }}')">{{ $reservation->customername }}</span></td>
                                        <td>{{ $reservation->rooms }}</td>
                                        <td>{{ $reservation->price }}</td>
                                        <td>{{ $reservation->arrival_date }}</td>
                                        <td>{{ $reservation->departure_date }}</td>
                                        <td>{{ $reservation->adult }}</td>
                                        <td>{{ $reservation->children }}</td>
                                        <td>{{ $added_date }}</td>
                                        <td style="cursor:pointer" class="ianctive_button" onclick="preReservation('{{ $reservation->ticket_id }}')">Refund</td>
                                        <!--<td style="cursor:pointer" class="ianctive_button" onclick="deleteReservation('{{ $reservation->ticket_id }}')">Delete</td>-->

                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 80%">
            <input type="hidden" id="customerid" value="0">
            <input type="hidden" id="status" value="0">
            <div class="modal-body" id="inactivecontent">
                <input type="hidden" id="ticket_id" value="0">
                Do you really want to refund this reservation?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="cancelReservation()">Yes</button>
                <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 80%">
            <input type="hidden" id="customerid" value="0">
            <input type="hidden" id="status" value="0">
            <div class="modal-body" id="inactivecontent">
                Do you really want to delete this reservation?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="delReservation()">Yes</button>
                <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 80%">
            <div class="modal-header" style="font-size: 200%">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Customer </h4>
            </div>
            <div class="modal-body">
                <div class="form-group has-success" style="width:70%; margin-left:15%">
                    <label class="control-label" for="first_name">Customer Name</label>
                    <span type="text-align" class="form-control" id="name1"></span>
                </div>
                <div class="form-group has-success" style="width:70%; margin-left:15%">
                    <label class="control-label" for="first_name">Customer Surname</label>
                    <span type="text-align" class="form-control" id="surname"></span>
                </div>
                <div class="form-group has-success" style="width:70%; margin-left:15%">
                    <label class="control-label" for="first_name">Customer Phone</label>
                    <span type="text-align" class="form-control" id="phone"></span>
                </div>
                <div class="form-group has-success" style="width:70%; margin-left:15%">
                    <label class="control-label" for="first_name">Customer Email</label>
                    <span type="text-align" class="form-control" id="email"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<!-- active/inactive sfow-->
<style>
    .ianctive_button:hover {
        color:#0618d8;
    }
    .ianctive_button {
        color:  #d80b06
    }
</style>
    @stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script>
        function preReservation(ticket_id){
            $('#ticket_id').val(ticket_id);
            $("#cancelModal").modal('show');
        }
        function cancelReservation(){
            var ticket_id = $('#ticket_id').val();
            window.location = '/admin/reservation/cancel/'+ticket_id;
        }
        function deleteReservation(ticket_id){
            $('#ticket_id').val(ticket_id);
            $("#deleteModal").modal('show');
        }
        function delReservation(){
            var ticket_id = $('#ticket_id').val();
            window.location = '/admin/reservation/delete/'+ticket_id;
        }
        function viewCustomer(name, surname, phone, email){
            $('#name1').html(name);
            $('#surname').html(surname);
            $('#phone').html(phone);
            $('#email').html(email);
            $("#customerModal").modal('show');
        }
    </script>
@stop
