@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Current Categories Status
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Current Categories Status</h1>
       	<ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <!--<li>
                <a href="#">Categories</a>
            </li>-->
            <li class="active">Current Categories Status</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    
					<div class="panel-heading">
                        <h3 class="panel-title">
                                    <span style="font-size: 110%">
                                    <i class="livicon" data-c="#6CC66C" data-hc="#6CC66C" data-name="medal" data-size="12" data-loop="true"></i>
                                    Current Categories Status</span>
                        </h3>
                    </div>
					
                    <div class="panel-body"> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="categorytable" role="grid">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 5%;">Category ID
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 15%;">Category Name
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 15%;">Price (US $)/Day
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 20%;">Number of Rooms
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 8%;">Number of booked Rooms
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 10%;">Number of remain Rooms
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td style="text-align: center">{{ $category->cat_id }}</td>
                                            <td style="text-align: center">{{ $category->name }}</td>
                                            <td style="text-align: center">{{ $category->price }}</td>
                                            <td style="text-align: center">{{ $category->rooms }}</td>
                                            <td style="text-align: center">{{ $category->book_num }}</td>
                                            <td style="text-align: center">{{ $category->remain_num }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->



@stop

{{-- page level scripts --}}
@section('footer_scripts')


@stop
