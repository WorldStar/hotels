@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
    Categories
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <!--section starts-->
    <h1>Categories</h1>
    <ol class="breadcrumb">
        <li>
            <a href="">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                         Dashboard
            </a>
        </li>
        <li>
            <a href="#">Categories</a>
        </li>
        <li class="active">Categories</li>
    </ol>
</section>

<div class="panel-body" style="width:70%;margin-left:15%">
    <div class="form-group has-success">
        <label class="control-label" style="font-size:25px; color: #FF0000;">View Categories</label>
    </div>
    <div class="col-sm-12">
        @if(!empty($error))
        {!! $error !!}
        @endif
    </div>
    @if(!empty($success))
        <div class="alert alert-success alert-dismissable">
                {!! $success !!}
        </div>
    @endif
    <form role="form" action="{{ route('admin.category.store') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <div class="form-group has-success">
            <label class="control-label" style="font-size: 120%" for="name">Category Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="category name" value="{{ $category->name }}" disabled="disabled">
        </div>
        <div class="form-group has-success">
            <label class="control-label" style="font-size: 120%" for="name">Category Francais Name</label>
            <input type="text" class="form-control" id="frname" name="frname" placeholder="category name" value="{{ $category->frname }}" disabled="disabled">
        </div>
        <div class="form-group has-success">
            <label class="control-label" style="font-size: 120%" for="name">Category Title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="category title" value="{{ $category->title }}" disabled="disabled">
        </div>
        <div class="form-group has-success">
            <label class="control-label" style="font-size: 120%" for="name">Category Francais Title</label>
            <input type="text" class="form-control" id="frtitle" name="frtitle" placeholder="category title" value="{{ $category->frtitle }}" disabled="disabled">
        </div>
        <div class="form-group has-success">
            <label class="control-label" for="description" style="font-size: 120%">Category Description</label>
            <textarea class="form-control" id="desc" name="desc" placeholder="Please enter category description here..." rows="5" disabled="disabled">{{ $category->desc }}</textarea>
        </div>
        <div class="form-group has-success">
            <label class="control-label" for="description" style="font-size: 120%">Category Francais Description</label>
            <textarea class="form-control" id="frdesc" name="frdesc" placeholder="Please enter category description here..." rows="5" disabled="disabled">{{ $category->frdesc }}</textarea>
        </div>
        <div class="form-group has-success">
            <div  class="col-lg-4">
                <label class="control-label" for="amount" style="font-size: 120%">Number of Rooms</label>
                <input type="number" class="form-control" id="rooms" name="rooms" placeholder="number of rooms" value ="{{ $category->rooms }}" min="1" disabled="disabled">
            </div>
            <div class="col-lg-4">
                <label class="control-label" for="price" style="font-size: 120%">Category Price (US $)/Day</label>
                <input type="text" class="form-control" id="price" name="price" placeholder="category price" value="{{ $category->price }}" disabled="disabled">
            </div>
            <div  class="col-lg-4">
                <label class="control-label" for="amount" style="font-size: 120%">Category Stars</label>
                <input type="number" class="form-control" id="stars" name="stars" placeholder="category stars" value ="{{ $category->stars }}" min="1" max="5" disabled="disabled">
            </div>
        </div>
        <div class="form-group has-success" style="margin-top:90px;">
            <label class="control-label" for="photo" style="font-size: 120%">Category Photo</label><br>
            <img src="/uploads/files/{{ $photo->photo }}" style="max-height:300px;">
        </div>
        <div class="form-group has-success" style="margin-top:70px;">
            <hr style="color:#999;border-top: 1px solid #a55e5e;">
            <label class="control-label" for="photo" style="font-size: 120%">Category Options</label>
        </div>
        <div class="form-group has-success">
            <div class="col-lg-3">
                <input type="checkbox" id="opt_calendar" name="opt_calendar" style="padding-top:5px;width:15px;height:15px;" disabled="disabled" value="1" @if($option->opt_calendar)checked="checked"@endif> <span  class="control-label" style="margin-top:-5px;">CALENDAR</span>
            </div>
            <div class="col-lg-3">
                <input type="checkbox" id="opt_wifi" name="opt_wifi" style="padding-top:5px;width:15px;height:15px;" disabled="disabled" value="1" @if($option->opt_wifi)checked="checked"@endif> <span  class="control-label" style="margin-top:-5px;">WI-FI</span>
            </div>
            <div class="col-lg-3">
                <input type="checkbox" id="opt_service" name="opt_service" style="padding-top:5px;width:15px;height:15px;" disabled="disabled" value="1" @if($option->opt_service)checked="checked"@endif> <span  class="control-label" style="margin-top:-5px;">7/24 SERVICE</span>
            </div>
            <div class="col-lg-3">
                <input type="checkbox" id="opt_minibar" name="opt_minibar" style="padding-top:5px;width:15px;height:15px;" disabled="disabled" value="1" @if($option->opt_minibar)checked="checked"@endif> <span  class="control-label" style="margin-top:-5px;">MINI BAR</span>
            </div>
        </div>

        <div class="form-group has-success" style="margin-top:60px;">
            <div class="col-lg-3">
                <label class="control-label">BALCONIES</label>
                <select name="opt_balconies" id="opt_balconies" class="form-control" disabled="disabled">
                    <option value="0">SELECT</option>
                    <?php
                    foreach($balconies as $bal){
                        if($option->opt_balconies == $bal->id)
                            echo '<option value="'.$bal->id.'" selected>'.$bal->name.'</option>';
                        else
                            echo '<option value="'.$bal->id.'">'.$bal->name.'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3">
                <label class="control-label">BEDS</label>
                <select name="opt_bed" class="form-control" disabled="disabled">
                    <option value="0">SELECT</option>
                    <?php
                    foreach($beds as $bed){
                        if($option->opt_bed == $bed->id)
                            echo '<option value="'.$bed->id.'" selected>'.$bed->name.'</option>';
                        else
                            echo '<option value="'.$bed->id.'">'.$bed->name.'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3">
                <label class="control-label">DESKS</label>
                <select name="opt_desk" class="form-control" disabled="disabled">
                    <option value="0">SELECT</option>
                    <?php
                    foreach($desks as $desk){
                        if($option->opt_desk == $desk->id)
                            echo '<option value="'.$desk->id.'" selected>'.$desk->name.'</option>';
                        else
                            echo '<option value="'.$desk->id.'">'.$desk->name.'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3">
                <label class="control-label">SAFES</label>
                <select name="opt_safe" class="form-control" disabled="disabled">
                    <option value="0">SELECT</option>
                    <?php
                    foreach($safes as $safe){
                        if($option->opt_safe == $safe->id)
                            echo '<option value="'.$safe->id.'" selected>'.$safe->name.'</option>';
                        else
                            echo '<option value="'.$safe->id.'">'.$safe->name.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group has-success" style="margin-top:140px;">
            <div class="col-lg-4">
                <label class="control-label">TVS</label>
                <select name="opt_tv" class="form-control" disabled="disabled">
                    <option value="0">SELECT</option>
                    <?php
                    foreach($tvs as $tv){
                        if($option->opt_tv == $tv->id)
                            echo '<option value="'.$tv->id.'" selected>'.$tv->name.'</option>';
                        else
                            echo '<option value="'.$tv->id.'">'.$tv->name.'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-4">
                <label class="control-label">WARDROBES</label>
                <select name="opt_wardrobe" class="form-control" disabled="disabled">
                    <option value="0">SELECT</option>
                    <?php
                    foreach($wardrobes as $wordrobe){
                        if($option->opt_wardrobe == $wordrobe->id)
                            echo '<option value="'.$wordrobe->id.'" selected>'.$wordrobe->name.'</option>';
                        else
                            echo '<option value="'.$wordrobe->id.'">'.$wordrobe->name.'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-4">
                <label class="control-label">AIR CONDITIONNERS</label>
                <select name="opt_air" class="form-control" disabled="disabled">
                    <option value="0">SELECT</option>
                    <?php
                    foreach($airs as $air){
                        if($option->opt_air == $air->id)
                            echo '<option value="'.$air->id.'" selected>'.$air->name.'</option>';
                        else
                            echo '<option value="'.$air->id.'">'.$air->name.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group has-success" style="margin-top:210px">
            <div class="col-xs-6">
                <input type="checkbox" id="opt_table" name="opt_table" disabled="disabled" style="padding-top:5px;width:15px;height:15px;" value="1" @if($option->opt_table)checked="checked"@endif> <span  class="control-label" style="margin-top:-5px;">COFFEE TABLE</span>
            </div>
            <div class="col-xs-6">
                <input type="checkbox" id="opt_hair" name="opt_hair" disabled="disabled" style="padding-top:5px;width:15px;height:15px;" value="1" @if($option->opt_hair)checked="checked"@endif> <span  class="control-label" style="margin-top:-5px;">HAIR DRYER</span>
            </div>
        </div>


        <div class="col-md-12 mar-10" style="padding-top:30px;">
            <div class="col-xs-6 col-md-6">
                <input type="button" name="button" id="button" style="font-size: 120%" value="Edit Category" class="btn btn-primary btn-block btn-md btn-responsive" onclick="editCategory('{{ $category->id }}')">
            </div>
            <div class="col-xs-6 col-md-6">
                <input type="reset" value="Back" style="font-size: 120%" class="btn btn-success btn-block btn-md btn-responsive" onclick="onBack()">
            </div>
        </div>
    </form>
</div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
<script>
    function onBack(){
        window.location = "/admin/getcategory";
        //history.go(-1);
    }
    function editCategory(cat_id){
        window.location = "/admin/category/edit/"+cat_id;
        //history.go(-1);
    }
</script>

@stop
