@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Customers
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}"/>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Customers</h1>
       	<ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <!--<li>
                <a href="#">Categories</a>
            </li>-->
            <li class="active">Customers</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    
					<div class="panel-heading">
                        <h3 class="panel-title">
                                    <span style="font-size: 110%">
                                    <i class="livicon" data-c="#6CC66C" data-hc="#6CC66C" data-name="user" data-size="12" data-loop="true"></i>
                                    Customers</span>
                        </h3>
                    </div>
					
                    <div class="panel-body"> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="categorytable" role="grid">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 5%;">ID
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 15%;">Customer Name
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 15%;">Customer Surname
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 20%;">Phone
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 8%;">Email
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 10%;">Created Date
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->



@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/pages/table-editable.js') }}" ></script>--}}

    <script>

        /* display table by using Datatable method  */
        var table = null;
		$(function () {
			var nEditing = null;
            table = $('#categorytable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route("admin.customers.data") !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'surname', name: 'surname'},
                    {data: 'phone', name: 'phone'},
                    {data: 'email', name: 'email'},
                    {data: 'created_at', name: 'created_at'},
                ]
            });
            table.on('draw', function () {
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });

            /*  Cancel functionality in row */
            function restoreRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }
			/*  Edit functionality  */
            var row_id, name, description, created_date;

            function editRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
                name = aData.name ? aData.name : '';
                description = aData.description ? aData.description : '';        

                jqTds[0].innerHTML = row_id;
                jqTds[1].innerHTML = '<input type="text" name="name" id="name"  class="form-control input-small" value="' + name + '">';
                jqTds[2].innerHTML = '<input type="text" name="description" id="description"  class="form-control input-large" value="' + description + '">';
                //jqTds[3].innerHTML = '<input type="text" name="created_date" id="created_date" class="form-control input-small" value="' + created_date + '">';
                jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            /*  Save functionality  */
            function saveRow(table, nRow) {
                var jqInputs = $('input', nRow);
                name = jqInputs[0].value;
                description = jqInputs[1].value;

				var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
				
                var tableData = 'name=' + name + '&description=' + description +
                        '&_token=' + $('meta[name=_token]').attr('content');

				$.ajax({
                    type: "post",
                    url: '/admin/category/' + row_id + '/update',
                    data: tableData,
                    success: function (result) {
                        console.log('result is' + result);
                        table.draw(false);
                    },
                    error: function (result) {
                        console.log(result)
                    }
                });
            }

            /*  Cancel Edit functionality  */
            function cancelEditRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }

            /*  When clicked on Delete button   */
            table.on('click', '.delete', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                /*  get row id to delete */
                $('#deleteid').val(id);
                /*  display deletemodal */
                $('#deleteModal').modal('show');
            });
            /*  When clicked on Delete button   */
            table.on('click', '.view', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                /*  get row id to delete */
                window.location = "/admin/getcategory/"+id;
            });

            /*  When clicked on cancel button  */
            table.on('click', '.cancel', function (e) {
                e.preventDefault();

                restoreRow(table, nEditing);
                nEditing = null;

            });

            /*  When clicked on edit button  */
            table.on('click', '.edit', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                /*  get row id to delete */
                window.location = "/admin/category/edit/"+id;

            });
        });
    </script>

@stop
