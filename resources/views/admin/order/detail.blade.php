@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
    Order Details
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Details</h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">orders</a>
            </li>
        </ol>
    </section>

    {{-- order information--}}
    <div class="panel-body" style="width:70%;margin-left:15%">

        <form role="form"  enctype="multipart/form-data">
            <div>
                <div class="panel-body" style="width:90%;margin-left:5%">
                    <div class="form-group has-success">
                        <label class="control-label" style="color: #f64747; font-size:25px;">Order Information</label>
                    </div>
                </div>
                <div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="name">Product Name</label>
                        <?php
                        $product = DB::table('occ_products')->where('id', $order->product_id)->first();
                        $productname = '';
                        if (!empty($product)) {
                        $productname = $product->name;
                        }
                        ?>
                        <input type="text" class="form-control" id="name" name="name" disabled value="{{ $productname }}" placeholder="product name">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="name">Description</label>
                        <input type="text" class="form-control" id="name" name="name" disabled value="{{ $order->description }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="gender">Amount</label>
                        <input type="text" class="form-control" id="gender" name="gender" disabled value="{{ $order->amount }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="birthday">Total Price</label>
                        <input type="text" class="form-control" id="birthday" name="birthday" disabled value="{{ $order->totalprice }}">
                    </div>
                </div>
            </div>

            {{--customer information--}}
            <div>
                <div class="panel-body" style="width:90%;margin-left:5%">
                    <div class="form-group has-success">
                        <label class="control-label" style="color: #f64747; font-size:25px;">Customer Information</label>
                    </div>
                </div>
                <div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <?php
                        $photo = $user->pic;
                        if($user->pic == ''){
                            $photo = 'default.png';
                        }
                        ?>
                         <label class="control-label" for="name">Photo</label>
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <?php
                        $user = DB::table('users')->where('id', $order->customer_id)->first();
                        $customername =  $user->first_name.' '.$user->last_name;
                        ?>
                        <label class="control-label" for="name">First Name</label>
                        <input type="text" class="form-control" id="name" name="name" disabled value="{{ $customername }}" placeholder="product name">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="gender">Gender</label>
                        <input type="text" class="form-control" id="gender" name="gender" disabled value="{{ $user->gender }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="birthday">Birthday</label>
                        <input type="text" class="form-control" id="birthday" name="birthday" disabled value="{{ $user->dob }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="email">User Email</label>
                        <input type="text" class="form-control" id="email" name="email" disabled value="{{ $user->email }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="country">Country</label>
                        <input type="text" class="form-control" id="country" name="country" disabled value="{{ $user->country }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="city">City</label>
                        <input type="text" class="form-control" id="city" name="city" disabled value="{{ $user->city }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $user->address }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="address">Contact email</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $user->email2 }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="address">Postal Code</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $user->postal }}">
                    </div>
                    <div class="form-group has-success" style="width:70%;margin-left:15%">
                        <label class="control-label" for="address">Contact Phone</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $user->contactno }}">
                    </div>
                </div>
            </div>
            <div>
                <div class="col-md-12 mar-10"  style="width:50%;margin-left:25%">
                    <input type="reset" value="Close" class="btn btn-success btn-block btn-md btn-responsive" onclick="onBack({{$order->id}})">
                </div>
            </div>
        </form>
    </div>


@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>

    <script>
        function onBack(id){
            history.go(-1);
        }

    </script>

@stop
