@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Orders
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Orders</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">orders</a>
            </li>
            <li class="active" ><?php echo ucfirst($category->name);?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                                    <span style="font-size: 110%">
                                         <i class="livicon" data-name="lab" data-c="#71ef6c" data-hc="#71ef6c" data-size="15" data-loop="true"></i>
                                        <?php echo ucfirst($category->name);?>
                                    </span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div id="sample_editable_1_wrapper" class="">
                            <input type="hidden" id="cat_id" value="{{ $category->id }}">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="ordertable" role="grid">
                                <thead>
                                <tr role="row">
                                    {{--<td class=" details-control" tabindex="0"></td>//--}}
                                    <th class="sorting_asc" tabindex="0" aria-controls="ordertable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   ID
                                            : activate to sort column ascending" style="width: 30px;">ID
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="ordertable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Product Name
                                            : activate to sort column ascending" style="width: 100px;"> Product Name
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="ordertable" rowspan="1"
                                        colspan="1" aria-label="
                                                Customer
                                            : activate to sort column ascending" style="width: 100px;">Customer
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="ordertable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Description
                                            : activate to sort column ascending" style="width: 300px;">Description
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="ordertable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Amount
                                            : activate to sort column ascending" style="width: 30px;">Amount
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="ordertable" rowspan="1"
                                        colspan="1" aria-label="
                                                Total Price
                                            : activate to sort column ascending" style="width: 50px;">Amount
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="ordertable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Created Date
                                            : activate to sort column ascending" style="width:100px;">Created Date
                                    </th>
                                <?php if($_SESSION['userrole'] == 2) {?>
                                    <th class="sorting" tabindex="0" aria-controls="ordertable" rowspan="1"
                                        colspan="1" aria-label="
                                                 Cancel
                                            : activate to sort column ascending" style="width: 30px;">State
                                    </th>
                                <?php }?>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->

    <!-- cancel modal -->
    <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 80%": >
                <div class="modal-body" style="font-size: 150%">
                    Notice!!!
                </div>
                <input type="hidden" id="orderid" value="0">
                <input type="hidden" id="state" value="0">
                <div class="modal-body" style="font-size: 130%" id="modalcontent">
                    Do you really want to cancel this order?
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="modal_order_id" value=""/>
                    <input type="hidden" id="modal_cancel_accept" value=""/>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="updateOrder()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 80%">
                <div class="modal-body" style="font-size: 150%">
                    Notice!!!
                </div>
                <div class="modal-body" style="font-size: 130%" id="messagecontent">
                    Successfully cancelled the order.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/pages/table-editable.js') }}" ></script>--}}

    <script>
        // display table by using Datatable method
        var table = null;
        $(function () {
            var nEditing = null;
            var table = $('#ordertable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("/admin/orders/get/".$category->id) !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'product_id', name: 'product_id'},
                    {data: 'customer_id', name: 'customer_id'},
                    {data: 'description', name: 'description'},
                    {data: 'amount', name: 'amount'},
                    {data: 'totalprice', name: 'totalprice'},
                    {data: 'created_at', name: 'created_at'},
                 <?php if ($_SESSION['userrole'] == 2){?>
                    {data: 'state', name: 'state', orderable: false, searchable: false}
                 <?php }?>
                ]
            });

            table.on('draw', function () {
                console.log('asaa');
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });
        })

        //cancel order
        function cancelModal(id, num){
            $("#modal_order_id").val(id);
            $("#modal_cancel_accept").val(num)
            if(num == 1){
                $('#modalcontent').html("Do you really want to cancel this order?");
                $('#cancelModal').modal('show');
               // document.getElementById("order_state_"+id.toString()).innerHTML="Accept";
            }else {
                $('#modalcontent').html("Do you really want to accept this order?");
                $('#cancelModal').modal('show');
               // document.getElementById("order_state_"+id.toString()).innerHTML="Cancel";
            }
        }
        function updateOrder() {
            var num = $("#modal_cancel_accept").val();
            var id = $("#modal_order_id").val();
            var state;
            if(num == 1){
                 document.getElementById("order_state_"+id.toString()).innerHTML="Accept";
                 state = 0;
            }else {
                 document.getElementById("order_state_"+id.toString()).innerHTML="Cancel";
                state = 1;
            }
            var data = {
                id:id,
                state: state
            };
            $.ajax({
                type: "get",
                url: '/admin/order/stateupdate',
                data: data,
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (result) {

                }
            });
        }
    </script>
@stop
