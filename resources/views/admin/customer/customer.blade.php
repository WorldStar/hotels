@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Customers
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <!--section starts-->
                <h1>Customers</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('dashboard') }}">
                            <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                            Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="#">customers</a>
                    </li>
                    <li class="active">list</li>
                </ol>
            </section>
            <!--section ends-->
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary filterable">
                            <div class="panel-heading clearfix  ">
                                <div class="panel-title pull-left">
                                    <div class="caption" style="font-size: 110%">
                                           <i class="livicon" data-name="user" data-c="#FF5722" data-hc="#FF5722" data-size="12"
                                              data-loop="true"></i>
                                             Customer List
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body table-responsive">
                                <table class="table table-striped table-bordered" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Photo</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Gender</th>
                                            <th>Age</th>
                                            <th>Address</th>
                                            <th>Email </th>
                                            <th>Company</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        //print_r($customers);
                                        $i = 0;
                                        foreach($customers as $customer){
                                        $i++;
                                        $added_date = date('M d, Y', strtotime($customer->created_at));
                                        $info =$customer->address.', '.$customer->city.', '.$customer->state.', '.$customer->country.', '.$customer->postal;
                                        $status = 'Active';
                                        if($customer->status == 1){
                                            $status = 'Inactive';
                                        }
                                        $photo = $customer->pic;
                                        if($customer->pic == ''){
                                            $photo = 'default.png';
                                        }

                                    ?>
                                    <!--$status = value(u.id);
                                       Not authorized
                                      if ($staus == 0){
                                      $site = "http://192.168.3.119:8888";
                                      fopen($site,"r")
                                      or die("Now, you cannot connection to $site. Don,t try again.");
                                    }-->
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td style="cursor:pointer" onclick="customerProfile('{{ $customer->id }}')">
                                            <img src="/uploads/users/{{ $photo  }}" style="width:50px;height:50px;border-radius: 50%">
                                        </td>
                                        <td style="cursor:pointer" onclick="customerProfile('{{ $customer->id }}')">{{ $customer->first_name }}</td>
                                        <td style="cursor:pointer" onclick="customerProfile('{{ $customer->id }}')">{{ $customer->last_name }}</td>
                                        <td>{{ $customer->gender }}</td>
                                        <td>{{ $customer->age }}</td>
                                        <td>{{ $info }}</td>
                                        <td>{{ $customer->email }}</td>
                                        <td>{{ $customer->company }}</td>
                                        <td style="cursor:pointer" class="ianctive_button" onclick="customerInactive('{{ $customer->id }}', '{{ $customer->status }}')">{{ $status }}</td>

                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

<!-- Message modal -->
        <div class="modal fade" id="customerProfileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="width: 80%">
                    <div class="modal-header" style="font-size: 200%">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">User Profile </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="first_name">First Name</label>
                            <span type="text-align" class="form-control" id="first_name"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="last_name">Last Name</label>
                            <span type="text-align" class="form-control" id="last_name"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="email">Email</label>
                            <span type="text-align" class="form-control" id="email"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="gender">Gender</label>
                            <span type="text-align" class="form-control" id="gender"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="birth">Birthday</label>
                            <span type="text-align" class="form-control" id="birth"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="address">Address</label>
                            <span type="text-align" class="form-control" id="address"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="city">City</label>
                            <span type="text-align" class="form-control" id="city"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="country">Country</label>
                            <span type="text-align" class="form-control" id="country"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="email2">Contact Email</label>
                            <span type="text-align" class="form-control" id="email2"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="postal"> Postal Code</label>
                            <span type="text-align" class="form-control" id="postal"></span>
                        </div>
                        <div class="form-group has-success" style="width:70%; margin-left:15%">
                            <label class="control-label" for="phone">Contact Phone</label>
                            <span type="text-align" class="form-control" id="phone"></span>
                        </div>style="font-size: 120%; background: #00dd00"
                    </div>
                    <div class="modal-footer" style="font-size: 120%">
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>

<!-- statechange  modal -->
        <div class="modal fade" id="inactiveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="width: 80%">
                    <input type="hidden" id="customerid" value="0">
                    <input type="hidden" id="status" value="0">
                    <div class="modal-body" id="inactivecontent">
                        Do you really want to in-active this user?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="inactiveProc()">Yes</button>
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>

<!-- Message modal -->
        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="width: 80%">
                    <div class="modal-body" id="messagecontent">
                        Successfully in-actived the user.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>

<!-- active/inactive sfow-->
<style>
    .ianctive_button:hover {
        color:#0618d8;
    }
    .ianctive_button {
        color:  #d80b06
    }
</style>
    @stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script>
        function customerProfile(id){
            $.ajax({
                type: "get",
                url: '/admin/customer/get/' + id,
                success: function (result) {
                    var data = JSON.parse(result);
                    $('#first_name').html(data.first_name);
                    $('#last_name').html(data.last_name);
                    $('#email').html(data.email);
                    $('#gender').html(data.gender);
                    $('#birth').html(data.dob);
                    $('#address').html(data.address);
                    $('#city').html(data.city);
                    $('#state').html(data.state);
                    $('#country').html(data.country);
                    $('#email2').html(data.email2);
                    $('#postal').html(data.postal);
                    $('#phone').html(data.contactno);

                    $('#customerProfileModal').modal('show');
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
        function customerInactive(id, status){
            $('#customerid').val(id);
            $('#status').val(status);
            console.log(status);
            if(status == 1){
                $('#inactivecontent').html("Do you really want to in-active this customer?");
            }else{
                $('#inactivecontent').html("Do you really want to active this customer?");
            }
            $("#inactiveModal").modal('show');
        }
        function inactiveProc(){
            var customerid = $('#customerid').val();
            var status = $('#status').val();
            $.ajax({
                type: "get",
                url: '/admin/customer/inactive/' + customerid+'/'+status,
                success: function (result) {
                    console.log(result);
                    console.log(result.status);
                    if(result.status == 0){
                        $('#messagecontent').html('Successfully in-actived the customer.');
                    }else{
                        $('#messagecontent').html('Successfully actived the customer.');
                    }
                    $('#messageModal').modal('show');
                    location.reload();
                },
                error: function (result) {
                    console.log(result);
                }
            });
        }
    </script>
@stop
