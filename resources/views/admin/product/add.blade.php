@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
    Product Details
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <!--section starts-->
    <h1><?php echo ucfirst($category->name);?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                         Dashboard
            </a>
        </li>
        <li>
            <a href="#">products</a>
        </li>
        <li class="active"><?php echo ucfirst($category->name);?></li>
    </ol>
</section>

<div class="panel-body" style="width:70%;margin-left:15%">
    <div class="form-group has-success">
        <label class="control-label" style="font-size:25px; color: #FF0000;">Add <?php echo ucfirst($category->name);?></label>
    </div>
    <div class="col-sm-12">
        @if(!empty($error))
        {!! $error !!}
        @endif
    </div>
    @if(!empty($success))
        <div class="alert alert-success alert-dismissable">
                {!! $success !!}
        </div>
    @endif
    <form role="form" action="{{ url('/admin/product/store') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="cat_id" value="{{ $category->id }}"/>
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <div class="form-group has-success">
            <label class="control-label" style="font-size: 120%" for="name">Product Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="product name">
        </div>
        <div class="form-group has-success">
            <label class="control-label" for="description" style="font-size: 120%">Product Description</label>
            <textarea class="form-control" id="description" name="description" placeholder="Please enter product description here..." rows="5"></textarea>
        </div>
        <div class="form-group has-success">
            <label class="control-label" for="price" style="font-size: 120%">Product Price</label>
            <input type="text" class="form-control" id="price" name="price" placeholder="product price">
        </div>
        <div class="form-group has-success">
            <label class="control-label" for="amount" style="font-size: 120%">Product Amount</label>
            <input type="text" class="form-control" id="amount" name="amount" placeholder="product amount">
        </div>
        <div class="form-group has-success">
            <label class="control-label" for="photo" style="font-size: 120%">Product Photos</label>
            <input type="file" class="form-control"  multiple="multiple" id="photo" name="photo[]" placeholder="product photos">
        </div>

        <div class="form-group has-warning">
            <label class="control-label" for="vendor" style="font-size: 120%">Vendor</label>
            <select class="form-control" id="vendor" name="vendor">
                <?php
                    foreach($vendors as $vendor){
                        echo '<option value="'.$vendor->id.'">'.$vendor->first_name.' '.$vendor->last_name.'</option>';
                    }
                ?>
            </select>
        </div>

        <div class="form-group has-error">
            <label class="control-label" for="occasion" style="font-size: 120%">Occasion</label>
            <select class="form-control" id="occasion" name="occasion">
                <?php
                foreach($occasions as $occasion){
                    echo '<option value="'.$occasion->id.'">'.$occasion->name.'</option>';
                }
                ?>
            </select>
        </div>

        <div class="col-md-12 mar-10">
            <div class="col-xs-6 col-md-6">
                <input type="submit" name="btnSubmit" id="btnSubmit" style="font-size: 120%" value="Add Product" class="btn btn-primary btn-block btn-md btn-responsive">
            </div>
            <div class="col-xs-6 col-md-6">
                <input type="reset" value="Cancel" style="font-size: 120%" class="btn btn-success btn-block btn-md btn-responsive" onclick="onBack({{ $category->id }})">
            </div>
        </div>
    </form>
</div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
<script>
    function onBack(cat_id){
        window.location = "/admin/products/"+cat_id;
        //history.go(-1);
    }
</script>

@stop
