@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Products
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}"/>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Products</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">products</a>
            </li>
            <li class="active" ><?php echo ucfirst($category->name);?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                                    <span style="font-size: 110%">
                                        <i class="livicon" data-name="lab" data-c="#71ef6c" data-hc="#71ef6c" data-size="15" data-loop="true"></i>
                                        <?php echo ucfirst($category->name);?>
                                    </span>
                        </h3>
                    </div>
                    <div class="panel-body"> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div id="sample_editable_1_wrapper" class="">
                            <input type="hidden" id="cat_id" value="{{ $category->id }}">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="producttable" role="grid">
                                <thead>
                                <tr role="row">
                                    {{--<td class=" details-control" tabindex="0"></td>//--}}
                                    <th class="sorting_asc" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   ID
                                            : activate to sort column ascending" style="width: 10px;">ID
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Name
                                            : activate to sort column ascending" style="width: 50px;">Name
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Description
                                            : activate to sort column ascending" style="width: 50px;">Description
                                    </th>
									<th class="sorting" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1" aria-label="
                                                Price
                                            : activate to sort column ascending" style="width: 30px;">Price
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Amount
                                            : activate to sort column ascending" style="width: 50px;">Amount
                                    </th>
									<th class="sorting" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1" aria-label="
                                                 Total Price
                                            : activate to sort column ascending" style="width: 30px;">Total Price
                                    </th>
                                    <?php if($_SESSION['userrole'] == 1) {?>
                                    <th class="sorting" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1" aria-label="
                                                Vendor
                                            : activate to sort column ascending" style="width: 50px;">Vendor
                                    </th>
                                    <?php }?>
									<th class="sorting" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1" aria-label="
                                                 Created Date
                                            : activate to sort column ascending" style="width: 100px;"> Created Date
                                    </th>

                                    <th class="sorting" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1" aria-label="
                                                 Edit
                                            : activate to sort column ascending" style="width: 30px;">Edit
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="producttable" rowspan="1"
                                        colspan="1" aria-label="
                                                 Delete
                                            : activate to sort column ascending" style="width: 30px;">Delete
                                    </th>

                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->


    <!-- delete modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 80%">
                <div class="modal-body" style="font-size: 150%">
                    Notice!!!
                </div>
                <input type="hidden" id="deleteid" value="0">
                <div class="modal-body">
                    Do you really want to delete this product?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="deleteproduct()">Delete</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Message modal -->
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 80%">
                <div class="modal-body" style="font-size: 150%; background-color: #f64747">
                    Notice!!!
                </div>
                <div class="modal-body" id="messagecontent">
                    Please input a product name.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/pages/table-editable.js') }}" ></script>--}}

    <script>
        // display table by using Datatable method
         var table = null;
        $(function () {
            var nEditing = null;
            var table = $('#producttable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("/admin/products/get/".$category->id) !!}',
                columns: [
                    // {data: 'details-control', name: 'details-control', orderable: true, searchable: true}
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'price', name: 'price'},
                    {data: 'amount', name: 'amount'},
                    {data: 'totalprice', name: 'totalprice'},
                    <?php if($_SESSION['userrole'] == 1) {?>
                    {data: 'vendor', name: 'vendor'},
                    <?php }?>
                    {data: 'created_at', name: 'created_at'},
                    {data: 'edit', name: 'edit', orderable: false, searchable: false},
                    {data: 'delete', name: 'delete', orderable: false, searchable: false}

           ]
            });

            $('#producttable_length').append('<button type="button" style="margin-left: 20px; border:1px solid #ccc; background-color: #6c7def; color: white" class="form-control button-sm" onclick="addProduct()">Add</button >');

            table.on('draw', function () {
                console.log('asaa');
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });

            /*
             Delete Functionality
             */
            table.on('click', '.delete', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                /*  get row id to delete */
                $('#deleteid').val(id);
                /*  display deletemodal */
                $('#deleteModal').modal('show');

                /*
                 When clicked on cancel button
                 */
                table.on('click', '.cancel', function (e) {
                    e.preventDefault();

                    restoreRow(table, nEditing);
                    nEditing = null;

                });
            })


       })
    </script>

    <script>
    /*  add and delete product */
        //delete product
        function deleteproduct(){
            var id = $('#deleteid').val();
           // console.log(id);
            /* delete row in database and datatable  */
            $.ajax({
                type: "get",
                url: '/admin/product/' + id + '/delete',
                success: function (result) {
                    console.log('row ' + result + ' deleted');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
        /*   add product */
        function addProduct () {
            var cat_id = $('#cat_id').val();
            window.location = "/admin/product/"+cat_id+"/add";
        }

    </script>
 @stop
