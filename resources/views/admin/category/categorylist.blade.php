@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Category
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}"/>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Category</h1>
       	<ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <!--<li>
                <a href="#">Categories</a>
            </li>-->
            <li class="active">Category</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    
					<div class="panel-heading">
                        <h3 class="panel-title">
                                    <span style="font-size: 110%">
                                    <i class="livicon" data-c="#6CC66C" data-hc="#6CC66C" data-name="list-ul" data-size="12" data-loop="true"></i>
                                    Category List</span>
                        </h3>
                    </div>
					
                    <div class="panel-body"> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="categorytable" role="grid">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 30px;">ID
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 100px;">name
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 300px;">decription
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 150px;">created date
                                        </th>
                                    
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 50px;">Edit
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="categorytable" rowspan="1"
                                            colspan="1" aria-label="
                                                Full Name
                                            : activate to sort column ascending" style="width: 50px;">Delete
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->

    <!-- add modal -->
	<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 80%">
				<div class="modal-header" style="font-size: 150%;" >
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title" id="myModalLabel">Add Category </h3>
				</div>
				<div class="modal-body" style="font-size: 120%">
					<table style="width:50%" class="table form-body" border="0">
						<tr style="border:0">
							<th style="border:0">name:</th>
							<td style="border:0">
                                <input type="text" name="categoryname" class="form-control" id="categoryname"  value="" placeholder="category name">
							</td>
						</tr>
						<tr>
							<th style="border:0">description:</th>
							<td style="border:0">
								<input type="text" name="categorydescription" class="form-control" id="categorydescription"  value="" placeholder="category description">
							</td>
						</tr>			  
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="addCategory()">Add</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

    <!-- delete modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="width: 80%">
                    <div class="modal-body" style="font-size: 150%">
                        Notice!!!
                    </div>
                    <input type="hidden" id="deleteid" value="0">
                    <div class="modal-body">
                        Do you really want to delete this category?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="deleteCategory()">Delete</button>
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
    </div>

    <!-- Message modal -->
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 80%">
                <div class="modal-body" style="font-size: 150%">
                    Notice!!!
                </div>
                <div class="modal-body" id="messagecontent">
                    Please input a category name.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>


@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/pages/table-editable.js') }}" ></script>--}}

    <script>

        /* display table by using Datatable method  */
        var table = null;
		$(function () {
			var nEditing = null;
            table = $('#categorytable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route("admin.category.data") !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'created_at', name: 'creadted_date'},
					{data: 'edit', name: 'edit', orderable: false, searchable: false},
                    {data: 'delete', name: 'delete', orderable: false, searchable: false}
                ]
            });
            table.on('draw', function () {
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });

            /*  Cancel functionality in row */
            function restoreRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }
			
			/*  in Javascript add HTML(button): append('-')  */
            $('#categorytable_length').append('<button type="button" style="margin-left: 20px; border:1px solid #ccc; background-color: #6c7def; color: white" class="form-control button-sm" data-toggle="modal" data-target="#addModal">Add</button >');
            						
			/*  Edit functionality  */
            var row_id, name, description, created_date;

            function editRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
                name = aData.name ? aData.name : '';
                description = aData.description ? aData.description : '';        

                jqTds[0].innerHTML = row_id;
                jqTds[1].innerHTML = '<input type="text" name="name" id="name"  class="form-control input-small" value="' + name + '">';
                jqTds[2].innerHTML = '<input type="text" name="description" id="description"  class="form-control input-large" value="' + description + '">';
                //jqTds[3].innerHTML = '<input type="text" name="created_date" id="created_date" class="form-control input-small" value="' + created_date + '">';
                jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            /*  Save functionality  */
            function saveRow(table, nRow) {
                var jqInputs = $('input', nRow);
                name = jqInputs[0].value;
                description = jqInputs[1].value;

				var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
				
                var tableData = 'name=' + name + '&description=' + description +
                        '&_token=' + $('meta[name=_token]').attr('content');

				$.ajax({
                    type: "post",
                    url: '/admin/category/' + row_id + '/update',
                    data: tableData,
                    success: function (result) {
                        console.log('result is' + result);
                        table.draw(false);
                    },
                    error: function (result) {
                        console.log(result)
                    }
                });
            }

            /*  Cancel Edit functionality  */
            function cancelEditRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }

            /*  When clicked on Delete button   */
            table.on('click', '.delete', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                /*  get row id to delete */
                $('#deleteid').val(id);
                /*  display deletemodal */
                $('#deleteModal').modal('show');
            });

            /*  When clicked on cancel button  */
            table.on('click', '.cancel', function (e) {
                e.preventDefault();

                restoreRow(table, nEditing);
                nEditing = null;

            });

            /*  When clicked on edit button  */
            table.on('click', '.edit', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];
                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    alert('You are already editing a row, you must save or cancel that row before editing a new row');

                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(table, nEditing);
                    nEditing = null;

                } else {
                    /* No edit in progress - let's start one */
                    editRow(table, nRow);
                    nEditing = nRow;
                }
            });
        });
    </script>

    /*  add and delete category */
    <script>
        //delete category
        function deleteCategory(){
            var id = $('#deleteid').val();
            /* delete row in database and datatable  */
            $.ajax({
                type: "get",
                url: '/admin/category/' + id + '/delete',
                success: function (result) {
                    console.log('row ' + result + ' deleted');
                    table.draw(false);
                    location.reload();

                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

        /*   add category */
        function addCategory () {
            /* get category name to add from modal  */
            var name = $("#categoryname").val();
            /* get category description to add from modal  */
            var description = $("#categorydescription").val();
            /* if dont input  name to add in the modal, display messaggeModal and retry to input name  */
            if (name == "") {
                /*  make  modal- " Please input category name."  */
                $('#messagecontent').html('Please input category name.');
                /*  display  modal- " Please input category name."  */
                $('#messageModal').modal('show');
                return;
            }

           /*  generate data from input value */
            var data = {
              name: name,
              description: description,
              _token:$('meta[name=_token]').attr('content')
            };
            /*  save generate data in database and display  */
            $.ajax({
                type: "post",
                url: '/admin/category/addcategory',
                data: data,
                success: function (result) {
                    table.draw(false);
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
    </script>
@stop
