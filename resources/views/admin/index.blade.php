@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Ihusi Hotels
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/vendors/fullcalendar/css/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/pages/calendar_custom.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" media="all" href="{{ asset('assets/vendors/bower-jvectormap/css/jquery-jvectormap-1.2.2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
    <meta name="_token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">

@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Welcome to Dashboard</h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="#">
                    <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
                    Dashboard
                </a>
            </li>
        </ol>
    </section>
    <?php
        if($_SESSION['userrole']==2){
            $dashboard_layout = "col-lg-4";
        }else{
            $dashboard_layout = "col-lg-3";
        }
    ?>
    <section class="content">
        <div class="row">
            <div class="{{$dashboard_layout}} col-md-6  col-sm-6 margin_10 animated fadeInLeftBig" style="width:50%">
                <!-- Trans label pie charts strats here-->
                <div class="lightbluebg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Reservation's Today</span>
                                        <?php
                                        $todayreservation = DB::table('hotel_tickets')->where('arrival_date', '=', date('Y-m-d'))->where('status', 1)->count();
                                        $dd = date('Y-m-d');
                                        $yesterdayreservation = DB::table('hotel_tickets')->where('arrival_date', '=', date('Y-m-d', strtotime($dd.'-1 days')))->where('status', 1)->count();
                                        $monthreservation = DB::table('hotel_tickets')->where('closed_date', '=', '0000-00-00 00:00:00')->where('status', 1)->count();
                                        $maxday = date('t',strtotime(date('Y-m-d')));
                                        $year = date('Y',strtotime(date('Y-m-d')));
                                        $month = date('m',strtotime(date('Y-m-d')));
                                        $start = $year.'-'.$month.'-01';
                                        $end = $year.'-'.$month.'-'.$maxday;
                                        $monthreservation = $monthreservation + DB::table('hotel_tickets')->where('closed_date', '>=', $start)->where('closed_date', '<=', $end)->where('status', 2)->count();
                                        ?>
                                    <div class="number" style="font-size: 150%">{{ $todayreservation }}</div>
                                </div>
                                <i class="livicon  pull-right" data-name="shopping-cart-in" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Yesterday</small>
                                    <h4>{{ $yesterdayreservation }}</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">This Month</small>
                                    <h4>{{ $monthreservation }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="{{$dashboard_layout}} col-md-6 col-sm-6 margin_10 animated fadeInUpBig" style="width:50%">
                <!-- Trans label pie charts strats here-->
                <div class="redbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Today's Sales</span>
                                    <?php
                                    $todayreservation = DB::table('hotel_tickets')->where('arrival_date', '=', date('Y-m-d'))->where('status', 1)->sum('price');
                                    $dd = date('Y-m-d');
                                    $yesterdayreservation = DB::table('hotel_tickets')->where('arrival_date', '=', date('Y-m-d', strtotime($dd.'-1 days')))->where('status', 1)->sum('price');
                                    $monthreservation = DB::table('hotel_tickets')->where('closed_date', '=', '0000-00-00 00:00:00')->where('status', 1)->sum('price');
                                    $maxday = date('t',strtotime(date('Y-m-d')));
                                    $year = date('Y',strtotime(date('Y-m-d')));
                                    $month = date('m',strtotime(date('Y-m-d')));
                                    $start = $year.'-'.$month.'-01';
                                    $end = $year.'-'.$month.'-'.$maxday;
                                    $monthreservation = $monthreservation + DB::table('hotel_tickets')->where('closed_date', '>=', $start)->where('closed_date', '<=', $end)->where('status', 2)->sum('price');

                                    ?>
                                    <div class="number" style="font-size: 150%">USD {{ number_format($todayreservation) }}</div>
                                </div>
                                <i class="livicon pull-right" data-name="piggybank" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Yesterday</small>
                                    <h4>USD {{ number_format($yesterdayreservation) }}</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">This Month</small>
                                    <h4>USD {{ number_format($monthreservation) }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!--/row-->
        <div class="row ">
            <div class="row" style="padding: 0 30px;">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box primary">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="livicon" data-name="address-book" data-c="rgb(255, 255, 255)" data-hc="rgb(255, 255, 255)" data-size="12"
                               data-loop="true"></i>
                            Today Reservations
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            <table class="table table-hover tb-recently ">
                                <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Reservation ID</th>
                                    <th>Category Name</th>
                                    <th>Customer Name</th>
                                    <th>Number of Room</th>
                                    <th>Price (US $)</th>
                                    <th>Arrival</th>
                                    <th>Departure</th>
                                    <th>Adult</th>
                                    <th>Children</th>
                                    <th>Created Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $reservations = DB::table('hotel_tickets as t')
                                        ->join('hotel_customers as c', 't.customer_id', '=', 'c.id')
                                        ->join('hotel_categories as cc', 't.cat_id', '=', 'cc.id')
                                        ->select('t.*', 'c.name as customername', 'c.surname', 'c.phone', 'c.email', 'cc.name as categoryname')->where('t.status', 1)->where('arrival_date', '=', date('Y-m-d'))->orderby('t.id', 'desc')->get();

                                $i = 0;
                                if(!empty($reservations)){
                                foreach($reservations as $reservation){
                                $i++;
                                $added_date = date('M d, Y', strtotime($reservation->created_at));

                                ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $reservation->ticket_id }}</td>
                                    <td><a href="{{ url('/admin/getcategory/'.$reservation->cat_id) }}" style="text-decoration: none">{{ $reservation->categoryname }}</a></td>
                                    <td><span style="cursor: pointer;color:#337ab7" onclick="viewCustomer('{{ $reservation->customername }}','{{ $reservation->surname }}','{{ $reservation->phone }}','{{ $reservation->email }}')">{{ $reservation->customername }}</span></td>
                                    <td>{{ $reservation->rooms }}</td>
                                    <td>{{ $reservation->price }}</td>
                                    <td>{{ $reservation->arrival_date }}</td>
                                    <td>{{ $reservation->departure_date }}</td>
                                    <td>{{ $reservation->adult }}</td>
                                    <td>{{ $reservation->children }}</td>
                                    <td>{{ $added_date }}</td>

                                </tr>
                                <?php
                                }
                                        }
                                ?>
                                </tbody>
                            </table>
                            <style>
                                .tb-recently td{
                                    text-align:center;
                                }
                            </style>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!--/row-->
        <div class="row ">
            <div class="row" style="padding: 0 30px;">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box primary">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="livicon" data-name="address-book" data-c="rgb(255, 255, 255)" data-hc="rgb(255, 255, 255)" data-size="12"
                               data-loop="true"></i>
                            Recently Reservations
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            <table class="table table-hover tb-recently ">
                                <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Reservation ID</th>
                                    <th>Category Name</th>
                                    <th>Customer Name</th>
                                    <th>Number of Room</th>
                                    <th>Price (US $)</th>
                                    <th>Arrival</th>
                                    <th>Departure</th>
                                    <th>Adult</th>
                                    <th>Children</th>
                                    <th>Created Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $reservations = DB::table('hotel_tickets as t')
                                        ->join('hotel_customers as c', 't.customer_id', '=', 'c.id')
                                        ->join('hotel_categories as cc', 't.cat_id', '=', 'cc.id')
                                        ->select('t.*', 'c.name as customername', 'c.surname', 'c.phone', 'c.email', 'cc.name as categoryname')->where('t.status', 1)->orderby('t.created_at', 'desc')->limit(10)->get();

                                $i = 0;
                                        if(!empty($reservations)){
                                foreach($reservations as $reservation){
                                $i++;
                                $added_date = date('M d, Y', strtotime($reservation->created_at));

                                ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $reservation->ticket_id }}</td>
                                    <td><a href="{{ url('/admin/getcategory/'.$reservation->cat_id) }}" style="text-decoration: none">{{ $reservation->categoryname }}</a></td>
                                    <td><span style="cursor: pointer;color:#337ab7" onclick="viewCustomer('{{ $reservation->customername }}','{{ $reservation->surname }}','{{ $reservation->phone }}','{{ $reservation->email }}')">{{ $reservation->customername }}</span></td>
                                    <td>{{ $reservation->rooms }}</td>
                                    <td>{{ $reservation->price }}</td>
                                    <td>{{ $reservation->arrival_date }}</td>
                                    <td>{{ $reservation->departure_date }}</td>
                                    <td>{{ $reservation->adult }}</td>
                                    <td>{{ $reservation->children }}</td>
                                    <td>{{ $added_date }}</td>

                                </tr>
                                <?php
                                }
                                        }
                                ?>
                                </tbody>
                            </table>
                            <style>
                                 .tb-recently td{
                                     text-align:center;
                                 }
                            </style>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div class="row ">
            <div class="row" style="padding: 0 30px;">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box primary">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="livicon" data-name="user" data-c="rgb(255, 255, 255)" data-hc="rgb(255, 255, 255)" data-size="12"
                               data-loop="true"></i>
                            Recently Customers
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover tb-recently ">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Customer Name</th>
                                    <th>Customer Surname</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Created_Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $customers = DB::table('hotel_customers')->orderby('created_at', 'desc')->limit(10)->get();
                                $i = 0;
                                foreach($customers as $customer){
                                $i++;
                                $added_date = date('M d, Y', strtotime($customer->created_at));

                                ?>
                                <tr>
                                    <td>{{ $customer->id }}</td>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->surname }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->email }}</td>
                                    <td>{{ $added_date }}</td>

                                </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="clearfix"></div>--}}

    </section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- EASY PIE CHART JS -->
    <script src="{{ asset('assets/vendors/bower-jquery-easyPieChart/js/easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bower-jquery-easyPieChart/js/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bower-jquery-easyPieChart/js/jquery.easingpie.js') }}"></script>
    <!--for calendar-->
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/fullcalendar/js/fullcalendar.min.js') }}" type="text/javascript"></script>
    <!--   Realtime Server Load  -->
    <script src="{{ asset('assets/vendors/flotchart/js/jquery.flot.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/flotchart/js/jquery.flot.resize.js') }}" type="text/javascript"></script>
    <!--Sparkline Chart-->
    <script src="{{ asset('assets/vendors/sparklinecharts/jquery.sparkline.js') }}"></script>
    <!-- Back to Top-->
    <script type="text/javascript" src="{{ asset('assets/vendors/countUp.js/js/countUp.js') }}"></script>
    <!--   maps -->
    <script src="{{ asset('assets/vendors/bower-jvectormap/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bower-jvectormap/js/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('assets/vendors/Chart.js/js/Chart.js') }}"></script>
    <!--  todolist-->
    <script src="{{ asset('assets/js/pages/todolist.js') }}"></script>
    <script src="{{ asset('assets/js/pages/dashboard.js') }}" type="text/javascript"></script>




@stop
