<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Welcome to Ihusi Hotel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="img/favicon.ico" />
    <!-- CSS FILES -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/prettyPhoto.css">
    <link rel="stylesheet" href="css/datepicker.css">
    <link rel="stylesheet" href="css/selectordie.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/2035.responsive.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.1.0.min.js"></script>
    <!-- Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="boxed">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<?php
if(isset($_SESSION['error_message']) && $_SESSION['error_message'] != ''){
    $error_message = $_SESSION['error_message'];
    $_SESSION['error_message'] = '';
    echo '<script>alert("'.$error_message.'");</script>';
}
?>
<div id="wrapper" class="container">
    <div class="header"><!-- Header Section -->
        <div class="pre-header"><!-- Pre-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left pre-address-b"><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, GOMA - Congo (DRC)</p></div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <ul class="pre-link-box">
                                <li><a href="/about">About</a></li>
                                <li><a href="/contact">Contact</a></li>
                            </ul>
                        </div>
                        <div class="pull-right">
                            <div class="language-box">
                                <ul>
                                    <li><a href="/"><img alt="language" src="temp/english.png"><span class="language-text">ENGLISH</span></a></li>
                                    <li><a href="/francais"><img alt="language" src="temp/france.png"><span class="language-text">FRANÇAIS</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header"><!-- Main-header -->
            <div class="container">
                <div class="row">
                    <div class="pull-left">
                        <div class="logo">
                            <a href="/"><img alt="Logo" src="img/logo.png" width="100" class="img-responsive" /></a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <nav class="nav">
                                <ul id="navigate" class="sf-menu navigate">
                                    <li class="active"><a href="/">HOMEPAGE</a></li>
                                    <!--<li class="parent-menu"><a href="#">FEATURES</a>
                                        <ul>
                                            <li><a href="#">2 Homepages</a></li>
                                            <li><a href="#">Ajax/PHP Booking Form</a></li>
                                            <li><a href="#">Ultra Responsive</a></li>
                                            <li><a href="under-construction.html">Countdown Page</a></li>
                                            <li><a href="#">2 Category Pages</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <!--<li class="parent-menu"><a href="#">PAGES</a>
                                        <ul>
                                            <li><a href="about.html">About</a></li>
                                            <li><a href="category-grid.html">Category Grid</a></li>
                                            <li><a href="category-list.html">Category List</a></li>
                                            <li><a href="room-single.html">Room Details</a></li>
                                            <li><a href="reservation-form-dark.html">Dark Reservation Form</a></li>
                                            <li><a href="reservation-form-light.html">Light Reservation Form</a></li>
                                            <li><a href="gallery.html">Gallery</a></li>
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog-details.html">Blog Single</a></li>
                                            <li><a href="left-sidebar-page.html">Left Sidebar Page</a></li>
                                            <li><a href="right-sidebar-page.html">Right Sidebar Page</a></li>
                                            <li><a href="under-construction.html">Under Construction</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>-->
                                    <li><a href="/categorylist">ROOMS</a></li>
                                    <li><a href="/event">MEETINGS & EVENTS</a></li>
                                    <li><a href="/gallery">GALLERY</a></li>
                                    <li><a href="/contact">CONTACT</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right">
                            <div class="button-style-1 margint45">
                                <a href="/reservation_form_dark"><i class="fa fa-calendar"></i>BOOK NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slider slider-home"><!-- Slider Section -->
        <div class="flexslider slider-loading falsenav">
            <ul class="slides">
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">WELCOME TO IHUSI HOTEL - KIVU GREAT LAKES...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">4 STAR HOTEL IN NORTH KIVU DRC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="temp/sli-1.jpg" />
                </li>
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">WELCOME TO IHUSI HOTEL - KIVU GREAT LAKES...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">4 STAR HOTEL IN NORTH KIVU DRC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="temp/sli-2.jpg" />
                </li>
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">WELCOME TO IHUSI HOTEL - KIVU GREAT LAKES...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">4 STAR HOTEL IN NORTH KIVU DRC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="temp/maps.jpg" />
                </li>
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">WELCOME TO IHUSI HOTEL - KIVU GREAT LAKES...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">4 STAR HOTEL IN NORTH KIVU DRC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="temp/4.jpg" />
                </li>
                <li>
                    <div class="slider-textbox clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar pull-left">WELCOME TO IHUSI HOTEL - KIVU GREAT LAKES...</div>
                                <div class="slider-triangle pull-left"></div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="slider-bar-under pull-left">4 STAR HOTEL IN NORTH KIVU DRC</div>
                                <div class="slider-triangle-under pull-left"></div>
                            </div>
                        </div>
                    </div>
                    <img alt="Slider 1" class="img-responsive" src="temp/7.jpg" />
                </li>
            </ul>
        </div>
        <div class="book-slider">
            <div class="container">
                <div class="row pos-center">
                    <div class="reserve-form-area">
                        <form action="/reservation" method="post" id="ajax-reservation-form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <ul class="clearfix">
                                <li class="li-input">
                                    <label>ARRIVAL</label>
                                    <input type="text" id="dpd1" name="dpd1" class="date-selector" placeholder="&#xf073;" />
                                </li>
                                <li class="li-input">
                                    <label>DEPARTURE</label>
                                    <input type="text" id="dpd2" name="dpd2" class="date-selector" placeholder="&#xf073;" />
                                </li>
                                <li class="li-select">
                                    <label>ROOMS</label>
                                    <select name="rooms" class="pretty-select">
                                        <option selected="selected" value="1" >1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </li>
                                <li class="li-select">
                                    <label>ADULT</label>
                                    <select name="adult" class="pretty-select">
                                        <option selected="selected" value="1" >1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </li>
                                <li class="li-select">
                                    <label>CHILDREN</label>
                                    <select name="children" class="pretty-select">
                                        <option selected="selected" value="0" >0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </li>
                                <li>
                                    <div class="button-style-1 margint40">
                                        <a id="res-submit" href="#"><i class="fa fa-search"></i>SEARCH</a>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-book-slider">
            <div class="container">
                <div class="row pos-center">
                    <ul>
                        <li><i class="fa fa-shopping-cart"></i> DO YOUR SHOPPINGS AT IHUSI SHOP</li>
                        <li><i class="fa fa-globe"></i> LANGUAGE COMPATIBLE</li>
                        <li><i class="fa fa-coffee"></i> COFFEE & BREAKFAST FREE</li>
                        <li><i class="fa fa-windows"></i> FREE WI-FI ALL ROOM</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="content"><!-- Content Section -->
        <div class="about clearfix"><!-- About Section -->
            <div class="container">
                <div class="row">
                    <div class="about-title pos-center">
                        <h2>WELCOME TO IHUSI HOTEL</h2>
                        <div class="title-shape"><img alt="Shape" src="img/shape.png"></div>
                        <p>Ihusi Hotel is located 6 km from Goma International Airport and 2 km away from the main NGOs district.</p>
                    </div>
                    <div class="otel-info margint60">
                        <div class="col-lg-4 col-sm-12">
                            <div class="title-style-1 marginb40">
                                <h5>GALLERY</h5>
                                <hr>
                            </div>
                            <div class="flexslider">
                                <ul class="slides">
                                    <li><img alt="Slider 1" class="img-responsive" src="temp/imgresp/1.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="temp/imgresp/2.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="temp/imgresp/3.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="temp/imgresp/4.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="temp/imgresp/5.jpg" /></li>
                                    <li><img alt="Slider 1" class="img-responsive" src="temp/imgresp/6.jpg" /></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="title-style-1 marginb40">
                                <h5>ABOUT US</h5>
                                <hr>
                            </div>
                            <p align="justify">Ihusi Hotel is a four-star hotel located in Goma, the touristic capital of the Democratic republic of Congo, in the province of North-Kivu. Located at the edge of Kivu Lake, Ihusi Hotel offers a breath-taking view on the mountain ranges of the famous park of Virunga the second world heritage of UNESCO.</p>
                            <p align="justify">With its 14-year existence, the four-star property is designed for the higher-level corporate guests or tourists. The hotel operates under the slogan “feel at home”, so coined because in providing comfortable environment and warm welcome, Ihusi Hotel is simply the in the region. The hotel also features 72 rooms and a full spa for your pleasure.&nbsp;&nbsp;<a class="active-color" href="about.html">[READ MORE]</a></p>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="title-style-1 marginb40">
                                <h5><i class="fa fa-money"></i>BANKING</h5>
                                <hr>
                            </div>
                            <div class="home-news">
                                <div class="news-box clearfix">

                                    <div class="news-content pull-left">
                                        <h6><a href="#">Secure Money Withdrawal with TMB</a></h6>
                                        <p class="margint10">We have an indoors ATM machine that allows our clients to withdraw money securely. The ATM machine is provided by a local bank.</p>
                                    </div>
                                </div>
                                <div class="news-box clearfix">

                                    <div class="news-content pull-left">
                                        <img src="temp/tmb.jpg" width="350"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="explore-rooms margint30 clearfix"><!-- Explore Rooms Section -->
            <div class="container">
                <div class="row">
                    <div class="title-style-2 marginb40 pos-center">
                        <h3>EXPLORE ROOMS</h3>
                        <hr>
                    </div>
                    <?php
                        $categories = DB::table('hotel_categories as c')
                            ->join('hotel_options as o', 'o.cat_id', '=', 'c.id')
                            ->leftJoin('hotel_airs as a', 'a.id', '=', 'o.opt_air')
                                ->leftJoin('hotel_balconies as b', 'b.id', '=', 'o.opt_balconies')
                                ->leftJoin('hotel_beds as be', 'be.id', '=', 'o.opt_bed')
                                ->leftJoin('hotel_desks as d', 'd.id', '=', 'o.opt_desk')
                                ->leftJoin('hotel_photos as p', 'p.cat_id', '=', 'c.id')
                                ->leftJoin('hotel_safes as s', 's.id', '=', 'o.opt_safe')
                                ->leftJoin('hotel_tvs as t', 't.id', '=', 'o.opt_tv')
                                ->leftJoin('hotel_wardrobes as w', 'w.id', '=', 'o.opt_wardrobe')
                            ->select(['c.*', 'o.opt_balconies', 'o.opt_bed', 'o.opt_desk', 'o.opt_safe', 'o.opt_tv', 'o.opt_wardrobe', 'o.opt_air', 'o.opt_calendar', 'o.opt_wifi', 'o.opt_service', 'o.opt_minibar', 'o.opt_table', 'o.opt_hair', 'b.frname as balname', 'a.frname as airname', 'be.frname as bedname', 'd.frname as deskname', 'p.photo', 's.frname as safename', 't.frname as tvname', 'w.frname as wname'])->orderby('c.id', 'asc')->limit(3)->get();
                        foreach($categories as $category){
                    ?>
                    <div class="col-lg-4 col-sm-6">
                        <div class="home-room-box">
                            <div class="room-image">
                                <img alt="Room Images" class="img-responsive" src="/uploads/files/{{ $category->photo }}">
                                <div class="home-room-details">
                                    <h5><a href="/catitem/{{ $category->id }}">The luxury room at Ihusi - {{ $category->name }}</a></h5>
                                    <div class="pull-left">
                                        <ul>
                                            @if($category->opt_calendar == 1)
                                            <li><i class="fa fa-calendar"></i></li>
                                            @endif
                                            @if($category->opt_wifi == 1)
                                            <li><i class="fa fa-flask"></i></li>
                                            @endif
                                            @if($category->opt_balconies != 0)
                                            <li><i class="fa fa-umbrella"></i></li>
                                            @endif
                                            @if($category->opt_tv != 1)
                                            <li><i class="fa fa-laptop"></i></li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="pull-right room-rating">
                                        <ul>
                                            <?php if($category->stars == 1){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                                <?php }else if($category->stars == 2){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                                <?php }else if($category->stars == 3){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                                <?php }else if($category->stars == 4){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                                <?php }else if($category->stars == 5){ ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                                <?php }else{ ?>
                                                <li><i class="fa fa-star inactive"></i></li>
                                                <li><i class="fa fa-star inactive"></i></li>
                                                <li><i class="fa fa-star inactive"></i></li>
                                                <li><i class="fa fa-star inactive"></i></li>
                                                <li><i class="fa fa-star inactive"></i></li>
                                                <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="room-details" style="height: 140px;max-height: 140px;">
                                <p>{{ $category->desc }}</p>
                            </div>
                            <div class="room-bottom">
                                <div class="pull-left"><h4>{{ $category->price }}$<span class="room-bottom-time">/ Day</span></h4></div>
                                <div class="pull-right">
                                    <div class="button-style-1">
                                        <a href="/catitem/{{ $category->id }}">READ MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div id="parallax123" class="parallax parallax-one clearfix margint60"><!-- Parallax Section -->
            <div class="support-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-4">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <div class="flipper">
                                    <div class="support-box pos-center front">
                                        <div class="support-box-title"><i class="fa fa-phone"></i></div>
                                        <h4>CALL US</h4>
                                        <p class="margint20">The Ihusi Hotel call center is available 24/7.<br>Call us today</p>
                                    </div>
                                    <div class="support-box pos-center back">
                                        <div class="support-box-title"><i class="fa fa-phone"></i></div>
                                        <h4>PHONE NUMBERS</h4>
                                        <p class="margint20">+243(0) 81 31 29 560 (Reception)
                                            <br /> +243(0) 81 35 32 300 (Management)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <div class="flipper">
                                    <div class="support-box pos-center front">
                                        <div class="support-box-title"><i class="fa fa-envelope"></i></div>
                                        <h4>SEND US E-MAIL</h4>
                                        <p class="margint20">For all inquiries, for assistance and information about general service<br>Please send us an email</p>
                                    </div>
                                    <div class="support-box pos-center back">
                                        <div class="support-box-title"><i class="fa fa-envelope"></i></div>
                                        <h4>E-MAIL ADDRESS</h4>
                                        <p class="margint20">Send us an email today at:<br />ihusihotel@yahoo.fr</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                <div class="flipper">
                                    <div class="support-box pos-center front">
                                        <div class="support-box-title"><i class="fa fa-home"></i></div>
                                        <h4>VISIT US</h4>
                                        <p class="margint20">Our doors are opened 24/7. <br>Please pay us a visit</p>
                                    </div>
                                    <div class="support-box pos-center back">
                                        <div class="support-box-title"><i class="fa fa-home"></i></div>
                                        <h4>COMPANY ADDRESS</h4>
                                        <p class="margint20">Ihusi Hotel, 140 Boulevard Kanyamuhanga, <br>Goma North Kivu<br>Congo (DRC)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Newsletter Section -->
        <!--<div class="newsletter-section">
            <div class="container">
                <div class="row">
                    <div class="newsletter-top pos-center margint30">
                        <img alt="Shape Image" src="img/shape.png" >
                    </div>
                    <div class="newsletter-form margint40 pos-center">
                        <div class="newsletter-wrapper">
                            <div class="pull-left">
                                <h2>Sign up newsletter</h2>
                            </div>
                            <div class="pull-left">
                                <form action="#" method="post" id="ajax-contact-form">
                                    <input type="text" placeholder="Enter a e-mail address">
                                    <input type="submit" value="SUBSCRIBE" >
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="footer margint40"><!-- Footer Section -->
            <div class="main-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-sm-2 footer-logo">
                            <img alt="Logo" src="img/logo.png" class="img-responsive" >
                        </div>
                        <div class="col-lg-10 col-sm-10">
                            <div class="col-lg-3 col-sm-3">
                                <h6>DISCOVER IEXPRESS</h6>
                                <ul class="footer-links">
                                    <li>Book and pay for your boats tickets online by using our newly designed Ihusi Express Web App. Click <a href="http://iexpress.ihusigroups.com" target="_blank">HERE</a> to discover the app.<br><br><img src="temp/logo.png" width="150"></li>
                                    <li>Available for<br><img src="temp/android.png"></li>
                                    <li><p><img src="temp/ios.png"></p></li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <h6>THINGS TO DO (OUTSIDE SERVICE)</h6>
                                <ul class="footer-links">
                                    <li>Visit the Virunga National Park and The Nyiragongo Mount<br><img src="temp/1.png" width="150"></a><br>Virunga National Park Tourism Office
                                        <br>Boulevard Kanya Mulanga
                                        <br>Goma, DRC

                                        <br><br><i class="fa fa-envelope"></i><a href="mailto:visit@virunga.org">visit@virunga.org</a>
                                        <br><i class="fa fa-phone"></i>+243 99 1715401</li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <h6>PAGES SITE</h6>
                                <ul class="footer-links">
                                    <li><a href="/contact">Contact</a></li>
                                    <li><a href="/gallery">Gallery</a></li>
                                    <li><a href="/categorylist">Rooms</a></li>
                                    <br><br>
                                    <li>
                                        <div class="newsletter-form margint40">
                                            <div class="newsletter-wrapper">
                                                <div class="pull-left">
                                                    <h2><i class="fa fa-shopping-cart"></i><b>Ihusi Shop</b></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>Buy souvenirs at the Ihusi Shop during your stay. We are top on fashion! <br>Please visit us.
                                    <li><i class="fa fa-envelope"></i><a href="mailto:ishop@ihusigroups.com">ishop@ihusigroups.com</a>
                                        <br><i class="fa fa-phone"></i>+243 99 0000000
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <h6>CONTACT</h6>
                                <ul class="footer-links">
                                    <li><p><i class="fa fa-map-marker"></i> Ihusi Hotel, 140 Boulevard Kanyamuhanga, Congo (DRC)</p></li>
                                    <li><p><i class="fa fa-phone"></i> +243(0) 81 31 29 560 </p></li>
                                    <li><p><i class="fa fa-envelope"></i> <a href="mailto:info@2035themes.com">ihusihotel@ihusigroups.com</a></p></li>
                                    <li><br><br><b>TRIP ADVISOR AWARDED</b></li>
                                    <li><img src="temp/ta.jpg" width="80">&nbsp;&nbsp;<img src="temp/tatc.jpg" width="80"></li>
                                    <li><br></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pre-footer">
                <div class="container">
                    <div class="row">
                        <div class="pull-left"><p>© IHUSI HOTEL 2016</p></div>
                        <div class="pull-right">
                            <ul>
                                <li><p>DESIGNED BY aXiom INVENT</p></li>
                                <li></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- JS FILES -->
<script src="js/vendor/jquery-1.11.1.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/retina-1.1.0.min.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/superfish.pack.1.4.1.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/selectordie.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/main.js"></script>
<!--
<script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src='//www.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
-->
</body>
</html>